import React from "react";

import "react-minimal-side-navigation/lib/ReactMinimalSideNavigation.css";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import Body from "./components/Body";
import Footer from "./components/Footer";
import Header from "./components/Header";

import "./css/style.css";

function App() {
  return (
    <div className="app">
      <ToastContainer position="top-right" autoClose={2000} />
      <Header />
      <Body />
      <Footer />
    </div>
  );
}

export default App;
