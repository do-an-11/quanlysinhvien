import React, { useState } from "react";
import { Link } from "react-router-dom";
import { PieChart } from "react-minimal-pie-chart";

import styles from "../../css/syllabus.module.css";
import { useTranslation } from "react-i18next";

const myData = [
  { title: "Assignment/Lab", value: 34, color: "#f4be37" },
  { title: "Concept/Lecture", value: 10, color: "#ff9f40" },
  { title: "Guide/Review", value: 29, color: "#0d2535" },
  { title: "Test Quiz/Exam", value: 26, color: "#5388d8" },
];

const OthersPage = ({
  prevStep,
  submitData,
  state,
  listChange,
}) => {
  const [content, setContent] = useState("");

  const handleContentChange = (event) => {
    setContent(event.target.value);
    let data = { ...state, trainingPrinciple: event.target.value };
    listChange(data);
  };

  const handleSubmit = (event) => {
    submitData(event);
  };

  const listItems = myData.map((data, index) => (
    <li key={index}>
      {data.title} ({data.value}%)
    </li>
  ));

  const { t } = useTranslation();

  return (
    <div className={styles.formSyllabus}>
      <div className={styles.pieChartContainer}>
        <div className={styles.pieChart}>
          <h3
            style={{
              textAlign: "center",
              fontStyle: "bold",
              background: "#2d3748",
              borderRadius: "10px 10px 0px 0px",
              color: "white",
              height: "40px",
            }}
          >
            {t("time_allocation")}
          </h3>
          <div
            style={{
              display: "flex",
              flexDirection: "row-reverse",
              marginTop: "70px",
            }}
          >
            <PieChart
              animation
              animationDuration={500}
              animationEasing="ease-out"
              center={[50, 50]}
              data={myData}
              labelPosition={50}
              lengthAngle={360}
              radius={30}
              startAngle={0}
            />
            <div className={styles.assignItems}>
              <ul style={{ fontSize: "20px", rowGap: "1rem" }}>{listItems}</ul>
            </div>
          </div>
        </div>
        <div className={styles.assessmentScheme}>
          <h3
            style={{
              textAlign: "center",
              fontStyle: "bold",
              background: "#2d3748",
              borderRadius: "10px 10px 0px 0px",
              color: "white",
              height: "40px",
            }}
          >
            {t("assessment_scheme")}
          </h3>
          <div
            className={styles.examPoint}
            style={{ paddingLeft: "2rem", paddingRight: "2rem" }}
          >
            <div className={styles.assessContent}>
              <span style={{ display: "inline-block" }}>{t("quiz")}*</span>
              <input
                style={{ fontWeight: "italic", marginLeft: "60px" }}
                type="text"
                name=""
                className={styles.formGroupInput}
              />
            </div>
            <div className={styles.assessContent}>
              <span style={{ display: "inline-block" }}>
                {t("assignment")}*
              </span>
              <input
                style={{ fontWeight: "italic" }}
                type="text"
                name=""
                className={styles.formGroupInput}
              />
            </div>
            <div className={styles.assessContent}>
              <span style={{ display: "inline-block" }}>{t("final")}*</span>
              <input
                style={{ fontWeight: "italic", marginLeft: "60px" }}
                type="text"
                name=""
                className={styles.formGroupInput}
              />
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                borderTop: "1px solid black",
                borderBottom: "1px solid black",
              }}
            >
              <div className={styles.assessContent}>
                <span style={{ display: "inline-block" }}>
                  {t("final_theory")}*
                </span>
                <input
                  style={{ fontWeight: "italic" }}
                  type="text"
                  name=""
                  className={styles.formGroupInput}
                />
              </div>
              <div className={styles.assessContent}>
                <span style={{ display: "inline-block" }}>
                  {t("final_practice")}*
                </span>
                <input
                  style={{ fontWeight: "italic" }}
                  type="text"
                  name=""
                  className={styles.formGroupInput}
                />
              </div>
            </div>

            <div>
              <p style={{ fontWeight: "bold", margin: "0 auto" }}>
                {" "}
                Passing criteria
              </p>
              <div className={styles.assessContent}>
                <span style={{ display: "inline-block" }}>{t("gpa")}*</span>
                <input
                  style={{ fontWeight: "italic", marginLeft: "60px" }}
                  type="text"
                  name=""
                  className={styles.formGroupInput}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <h3
          style={{
            textAlign: "center",
            fontStyle: "bold",
            background: "#2d3748",
            borderRadius: "10px 10px 0px 0px",
            color: "white",
            height: "40px",
          }}
        >
          {t("training_delivery_principle")}
        </h3>
        <textarea
          type="text"
          value={state?.trainingPrinciple}
          onChange={handleContentChange}
          className={styles.formTextArea}
        />
      </div>
      <div className={styles.buttons}>
        <div>
          <button
            className={`${styles.buttons__button} ${styles.buttons__button__back}`}
            onClick={prevStep}
          >
            {t("pre")}
          </button>
        </div>
        <div>
          <Link to="/syllabus">
            <button
              className={`${styles.buttons__button} ${styles.buttons__button__cancel}`}
            >
              {t("cancel")}
            </button>
          </Link>
          <button
            className={`${styles.buttons__button} ${styles.buttons__button__next}`}
            type="submit"
            onClick={handleSubmit}
          >
            {t("save")}
          </button>
        </div>
      </div>
    </div>
  );
};

export default OthersPage;
