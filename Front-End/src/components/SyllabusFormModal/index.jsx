import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { CardTitle } from "reactstrap";
import GeneralPage from "./GeneralPage";
import MultiStepProgressBar from "./MultiStepProgressBar";
import OthersPage from "./OthersPage";
import OutlinePage from "./OutlinePage";
import { actPostSyllabusAPIAsync, actSyllabusUpdateAPIAsync } from "../../store/syllabus/action";
import { actSyllabusListAPIGetAsync } from "../../store/syllabus/action";
import { useTranslation } from "react-i18next";
const levelsData = ["Beginner", "Intermediate", "Advanced"];
const items = ["General", "Outline", "Others"];
const Form = () => {
  const [step, setStep] = useState(0);
  const navigate = useNavigate()
  const [formData, setFormData] = useState({
    topicName: "",
    topicCode: "",
    version: "",
    techrequire: "",
    trainingAudience: "",
    trainingPrinciple: "",
    courseobj: "",
    level: "",
    isErrorTopicName: true,
    isErrorTopicCode: true,
    isErrorAttendee: true,
    errorMessageTopicName: "",
    errorMessageTopicCode: "",
    errorMessageAttendee: "",
    Days: [],
  });
  const param = useParams();
  const { id } = param;
  const user = useSelector((state) => state.USER.currentUser);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(actSyllabusListAPIGetAsync(localStorage.getItem("ACCESS_TOKEN")));
  }, []);
  let getallsyllabus = useSelector((state) => state.SYLLABUS.syllabus);
  useEffect(() => {
    if (id && getallsyllabus) {
      let item = getallsyllabus?.find((e) => e?.syllabusCode === id);
      let data = { ...item };
      let update = {
        topicName: data?.syllabusName,
        topicCode: data?.syllabusCode,
        version: data?.version,
        techrequire: data?.technicalRequirement,
        trainingAudience: data?.attendeeNumber,
        trainingPrinciple: data?.trainingPrinciple,
        courseobj: data?.courseObjective,
        level: data?.level,
        Days: data?.dayList?.map((e) => {
          return {
            id: e?.dayNumber,
            units: e?.unitList?.map((i) => {
              return {
                name: i?.unitName,
                id: i?.unitId,
                newcontent: i?.contentList?.map((o) => {
                  return {
                    id: o?.contentId,
                    name: o?.contentName,
                    note: o?.note,
                    delivery: o?.deliveryType,
                    trainingtime: o?.duration,
                    output: o?.standardOutput,
                    trainingMaterials: [],
                    status: o?.online,
                  };
                }),
              };
            }),
          };
        }),
      };
      setFormData(update)
    }
  }, [id, getallsyllabus]);
  function handleChange(item) {
    setFormData({ ...item });
  }

  const validateFields = useCallback(() => {
  }, [formData]);

  const nextStep = () => setStep((prevStep) => prevStep + 1);
  const prevStep = () => setStep((prevStep) => prevStep - 1);

  const submitData = (e) => {
    e.preventDefault();

    // Add submit logic here
    if (id) {
      let data = { ...formData };
      let dataupdate = {
        topicCode: data?.topicCode,
        topicName: data?.topicName,
        version: data?.version,
        technicalRequirement: data?.techrequire,
        priority: data?.level,
        courseObjective: data?.courseobj,
        publishStatus: "active",
        trainingPrinciple: data?.trainingPrinciple,
        creatorEmail: user?.email,
        trainingAudience: data?.trainingAudience,
        assignmentLab: 0,
        conceptLecture: 0,
        guideReview: 0,
        testQuiz: 0,
        exam: 0,
        quiz: 0,
        assignment: 0,
        fin: 0,
        finalTheory: 0,
        finalPractice: 0,
        gpa: 0,
        syllabus: data?.Days?.map((e) => {
          return {
            dayNumber: e?.id,
            units: e?.units?.map((i) => {
              return {
                unitName: i?.name,
                unitCode: i?.id,
                contents: i?.newcontent?.map((o) => {
                  return {
                    contentId: o?.id,
                    content: o?.name,
                    note: "note",
                    deliveryType: o?.delivery,
                    duration: o?.trainingtime,
                    standardOutput: "H4SD",
                    // trainingMaterials: o?.names,
                    trainingMaterials: [],
                    online: o?.status,
                  };
                }),
              };
            }),
          };
        }),
      }
      dispatch(
        actSyllabusUpdateAPIAsync(
          dataupdate,
          localStorage.getItem("ACCESS_TOKEN")
        )
      ).then(() => {
        navigate("/syllabus");
      });
    } else {
      let data = { ...formData };
      let formatData = {
        topicCode: data?.topicCode,
        topicName: data?.topicName,
        version: data?.version,
        technicalRequirement: data?.techrequire,
        priority: data?.level,
        courseObjective: data?.courseobj,
        publishStatus: "active",
        trainingPrinciple: data?.trainingPrinciple,
        creatorEmail: user?.email,
        trainingAudience: data?.trainingAudience,
        assignmentLab: 0,
        conceptLecture: 0,
        guideReview: 0,
        testQuiz: 0,
        exam: 0,
        quiz: 0,
        assignment: 0,
        fin: 0,
        finalTheory: 0,
        finalPractice: 0,
        gpa: 0,
        syllabus: data?.Days?.map((e) => {
          return {
            dayNumber: e?.id,
            units: e?.units?.map((i) => {
              return {
                unitName: i?.name,
                unitCode: i?.id,
                contents: i?.newcontent?.map((o) => {
                  return {
                    contentId: o?.id,
                    content: o?.name,
                    note: "note",
                    deliveryType: o?.delivery,
                    duration: o?.trainingtime,
                    standardOutput: o?.output[0],
                    trainingMaterials: [],
                    online: o?.status,
                  };
                }),
              };
            }),
          };
        }),
      };
      dispatch(
        actPostSyllabusAPIAsync(
          formatData,
          localStorage.getItem("ACCESS_TOKEN")
        )
      ).then(() => {
        navigate("/syllabus");
      });
    }
  };

  const { t } = useTranslation();

  const levelsDataKeys = ["beginner", "intermediate", "advanced"];
  const itemsKeys = ["general", "outline", "others"];

  const levelsData = levelsDataKeys.map((level) => t(level));
  const items = itemsKeys.map((item) => t(item));

  const levelOptions = levelsData.map((level, index) => (
    <option key={index} value={level}>
      {level}
    </option>
  ));
  return (
    <div>
      <CardTitle style={{ display: "flex", alignItems: "center" }}>
        <h1 style={{ marginRight: "100px", fontSize: "40px" }}>Syllabus</h1>
        <MultiStepProgressBar step={step} />
      </CardTitle>
      <div className="row" style={{ textAlign: "center", margin: "0" }}>
        {items.map((item, index) => (
          <div
            key={index}
            className={`col-lg-2 ${step === index ? "clicked" : ""}`}
            style={{
              color: "white",
              background: step === index ? "#002761" : "#6d7684",
              borderTopLeftRadius: "10px",
              borderTopRightRadius: "10px",
              cursor: "pointer",
              margin: "20px 0 0px 0",
            }}
            onClick={() => setStep(index)}
          >
            {item}
          </div>
        ))}
      </div>
      <div>
        {step !== null && (
          <>
            {step === 0 && (
              <GeneralPage
                nextStep={nextStep}
                listChange={handleChange}
                levelOptions={levelOptions}
                state={formData}
                id={id}
              />
            )}
            {step === 1 && (
              <OutlinePage
                nextStep={nextStep}
                prevStep={prevStep}
                state={formData}
                listChange={handleChange}
              />
            )}
            {step === 2 && (
              <OthersPage
                nextStep={nextStep}
                prevStep={prevStep}
                listChange={handleChange}
                submitData={submitData}
                state={formData}
              />
            )}
          </>
        )}
      </div>
    </div>
  );
};

export default Form;
