import { useState } from "react";

import { Button, Form, Modal } from "react-bootstrap";
const DeleteDay = (props) => {
  const { show, handleClose, dataDeleteDay, handleDeleteUserFromModal } = props;

  const confirmDelete = () => {
    handleClose();
    handleDeleteUserFromModal(dataDeleteDay);
  };

  return (
    <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false}>
      <Modal.Header closeButton>
        <Modal.Title>Delete Day</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form.Group controlId="Syllabus">
          <div>Are you sure you want to delete this day?</div>
        </Form.Group>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
        <Button variant="danger" color="error" onClick={() => confirmDelete()}>
          Delete
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
export default DeleteDay;
