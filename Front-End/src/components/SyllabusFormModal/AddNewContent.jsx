import { useState } from "react";

import {
  faBookmark,
  faEarListen,
  faHand,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Switch from "@mui/material/Switch";
import { Button, Form, Modal } from "react-bootstrap";
import { GrWorkshop } from "react-icons/gr";
import { ImSearch } from "react-icons/im";
import { MdQuiz } from "react-icons/md";
import { PiExamBold } from "react-icons/pi";
import Select from "react-select";
import CreatableSelect from "react-select/creatable";

const AddNewContent = (props) => {
  const { show, handleClose, handleAddNewContentModel, datanewcontent } = props;
  const [name, setName] = useState("");
  const [output, setOutput] = useState("");
  const [trainingtime, settrainingtime] = useState("");
  const [delivery, setdelivery] = useState("");
  const [status, setstatus] = useState(false);
  const [validated, setValidated] = useState(false);
  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    setValidated(true);
  };

  const colourOptions = [
    { value: "H4SD", label: "H4SD", color: "#00B8D9", isFixed: true },
    { value: "H6SD", label: "H6SD", color: "#0052CC", isDisabled: true },
    { value: "HK416", label: "HK416", color: "#5243AA" },
    { value: "K6SD", label: "K6SD", color: "#FF5630", isFixed: true },
    { value: "M4A1", label: "M4A1", color: "#FF8B00" },
    { value: "MAC10", label: "MAC10", color: "#FFC400" },
    { value: "MP5K", label: "MP5K", color: "#36B37E" },
  ];

  const Mins = [
    { value: "30", label: "30" },
    { value: "60", label: "60" },
  ];
  const optionsDelyveryType = [
    {
      value: "assignment",
      label: (
        <>
          <FontAwesomeIcon icon={faBookmark} /> Assignment/Lab
        </>
      ),
    },
    {
      value: "concept",
      label: (
        <>
          <FontAwesomeIcon icon={faEarListen} /> Concept/Lecture
        </>
      ),
    },
    {
      value: "guide",
      label: (
        <>
          <FontAwesomeIcon icon={faHand} /> Guide/Review
        </>
      ),
    },
    {
      value: "test",
      label: (
        <>
          <MdQuiz /> Test/Quiz
        </>
      ),
    },
    {
      value: "exam",
      label: (
        <>
          <PiExamBold /> Exam
        </>
      ),
    },
    {
      value: "workshop",
      label: (
        <>
          <GrWorkshop /> Seminar/Workshop
        </>
      ),
    },
  ];

  const handleSaveUser = (e) => {
    handleClose();
    setName("");
    setOutput("");
    settrainingtime("");
    setdelivery("");
    setstatus(false);
    handleAddNewContentModel(
      {
        name: name,
        output: output,
        trainingtime: trainingtime,
        delivery: delivery,
        status: status,
      },
      datanewcontent
    );
  };

  return (
    <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false}>
      <Modal.Header closeButton>
        <Modal.Title>New content</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
          <Form.Group controlId="Name">
            <div className="row" style={{ padding: "0px" }}>
              <div className="col-sm-4">
                <p>
                  {" "}
                  <Form.Label style={{ color: "black" }}>Name</Form.Label>
                </p>
              </div>
              <div className="col-sm-8">
                <Form.Control
                  placeholder="Name of content"
                  type="text"
                  onChange={(event) => setName(event.target.value)}
                  style={{ textAlign: "left" }}
                />
                <Form.Control.Feedback type="invalid">
                  The field is required
                </Form.Control.Feedback>
              </div>
            </div>
          </Form.Group>
          <Form.Group controlId="Output">
            <div className="row" style={{ padding: "0px" }}>
              <div className="col-sm-4">
                <p>
                  {" "}
                  <Form.Label style={{ color: "black" }}>
                    Output standard
                  </Form.Label>
                </p>
              </div>
              <div className="col-sm-8">
                <Select
                  defaultValue={[colourOptions[2], colourOptions[3]]}
                  isMulti
                  name="colors"
                  className="basic-multi-select"
                  classNamePrefix="select"
                  placeholder={
                    <>
                      <ImSearch /> Select ...{" "}
                    </>
                  }
                  style={{ width: "100%" }}
                  options={colourOptions}
                  value={output}
                  onChange={(selected) => setOutput(selected)}
                />
              </div>
            </div>
          </Form.Group>
          <Form.Group controlId="trainingtime">
            <div className="row" style={{ padding: "0px" }}>
              <div className="col-sm-4">
                <p>
                  {" "}
                  <Form.Label style={{ color: "black" }}>
                    Training time
                  </Form.Label>
                </p>
              </div>
              <div className="col-sm-8">
                <Select
                  options={Mins}
                  value={trainingtime}
                  onChange={(selected) => settrainingtime(selected)}
                  placeholder={<>Select one</>}
                />
                <Form.Control.Feedback type="invalid">
                  The field is required
                </Form.Control.Feedback>
              </div>
            </div>
          </Form.Group>
          <Form.Group controlId="type">
            <div className="row" style={{ padding: "0px" }}>
              <div className="col-sm-4">
                <p>
                  <Form.Label style={{ color: "black" }}>
                    delivery type
                  </Form.Label>
                </p>
              </div>
              <div className="col-sm-8">
                <CreatableSelect
                  isClearable
                  placeholder="Select one"
                  options={optionsDelyveryType}
                  value={delivery}
                  onChange={(selected) => setdelivery(selected)}
                />
                <Form.Control.Feedback type="invalid">
                  The field is required
                </Form.Control.Feedback>
              </div>
            </div>
          </Form.Group>
          <Form.Group controlId="method">
            <div className="row" style={{ padding: "0px" }}>
              <div className="col-sm-4">
                <p>
                  <Form.Label style={{ color: "black" }}>Method</Form.Label>
                </p>
              </div>
              <div className="col-sm-8">
                <Switch
                  label="Required"
                  onChange={(event) => setstatus(event.target.checked)}
                />
              </div>
            </div>
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
        <Button variant="primary" onClick={() => handleSaveUser()}>
          Save Changes
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
export default AddNewContent;
