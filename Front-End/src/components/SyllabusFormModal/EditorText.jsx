import React, { Component } from "react";
import { Editor } from "@tinymce/tinymce-react";

class EditorText extends Component {
  onChange = (e) => {
    const content = e.target.getContent();
    this.props.parentCallBack(content);
  };

  render() {
    return (
      <div>
        <Editor
          initialValue=""
          init={{
            branding: false,
            height: 400,
            menubar: false,
          }}
          onChange={this.onChange}
        />
      </div>
    );
  }
}

export default EditorText;
