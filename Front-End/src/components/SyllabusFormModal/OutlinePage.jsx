import React, { useState } from "react";

import { Link } from "react-router-dom";

import {
  faCircleMinus,
  faFolder,
  faPencil,
  faPlus,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Modal from "react-bootstrap/Modal";
import { useTranslation } from "react-i18next";
import { MdOutlineArrowDropDownCircle } from "react-icons/md";
import { PieChart } from "react-minimal-pie-chart";
import AddNewContent from "./AddNewContent";
import DeleteDay from "./DeleteDay";
import styles from "../../css/syllabus.module.css";
import "../../css/outline.css";

const myData = [
  { title: "Assignment/Lab", value: 34, color: "#f4be37" },
  { title: "Concept/Lecture", value: 10, color: "#ff9f40" },
  { title: "Guide/Review", value: 29, color: "#0d2535" },
  { title: "Test Quiz/Exam", value: 26, color: "#5388d8" },
];

const listItems = myData.map((data, index) => (
  <li key={index}>
    {data.title} ({data.value}%)
  </li>
));

function OutlinePage(props) {
  let { state, listChange, open } = props;
  const [unitId, setUnitId] = useState(null);
  const [files, setFiles] = useState([]);

  let token = localStorage.getItem("ACCESS_TOKEN");

  const handleContinue = (e) => {
    e.preventDefault();
    props.nextStep(1);
  };

  const back = (e) => {
    e.preventDefault();
    props.prevStep();
  };

  const [days, setDays] = useState([]);
  const [show, setShow] = useState(false);
  const [IsShowModalAddNewContent, setIsShowModalAddNewContent] =
    useState(false);
  const [datanewcontent, setdatanewContent] = useState({});
  const handleShow = (id, index, idNewCotent) => {
    const day = days.find((day) => +day.id === index);
    const unit = day.units.find((unit) => unit.id === id);
    if (!unit) {
      return;
    }
    const item = {
      dayId: day.id,
      unit: {
        ...unit,
        newcontent: unit?.newcontent?.find((e) => e.id === idNewCotent),
      },
    };

    setUnitId(item);
    setShow(true);
  };

  function handleDelete(fileId, individualFileUrl) {
    setFiles((prevFiles) =>
      prevFiles.map((file) => {
        if (file.id === fileId) {
          return {
            ...file,
            files: file.files.filter((f) => f.url !== individualFileUrl),
          };
        }
        return file;
      })
    );
  }

  function handleAddFile(event, dayIndex, unitIndex, newcontentid) {
    const selectedFiles = event.target.files;
    const MAX_SIZE = 25 * 1024 * 1024;

    const newFiles = Array.from(selectedFiles).reduce((acc, file) => {
      if (file.size <= MAX_SIZE) {
        acc.push({
          name: file.name,
          url: URL.createObjectURL(file),
        });
      } else {
        alert(`File ${file.name} exceeds the 25MB limit.`);
      }
      return acc;
    }, []);

    if (newFiles.length === 0) {
      return;
    }

    const fileId = `file-${dayIndex}-${unitIndex}-${newcontentid}`;
    const fileObject = {
      id: fileId,
      files: newFiles,
    };

    setFiles((prevFiles) => {
      const existingIndex = prevFiles.findIndex((file) => file.id === fileId);
      if (existingIndex !== -1) {
        const updatedFiles = [...prevFiles];
        updatedFiles[existingIndex].files = [
          ...updatedFiles[existingIndex].files,
          ...newFiles,
        ];
        return updatedFiles;
      } else {
        return [...prevFiles, fileObject];
      }
    });
  }

  const [showEditUnit, setshowEditUnit] = useState(false);
  const [IsShowModalDeleteDay, setIsShowModalDeleteDay] = useState(false);
  const [dataDeleteDay, setdataDeleteDay] = useState();
  const handleClose = () => {
    setIsShowModalAddNewContent(false);
    setIsShowModalDeleteDay(false);
  };
  const handleClose1 = () => setShow(false);
  const handleAddDay = () => {
    let newDay = {
      id: `${state?.Days.length + 1}`,
      units: [],
      isShowAddUnitButton: false,
    };
    setDays([...state?.Days, newDay]);
    let update = [...state?.Days, newDay];
    listChange({ ...state, Days: update });
  };

  const handleShowUnit = (dayIndex) => {
    const Days = [...state?.Days];
    Days[dayIndex].isShowAddUnitButton = !Days[dayIndex].isShowAddUnitButton;
    setDays(Days);
    let update = Days;
    listChange({ ...state, Days: update });
  };

  const handleAddUnit = (dayIndex) => {
    const Days = [...state?.Days];
    const Units = {
      id: Days[dayIndex].units.length + 1,
      name: "",
      isInput: true,
      inputName: "",
      isButtonVisible: false,
      newcontent: [],
    };
    Days[dayIndex].units.push(Units);
    setDays(Days);
    listChange({ ...state, Days: Days });
  };

  const handleInputChange = (dayIndex, unitIndex, value) => {
    if (value) {
      const Days = [...state?.Days];
      Days[dayIndex].units[unitIndex].inputName = value;
      setDays(Days);
      listChange({ ...state, Days: Days });
    } else {
      const Days = [...state?.Days];
      Days[dayIndex].units[unitIndex].inputName = "";
      setDays(Days);
      listChange({ ...state, Days: Days });
    }
  };

  const createUnit = (dayIndex, unitIndex) => {
    const Days = [...state?.Days];
    const Units = Days[dayIndex].units[unitIndex];
    if (Units.inputName.trim() !== "") {
      const newUnit = {
        ...Units,
        name: Units.inputName,
        isInput: false,
        inputName: Units.inputName,
        isButtonVisible: true,
        newcontent: Units.newcontent,
      };
      Days[dayIndex].units[unitIndex] = newUnit;
      setDays(Days);
      listChange({ ...state, Days: Days });
    }
    setshowEditUnit(!showEditUnit);
  };

  const handleEditUnit = (dayIndex, unitIndex) => {
    const Days = [...state?.Days];
    Days[dayIndex].units[unitIndex].name = "";
    setDays(Days);
    listChange({ ...state, Days: Days });
  };

  const handleAddNewContent = (dayIndex, unitIndex) => {
    setIsShowModalAddNewContent(true);
    setdatanewContent({ dayIndex: dayIndex, unitIndex: unitIndex });
  };

  const handleAddNewContentModel = (newContext, datanewcontent) => {
    const Days = [...state?.Days];
    const Units = Days[datanewcontent.dayIndex].units[datanewcontent.unitIndex];
    const NewContents = {
      id: Units.newcontent.length + 1,
      name: newContext.name,
      output: Array.isArray(newContext.output)
        ? newContext.output.map((e) => e.value)
        : [],
      trainingtime: newContext.trainingtime.value,
      delivery: newContext.delivery.value,
      status: newContext.status,
    };
    Units.newcontent.push(NewContents);

    setDays(Days);
    listChange({ ...state, Days: Days });
  };
  const handleDeleteDay = (dayIndex) => {
    setIsShowModalDeleteDay(true);
    setdataDeleteDay(dayIndex);
  };

  const handleDeleteUserFromModal = (dataDeleteDay) => {
    const Days = [...state?.Days];
    Days.splice(dataDeleteDay, 1);
    setDays(Days);
    listChange({ ...state, Days: Days });
  };

  const showcontent = (dayIndex, unitIndex) => {
    const Days = [...days];
    const Units = Days[dayIndex].units[unitIndex];
    Units.isButtonVisible = !Units.isButtonVisible;
    setDays(Days);
    listChange({ ...state, Days: Days });
  };

  const myData = [
    { title: "Assignment/Lab", value: 34, color: "#f4be37" },
    { title: "Concept/Lecture", value: 10, color: "#ff9f40" },
    { title: "Guide/Review", value: 29, color: "#0d2535" },
    { title: "Test Quiz/Exam", value: 26, color: "#5388d8" },
  ];

  const { t } = useTranslation();
  return (
    <div className={styles.gridTemplate}>
      <div className={styles.formSyllabus}>
        <div>
          <div>
            {" "}
            <div>
              {state?.Days &&
                state?.Days?.length > 0 &&
                state?.Days?.map((day, dayIndex) => {
                  return (
                    <div
                      key={dayIndex}
                      style={{
                        boxShadow: "10px 10px 5px 0px rgba(255,255,255,0.75)",
                        borderRadius: "7px",
                        backgroundColor: "#E8E8E8",
                      }}
                    >
                      <div
                        className="day"
                        onClick={() => handleShowUnit(dayIndex)}
                      >
                        <div className="day-header">
                          <span style={{ padding: "10px" }}>
                            {t("day")} {dayIndex + 1}
                          </span>
                        </div>
                        <div className="iconDelete">
                          <FontAwesomeIcon
                            icon={faCircleMinus}
                            onClick={() => handleDeleteDay(dayIndex)}
                          />
                        </div>
                      </div>
                      {day.isShowAddUnitButton === true ? (
                        <>
                          {day.units &&
                            day.units.length > 0 &&
                            day.units.map((unit, unitIndex) => {
                              return (
                                <div key={unitIndex}>
                                  <div className="unitLine">
                                    <span
                                      style={{
                                        marginRight: "50px",
                                        width: "80px",
                                      }}
                                    >
                                      {t("add_unit")} {unit.id}
                                    </span>
                                    {unit.name && unit.name.length > 0 ? (
                                      <div className="unitLineRight">
                                        <div className="unitLineRight1">
                                          <span>{unit.name}</span>
                                          <FontAwesomeIcon
                                            onClick={() =>
                                              handleEditUnit(
                                                dayIndex,
                                                unitIndex
                                              )
                                            }
                                            style={{
                                              backgroundColor: "black",
                                              color: "white",
                                              padding: "5px 10px",
                                              borderRadius: "20%",
                                            }}
                                            icon={faPencil}
                                          />
                                        </div>
                                        <div className="unitLineRight2">
                                          <MdOutlineArrowDropDownCircle
                                            style={{
                                              width: "30px",
                                              height: "30px",
                                            }}
                                            onClick={() =>
                                              showcontent(dayIndex, unitIndex)
                                            }
                                          />
                                        </div>
                                      </div>
                                    ) : (
                                      <div className="unit-create">
                                        <input
                                          style={{
                                            paddingLeft: "10px",
                                          }}
                                          placeholder={t("unit_name")}
                                          value={unit.inputName}
                                          onChange={(e) =>
                                            handleInputChange(
                                              dayIndex,
                                              unitIndex,
                                              e.target.value
                                            )
                                          }
                                        />
                                        <div>
                                          <button
                                            className="createUnitbtn"
                                            onClick={() =>
                                              createUnit(dayIndex, unitIndex)
                                            }
                                          >
                                            {t("create")}
                                          </button>
                                        </div>
                                      </div>
                                    )}
                                  </div>
                                  {unit.newcontent &&
                                    unit.newcontent.length > 0 &&
                                    unit.newcontent.map(
                                      (newcontent, newcontentIndex) => {
                                        return (
                                          <>
                                            {unit.isButtonVisible &&
                                              unit.isButtonVisible === true ? (
                                              <div
                                                className="row"
                                                style={{
                                                  backgroundColor: "#DCDCDC",
                                                  margin:
                                                    "10px 120px 0px 150px",
                                                  borderRadius: "10px",
                                                  padding: "5px",
                                                  alignItems: "center",
                                                }}
                                              >
                                                <Col>{newcontent.name}</Col>
                                                <Col>{newcontent.output}</Col>
                                                <Col>
                                                  {newcontent.trainingtime}
                                                </Col>
                                                <Col>
                                                  {newcontent.status === true
                                                    ? "Online"
                                                    : "Offline"}
                                                </Col>
                                                <Col>{newcontent.delivery}</Col>
                                                <Col>
                                                  <Button
                                                    variant="primary"
                                                    style={{
                                                      backgroundColor: "black",
                                                      color: "white",
                                                    }}
                                                    onClick={() =>
                                                      handleShow(
                                                        unit.id,
                                                        dayIndex + 1,
                                                        newcontent.id
                                                      )
                                                    }
                                                  >
                                                    <FontAwesomeIcon
                                                      icon={faFolder}
                                                    />
                                                  </Button>
                                                  <Modal
                                                    show={show}
                                                    onHide={handleClose1}
                                                    backdrop="static"
                                                    keyboard={false}
                                                  >
                                                    <Modal.Header closeButton>
                                                      <Modal.Title
                                                        style={{
                                                          marginLeft: "auto",
                                                        }}
                                                      >
                                                        Day {unitId?.dayId}
                                                      </Modal.Title>
                                                    </Modal.Header>
                                                    <Modal.Body>
                                                      <div
                                                        style={{
                                                          display: "flex",
                                                          marginBottom: "10px",
                                                        }}
                                                      >
                                                        <div
                                                          className="col-md-4"
                                                          style={{
                                                            backgroundColor:
                                                              "white",
                                                          }}
                                                        >
                                                          <h5>
                                                            Unit{" "}
                                                            {unitId?.unit?.id}
                                                          </h5>
                                                        </div>
                                                        <div className="col-md-6">
                                                          <h5>
                                                            {unitId?.unit?.name}
                                                          </h5>
                                                        </div>
                                                      </div>
                                                      <div className="contentfafoder">
                                                        <div className="contentfafoder_title">
                                                          <h7>
                                                            {unitId?.unit?.name}
                                                          </h7>
                                                        </div>
                                                        <div
                                                          style={{
                                                            display: "flex",
                                                          }}
                                                        >
                                                          <div
                                                            className="col-md-8"
                                                            style={{
                                                              color: "black",
                                                            }}
                                                          >
                                                            {files?.map(
                                                              (file, index) => {
                                                                if (
                                                                  file?.id.startsWith(
                                                                    `file-${unitId?.dayId}-${unitId?.unit?.id}-${unitId?.unit?.newcontent?.id}`
                                                                  )
                                                                ) {
                                                                  return (
                                                                    <div
                                                                      key={
                                                                        file.id
                                                                      }
                                                                    >
                                                                      {file.files.map(
                                                                        (
                                                                          individualFile,
                                                                          fileIndex
                                                                        ) => (
                                                                          <div
                                                                            key={`${file.id}-file-${fileIndex}`}
                                                                            style={{
                                                                              display:
                                                                                "flex",
                                                                              justifyContent:
                                                                                "space-between",
                                                                            }}
                                                                          >
                                                                            <div>
                                                                              <a
                                                                                key={`${file.id}-file-${fileIndex}`}
                                                                                href={
                                                                                  individualFile.url
                                                                                }
                                                                                target="_blank"
                                                                                rel="noopener noreferrer"
                                                                              >
                                                                                {
                                                                                  individualFile.name
                                                                                }
                                                                              </a>{" "}
                                                                            </div>
                                                                            <div>
                                                                              <FontAwesomeIcon
                                                                                style={{
                                                                                  marginLeft:
                                                                                    "100%",
                                                                                }}
                                                                                icon={
                                                                                  faTrash
                                                                                }
                                                                                onClick={() =>
                                                                                  handleDelete(
                                                                                    file.id,
                                                                                    individualFile.url
                                                                                  )
                                                                                }
                                                                              />
                                                                            </div>
                                                                          </div>
                                                                        )
                                                                      )}
                                                                    </div>
                                                                  );
                                                                }
                                                                return;
                                                              }
                                                            )}
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </Modal.Body>
                                                    <Modal.Footer
                                                      style={{
                                                        justifyContent:
                                                          "center",
                                                      }}
                                                    >
                                                      <div>
                                                        <label
                                                          htmlFor="Import"
                                                          className="btn btn-warning"
                                                        >
                                                          Upload
                                                        </label>
                                                        <input
                                                          id="Import"
                                                          type="file"
                                                          accept=".jpg, .jpeg, .png, .pdf, .ppt, .pptx, .mp4, .avi, .xls, .xlsx"
                                                          placeholder="Choose File"
                                                          hidden
                                                          multiple
                                                          onChange={(event) =>
                                                            handleAddFile(
                                                              event,
                                                              unitId?.dayId,
                                                              unitId?.unit?.id,
                                                              unitId?.unit
                                                                ?.newcontent?.id
                                                            )
                                                          }
                                                        />
                                                      </div>
                                                      <Button
                                                        variant="primary"
                                                        onClick={handleClose1}
                                                      >
                                                        Close
                                                      </Button>
                                                    </Modal.Footer>
                                                  </Modal>
                                                </Col>
                                              </div>
                                            ) : (
                                              <></>
                                            )}
                                          </>
                                        );
                                      }
                                    )}
                                  {unit.name && unit.name.length > 0 ? (
                                    <>
                                      <div>
                                        <button
                                          onClick={() =>
                                            handleAddNewContent(
                                              dayIndex,
                                              unitIndex
                                            )
                                          }
                                          className="AddNewContentbtn"
                                        >
                                          <FontAwesomeIcon icon={faPlus} />
                                        </button>
                                      </div>
                                      <hr
                                        style={{
                                          backgroundColor: "black",
                                          height: "2px",
                                        }}
                                      />
                                    </>
                                  ) : (
                                    <></>
                                  )}
                                </div>
                              );
                            })}
                          <div className="add-unit">
                            <button
                              className="add-unit-button"
                              onClick={() => handleAddUnit(dayIndex)}
                            >
                              {t("add_unit")}
                            </button>
                          </div>
                        </>
                      ) : (
                        <></>
                      )}
                    </div>
                  );
                })}

              <div>
                <button className="add-day" onClick={() => handleAddDay()}>
                  {t("add_date")}
                </button>
              </div>
              <AddNewContent
                show={IsShowModalAddNewContent}
                handleClose={handleClose}
                datanewcontent={datanewcontent}
                handleAddNewContentModel={handleAddNewContentModel}
              />
              <DeleteDay
                show={IsShowModalDeleteDay}
                handleClose={handleClose}
                dataDeleteDay={dataDeleteDay}
                handleDeleteUserFromModal={handleDeleteUserFromModal}
              />
            </div>
          </div>
          <div className={styles.buttons}>
            <div>
              <button
                className={`${styles.buttons__button} ${styles.buttons__button__back}`}
                onClick={back}
              >
                {t("pre")}
              </button>
            </div>
            <div>
              <Link to="/syllabus">
                <button
                  className={`${styles.buttons__button} ${styles.buttons__button__cancel}`}
                >
                  {t("cancel")}
                </button>
              </Link>
              <button
                className={`${styles.buttons__button} ${styles.buttons__button__next}`}
                onClick={handleContinue}
              >
                {t("next")}
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.timeAllocation}>
        <h3
          style={{
            textAlign: "center",
            fontStyle: "bold",
            background: "#2d3748",
            borderRadius: "10px 10px 0px 0px",
            color: "white",
            height: "40px",
          }}
        >
          {t("time_allocation")}
        </h3>
        <div className={styles.assignItems}>
          <PieChart
            animation
            animationDuration={5000}
            animationEasing="ease-out"
            center={[60, 50]}
            data={myData}
            labelPosition={50}
            lengthAngle={360}
            radius={40}
            startAngle={0}
          />
          <ul style={{ fontSize: "20px", rowGap: "1rem", marginTop: "30px" }}>
            {listItems}
          </ul>
        </div>
      </div>
    </div>
  );
}

export default OutlinePage;
