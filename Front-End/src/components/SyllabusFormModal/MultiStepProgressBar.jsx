import React from "react";

import { ProgressBar, Step } from "react-step-progress-bar";
import "react-step-progress-bar/styles.css";

import "../../css/multistep.css";
import { useTranslation } from "react-i18next";

const MultiStepProgressBar = (props) => {
  var stepPercentage = 0;

  if (props.step === 0) {
    stepPercentage = 0;
  } else if (props.step === 1) {
    stepPercentage = 50;
  } else if (props.step === 2) {
    stepPercentage = 100;
  } else {
    stepPercentage = 0;
  }

  const { t } = useTranslation();

  return (
    <ProgressBar percent={stepPercentage}>
      <Step>
        {({ accomplished }) => (
          <div
            className={`indexedStep ${accomplished ? "accomplished" : null}`}
          >
            {t("general")}
          </div>
        )}
      </Step>
      <Step>
        {({ accomplished, index }) => (
          <div
            className={`indexedStep ${accomplished ? "accomplished" : null}`}
          >
            {t("outline")}
          </div>
        )}
      </Step>
      <Step>
        {({ accomplished, index }) => (
          <div
            className={`indexedStep ${accomplished ? "accomplished" : null}`}
          >
            {t("others")}
          </div>
        )}
      </Step>
    </ProgressBar>
  );
};

export default MultiStepProgressBar;
