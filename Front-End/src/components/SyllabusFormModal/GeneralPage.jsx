import React, { useEffect, useState } from "react";
import { PieChart } from "react-minimal-pie-chart";

import { useTranslation } from "react-i18next";
import styles from "../../css/syllabus.module.css";
import EditorText from "./EditorText";

const myData = [
  { title: "Assignment/Lab", value: 34, color: "#f4be37" },
  { title: "Concept/Lecture", value: 10, color: "#ff9f40" },
  { title: "Guide/Review", value: 29, color: "#0d2535" },
  { title: "Test Quiz/Exam", value: 26, color: "#5388d8" },
];

const GeneralPage = (props) => {
  const [state, setState] = useState(props.state);
  const { id } = props
  const {
    validateTopicName,
    validateTopicCode,
    isErrorTopicName,
    isErrorTopicCode,
    errorMessageTopicName,
    errorMessageTopicCode,
    levelOptions,
    listChange,
    nextStep,
  } = props;

  const continueHandler = (e) => {
    e.preventDefault();
    nextStep(0);
  };
  useEffect(() => {
    if (id && props.state) {
      setState(props.state)
    }
  }, [id, props.state])
  const handleChange = (name) => (event) => {
    setState({ ...state, [name]: event.target.value });
    listChange({ ...state, [name]: event.target.value });
  };

  const listItems = myData.map((data, index) => (
    <li key={index}>
      {data.title} ({data.value}%)
    </li>
  ));

  const receiveDataFromChild = (data) => {
    setState({ ...state, courseobj: data });
  };

  const { t } = useTranslation();

  return (
    <div className={styles.gridTemplate}>
      <div className={styles.formSyllabus}>
        <form onSubmit={(e) => e.preventDefault()}>
          <div className={styles.syllabusName}>
            <div>
              <h5 style={{ display: "inline-block" }}>{t("syllabus_name")}*</h5>
              <input
                style={{ fontWeight: "bold", width: "400px" }}
                type="text"
                value={state?.topicName}
                name="topic name"
                onChange={handleChange("topicName")}
                onBlur={validateTopicName}
                className={`${styles.formGroupInput} ${styles.syllabusNameInput}`}
                required
              />
              <div className={styles.error}>
                {isErrorTopicName && errorMessageTopicName}
              </div>
            </div>
            <div>
              <h5 style={{ display: "inline-block", marginRight: "10px" }}>
                {t("code")}
              </h5>
              <input
                style={{
                  fontWeight: "bold",
                  width: "90px",
                  textAlign: "center",
                }}
                type="text"
                value={state?.topicCode}
                name="topic code"
                onChange={handleChange("topicCode")}
                onBlur={validateTopicCode}
                className={`${styles.formGroupInput} ${styles.syllabusNameInput}`}
              />
              <div className={styles.error}>
                {isErrorTopicCode && errorMessageTopicCode}
              </div>
            </div>
            <div>
              <h5 style={{ display: "inline-block", marginRight: "10px" }}>
                {t("ver")}
              </h5>
              <input
                style={{
                  fontWeight: "bold",
                  width: "90px",
                  textAlign: "center",
                }}
                type="text"
                value={state?.version}
                name="version"
                onChange={handleChange("version")}
                className={`${styles.formGroupInput} ${styles.syllabusNameInput}`}
              />
            </div>
          </div>
          <div className={styles.select}>
            <h5 style={{ display: "inline-block", marginRight: "10px" }}>
              {t("level")}*
            </h5>
            <select
              name="level"
              value={state?.level}
              onChange={handleChange("level")}
              className={styles.select__item}
            >
              <option value="null">Auto detect</option>
              {levelOptions}
            </select>
          </div>
          <div className={styles.attendeeNumber}>
            <h5
              style={{
                display: "inline-block",
                margin: "auto 0",
                marginRight: "10px",
              }}
            >
              {t("attendee_number")}*
            </h5>
            <input
              style={{ width: "100px", textAlign: "center" }}
              type="text"
              value={state?.trainingAudience}
              name="trainingAudience"
              onChange={handleChange("trainingAudience")}
              className={styles.attendeeNumberInput}
            />
          </div>
          <div className={styles.technicalRequirement}>
            <h5>{t("technical_req")}*</h5>
            <textarea
              type="text"
              value={state?.techrequire}
              onChange={handleChange("techrequire")}
              className={styles.formTextArea}
            />
          </div>
          <div>
            <h4>{t("course_obj")}</h4>
            <textarea
              type="text"
              value={state?.courseobj}
              onChange={handleChange("courseobj")}
              className={styles.formTextArea}
            />
          </div>
          <div style={{ textAlign: "right" }}>
            <button
              className={`${styles.buttons__button} ${styles.buttons__button__next}`}
              onClick={continueHandler}
            >
              {t("next")}
            </button>
          </div>
        </form>
      </div>
      <div className={styles.timeAllocation}>
        <h3
          style={{
            textAlign: "center",
            fontStyle: "bold",
            background: "#2d3748",
            borderRadius: "10px 10px 0px 0px",
            color: "white",
            height: "40px",
          }}
        >
          Time allocation
        </h3>
        <div className={styles.assignItems}>
          <PieChart
            animation
            animationDuration={500}
            animationEasing="ease-out"
            center={[60, 50]}
            data={myData}
            labelPosition={50}
            lengthAngle={360}
            radius={40}
            startAngle={0}
          />
          <ul style={{ fontSize: "18px", rowGap: "1rem", marginTop: "50px" }}>
            {listItems}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default GeneralPage;
