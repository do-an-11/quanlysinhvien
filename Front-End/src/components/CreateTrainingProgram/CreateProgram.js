import React, { useState } from "react";

import { useTranslation } from "react-i18next";

import styles from "../../css/trainingProgram.module.css";

const CreateProgram = ({ setOpen, id, listItem, item }) => {
  const { t } = useTranslation();
  const [programName, setProgramName] = useState("");
  const [alert, setAlert] = useState("");

  const handleCreateButton = () => {
    if (programName.trim() !== "") {
      listItem({
        ...item,
        trainingProgramName: programName,
      });
      setOpen(true);
      setProgramName("");
      setAlert("");
    } else {
      setAlert(t("enter_value"));
    }
  };

  return (
    <div className={styles.containerClass}>
      {id ? <h1>{t("update_program")}</h1> : <h1>{t("new_program")}</h1>}
      <div className={styles.createClass}>
        <label style={{ color: "black", margin: "auto 20px auto 0" }}>
          {t("program_name")}
        </label>
        <input
          type="text"
          placeholder={t("type_program_name")}
          value={programName}
          onChange={(e) => setProgramName(e.target.value)}
        />
        <button onClick={handleCreateButton}>{t("create")}</button>
        {alert && <p style={{ color: "red" }}>{alert}</p>}
      </div>
    </div>
  );
};

export default CreateProgram;
