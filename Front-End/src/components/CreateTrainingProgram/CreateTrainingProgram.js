import React, { useEffect, useState } from "react";

import { useTranslation } from "react-i18next";
import { ImSearch } from "react-icons/im";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Select from "react-select";
import { actUserGetTrainerAsyncAPI } from "../../store/user/action";

import styles from "../../css/trainingProgram.module.css";

export const CreateTrainingProgram = ({
  item,
  handleSaveButton,
  listItem,
  id,
}) => {
  const [selectedOptions, setSelectedOptions] = useState([]);
  const [selectedTrainer, setSelectedTrainer] = useState([]);
  const [form, setForm] = useState({});

  const navigate = useNavigate();

  const listSyllabus = useSelector(state => state.SYLLABUS.activeSyllabus);

  const token = localStorage.getItem("ACCESS_TOKEN");

  useEffect(() => {
    setForm({ ...item });

    if (id) {
      setSelectedTrainer({
        value: item?.trainerGmail,
        label: item?.trainerGmail,
      });

      setSelectedOptions(item?.topicCode?.map((e) => ({ value: e, label: e })));
    }
  }, [item, id]);

  const selectOptions = listSyllabus?.map((item) => ({
    value: item.topicCode,
    label: item.topicCode,
  }));

  const handleSelectTrainerChange = (selectedOptions) => {
    setSelectedTrainer(selectedOptions);
    listItem({
      ...form,
      trainerGmail: selectedOptions?.value,
    });
  };

  const handleSelectChange = (selectedOptions) => {
    setSelectedOptions(selectedOptions);
    listItem({
      ...form,
      topicCode: selectedOptions?.map(e => e.value),
    });
  };


  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actUserGetTrainerAsyncAPI(token));
  }, []);

  const trainer = useSelector(state => state.USER.trainer);

  const trainerOptions = trainer?.map((e) => ({
    label: e.email,
    value: e.email,
  }));

  function handleCancel() {
    navigate("/program");
  }

  const { t } = useTranslation();

  return (
    <div>
      <div className={styles.headerTraining}>
        <div>
          <h5>{t("program")}</h5>
        </div>
        <div>
          <h2>{form?.trainingProgramName}</h2>
          <div
            style={{
              marginLeft: "50px",
              marginTop: "3px",
              width: "130px",
            }}
          >
            <p>Active</p>
          </div>
        </div>
      </div>
      <div className={styles.headerInfoTraining}></div>
      <div className={styles.trainingContent}>
        <h3>
          <b>{t("content")}</b>
        </h3>
      </div>
      <div>
        <div style={{ display: "flex", marginBottom: "10px" }}>
          <label
            className="col-lg-1"
            style={{
              color: "black",
              display: "flex",
              justifyContent: "center",
              margin: "auto 0",
            }}
          >
            {t("select_syllabus")}
          </label>{" "}
          <Select
            isMulti
            className="col-lg-3"
            options={selectOptions}
            value={selectedOptions}
            onChange={handleSelectChange}
            placeholder={
              <>
                <ImSearch /> {t("search")}...
              </>
            }
          />
        </div>
        <div style={{ display: "flex" }}>
          <label
            className="col-lg-1"
            style={{
              color: "black",
              display: "flex",
              justifyContent: "center",
              margin: "auto 0",
            }}
          >
            {t("select_trainer")}
          </label>{" "}
          <Select
            className="col-lg-3"
            options={trainerOptions}
            value={selectedTrainer}
            onChange={handleSelectTrainerChange}
            placeholder={
              <>
                <ImSearch /> {t("search")}...
              </>
            }
          />
        </div>
      </div>
      <div className={styles.trainingFooter}>
        <div>
          <button className={styles.btnBack} onClick={handleCancel}>
            {t("back")}
          </button>
        </div>
        <div>
          <button className={styles.btnSave} onClick={handleSaveButton}>
            {t("save")}
          </button>
        </div>
      </div>
    </div>
  );
};
