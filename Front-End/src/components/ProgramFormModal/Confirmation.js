import React from "react";

import ConfirmStyles from "../../css/Confirm.module.css";

function Confirmation({
  showConfirmation,
  handleConfirmDelete,
  handleCloseConfirmation,
  selectedProgramData,
  t,
}) {
  return (
    showConfirmation && (
      <div className={ConfirmStyles.overlay}>
        <div className={ConfirmStyles.popup}>
          <p>{t("delete_program_confirm_message")}</p>
          <div className={ConfirmStyles.btncontainer}>
            <button
              className={ConfirmStyles.btn}
              onClick={() => handleConfirmDelete(selectedProgramData)}
              style={{ color: "white", backgroundColor: "#1b691b" }}
            >
              {t("delete")}
            </button>
            <button
              className={ConfirmStyles.btn}
              onClick={() => handleCloseConfirmation()}
              style={{ color: "white", backgroundColor: "#a72c0d" }}
            >
              {t("cancel")}
            </button>
          </div>
        </div>
      </div>
    )
  );
}

export default Confirmation;
