import React, { useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";
import { actProgramAPIAllGetAsync } from "../../store/program/action";

import ProgramFormSearch from "./ProgramFormSearch";

export const ProgramFormModal = () => {
  let token = localStorage.getItem("ACCESS_TOKEN");

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actProgramAPIAllGetAsync(token));
  }, []);

  const listProgram = useSelector((state) => state.program.programsAll);

  return (
    <div>
      <ProgramFormSearch tableData={listProgram} />
    </div>
  );
};
