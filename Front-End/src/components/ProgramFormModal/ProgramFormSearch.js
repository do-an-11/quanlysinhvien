import React, { useCallback, useEffect, useState } from "react";

import { MDBDataTableV5 } from "mdbreact";
import { Dropdown, DropdownButton } from "react-bootstrap";
import { ImSearch } from "react-icons/im";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import Select from "react-select";
import {
  actProgramDeleteAPIAsync,
  actProgramRecoverAPIAsync,
} from "../../store/program/action";
import { useTranslation } from "react-i18next";

import Confirmation from "./Confirmation";

import styles from "../../css/trainingProgram.module.css";


function ProgramFormSearch({ tableData }) {
  const [isPopupVisible, setIsPopupVisible] = useState(false);
  const [selectedProgramData, setSelectedProgramData] = useState(null);
  const [selectedOptions, setSelectedOptions] = useState([]);
  const [list, setList] = useState([]);

  const [showConfirmation, setShowConfirmation] = useState(false);

  const dispatch = useDispatch();

  const token = localStorage.getItem("ACCESS_TOKEN");

  const listOptions = tableData?.map((e) => {
    return { value: e.name, label: e.name };
  });

  const openPopup = (data) => {
    setSelectedProgramData(data);
    setIsPopupVisible(true);
  };

  const closePopup = useCallback(() => {
    setSelectedProgramData(null);
    setIsPopupVisible(false);
  }, []);

  const handleSelectChange = (selected) => {
    setSelectedOptions(selected);
  };

  useEffect(() => {
    const selectedProgram = selectedOptions.map((option) => option.value);
    let filteredList = tableData.filter((e) =>
      selectedProgram.includes(e.name)
    );
    setList(filteredList);
  }, [selectedOptions]);

  const handleDeleteClick = (id) => {
    setSelectedProgramData(id);
    setShowConfirmation(true);
  };

  const handleConfirmDelete = (id) => {
    dispatch(actProgramDeleteAPIAsync(id, token));
    setShowConfirmation(false);
  };

  const handleCloseConfirmation = () => {
    setShowConfirmation(false);
  };
  const formatTimestamp = (timestamp) => {
    const date = new Date(timestamp);
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const day = date.getDate().toString().padStart(2, "0");

    return `${year}/${month}/${day}`;
  };

  function handleClickRecover(id) {
    dispatch(actProgramRecoverAPIAsync(id, token));
  }

  const { t } = useTranslation();

  return (
    <div className={styles.manager}>
      <div className="row">
        <div className="col-md-3">
          <Select
            isMulti
            options={listOptions}
            value={selectedOptions}
            onChange={handleSelectChange}
            placeholder={
              <>
                <ImSearch /> {t("search")}...
              </>
            }
          />
        </div>
      </div>

      <div className="row" style={{ marginTop: "20px" }}>
        {isPopupVisible && selectedProgramData && (
          <div className="classDetail">
            <div className="col-md-2"></div>
            <div
              className="popupBackground"
              style={{
                width: "100vw",
                height: "100vh",
                backgroundColor: "#0000009d",
                position: "fixed",
                left: 0,
                top: 0,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                zIndex: "500000",
              }}
            >
              <div
                className="popup"
                style={{
                  width: "50%",
                  height: "fit-content",
                  zIndex: "50",
                  border: "2px solid #3d3d3d",
                  borderRadius: "10px",
                }}
              >
                <div
                  className="classInfoHeader"
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between",
                    height: "3rem",
                    backgroundColor: "#2e3647",
                    borderRadius: "10px 10px 0 0",
                    paddingLeft: "17rem",
                  }}
                >
                  <h2 style={{ color: "cyan", alignContent: "center" }}>
                    Program Information
                  </h2>
                  <button
                    onClick={closePopup}
                    className="closeButton"
                    style={{
                      backgroundColor: "#00000000",
                      zIndex: "1001",
                      color: "cyan",
                      fontSize: "40px",
                      fontWeight: "bold",
                      border: "none",
                      padding: "10px",
                    }}
                  >
                    &times;
                  </button>
                </div>

                <div
                  class="classInfoBody"
                  style={{
                    display: "flex",
                    borderRadius: " 0 0 10px 10px",
                  }}
                >
                  <div class="col-md-4">
                    <div
                      className="row1"
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        fontSize: "30px",
                        backgroundColor: "white",
                        border: "2px solid #3d3d3d",
                        paddingLeft: "10px",
                      }}
                    >
                      <p>Program Name </p>
                      <p>Created On </p>
                      <p>Created By </p>
                      <p>Duration </p>
                      <p>Status </p>
                    </div>
                  </div>

                  <div class="col-md-8" style={{ height: "100%" }}>
                    <div
                      className="row2"
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        fontSize: "30px",
                        backgroundColor: "white",
                        border: "2px solid #3d3d3d",
                        height: "100%",
                        paddingLeft: "10px",
                      }}
                    >
                      <p style={{ minHeight: "37.5px", color: "black" }}>
                        {selectedProgramData.name}
                      </p>
                      <p style={{ minHeight: "37.5px", color: "black" }}>
                        {selectedProgramData.createdDate}
                      </p>
                      <p style={{ minHeight: "37.5px", color: "black" }}>
                        {selectedProgramData.createdBy}
                      </p>
                      <p style={{ minHeight: "37.5px", color: "black" }}>
                        {selectedProgramData.duration}
                      </p>
                      <p style={{ minHeight: "37.5px", color: "black" }}>
                        {selectedProgramData.status}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-2"></div>
          </div>
        )}
        <Confirmation
          showConfirmation={showConfirmation}
          handleConfirmDelete={handleConfirmDelete}
          handleCloseConfirmation={handleCloseConfirmation}
          selectedProgramData={selectedProgramData}
          t={t}
        />
        <div className={styles.tableContainer}>
          <MDBDataTableV5
            className={styles.customTable}
            data={{
              columns: [
                {
                  label: t("program"),
                  field: "program",
                  width: 200,
                },
                {
                  label: t("create_by"),
                  field: "createBy",
                  width: 200,
                },
                {
                  label: t("duration"),
                  field: "Duration",
                  width: 150,
                },
                {
                  label: t("status"),
                  field: "status",
                  width: 100,
                },
                {
                  label: t("options"),
                  field: "option",
                  width: 100,
                },
              ],
              rows:
                list.length > 0
                  ? list.map((data) => ({
                      program: (
                        <Link
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            cursor: "pointer",
                          }}
                          onClick={() => openPopup(data)}
                        >
                          {data.name}
                        </Link>
                      ),
                      createOn: formatTimestamp(data.createdDate),
                      createBy: data.createdBy,
                      Duration: `${Math.floor(data.duration / 86400)} days`,
                      status:
                        data.status === true
                          ? "active"
                          : "inactive" && (
                              <span
                                style={{
                                  padding: "5px 10px",
                                  color:
                                    data.status === "active"
                                      ? "rgb(17, 159, 9)"
                                      : data.status === "inactive"
                                      ? "rgb(175, 148, 87)"
                                      : data.status === "drafting"
                                      ? "#285d9a"
                                      : "#b9b9b9",
                                  borderRadius: "20px",
                                  backgroundColor:
                                    data.status === "active"
                                      ? "rgb(159, 236, 239)"
                                      : data.status === "inactive"
                                      ? "rgb(255, 236, 198)"
                                      : data.status === "drafting"
                                      ? "#285d9a"
                                      : "#b9b9b9",
                                }}
                              >
                                {data.status}
                              </span>
                            ),
                      option:
                        data?.status === "active" ? (
                          <div style={{ maxWidth: "40px", maxHeight: "30px" }}>
                            <DropdownButton>
                              <Dropdown.Item className="editBtn">
                                <Link
                                  to={`/createprogram/${data.trainingProgramCode}`}
                                >
                                  {t("edit_program")}
                                </Link>
                              </Dropdown.Item>
                              <Dropdown.Item
                                style={{
                                  marginTop: "5px",
                                }}
                                onClick={() => {
                                  handleDeleteClick(data.trainingProgramCode);
                                }}
                              >
                                {t("delete_program")}
                              </Dropdown.Item>
                            </DropdownButton>
                          </div>
                        ) : (
                          <div style={{ maxWidth: "40px", maxHeight: "30px" }}>
                            <DropdownButton>
                              <Dropdown.Item
                                className="recoverBtn"
                                onClick={() =>
                                  handleClickRecover(data.trainingProgramCode)
                                }
                              >
                                {t("recover")}
                              </Dropdown.Item>
                            </DropdownButton>
                          </div>
                        ),
                    }))
                  : tableData.map((data) => ({
                      program: (
                        <Link
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            cursor: "pointer",
                          }}
                          onClick={() => openPopup(data)}
                        >
                          {data.name}
                        </Link>
                      ),
                      createOn: formatTimestamp(data.createdDate),
                      createBy: data.createdBy,
                      Duration: data.duration,
                      status:
                        data.status === true
                          ? "active"
                          : "inactive" && (
                              <span
                                style={{
                                  padding: "5px 10px",
                                  color:
                                    data.status === "active"
                                      ? "rgb(17, 159, 9)"
                                      : data.status === "inactive"
                                      ? "rgb(175, 148, 87)"
                                      : "#b9b9b9",
                                  borderRadius: "20px",
                                  backgroundColor:
                                    data.status === "active"
                                      ? "rgb(159, 236, 239)"
                                      : data.status === "inactive"
                                      ? "rgb(255, 236, 198)"
                                      : "#b9b9b9",
                                }}
                              >
                                {data.status}
                              </span>
                            ),
                      option:
                        data?.status === "active" ? (
                          <div style={{ maxWidth: "40px", maxHeight: "30px" }}>
                            <DropdownButton>
                              <Dropdown.Item
                                style={{
                                  color: "#647487",
                                  backgroundColor: "rgb(159, 236, 239)",
                                }}
                              >
                                <Link
                                  style={{ color: "#647487" }}
                                  to={`/createprogram/${data.trainingProgramCode}`}
                                >
                                  {t("edit_program")}
                                </Link>
                              </Dropdown.Item>
                              <Dropdown.Item
                                style={{
                                  color: "#c91c2d",
                                  backgroundColor: "rgb(159, 236, 239)",
                                  marginTop: "5px",
                                }}
                                onClick={() => {
                                  handleDeleteClick(data.trainingProgramCode);
                                }}
                              >
                                {t("delete_program")}
                              </Dropdown.Item>
                            </DropdownButton>
                          </div>
                        ) : (
                          <div style={{ maxWidth: "40px", maxHeight: "30px" }}>
                            <DropdownButton>
                              <Dropdown.Item
                                onClick={() =>
                                  handleClickRecover(data.trainingProgramCode)
                                }
                              >
                                {t("recover")}
                              </Dropdown.Item>
                            </DropdownButton>
                          </div>
                        ),
                    })),
            }}
            hover
            entriesOptions={[5, 10, 20]}
            entries={10}
            pagesAmount={5}
            paging={true}
            searchTop={false}
            searchBottom={false}
          />
        </div>
      </div>
    </div>
  );
}

export default ProgramFormSearch;
