import React, { useEffect, useState } from "react";

import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { Button, Form, Modal } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import Switch from "react-switch";

import { toast } from "react-toastify";

import {
  actPostUserAsyncAPI,
  actUserGetAsyncAPI,
  actUserUpdateAsyncAPI,
} from "../../store/user/action";

import { useTranslation } from "react-i18next";

const UserFormModal = ({ show, handleClose }) => {
  const [formData, setFormData] = useState({
    role: "",
    name: "",
    email: "",
    phone: "",
    dob: "",
    gender: "",
    status: "",
  });
  const [isPhoneValid, setIsPhoneValid] = useState(true);
  const currentUser = useSelector((state) => state.USER.currentUser);
  const token = localStorage.getItem("ACCESS_TOKEN");
  const role = useSelector((state) => state.USER.role);

  const [isLoading, setIsLoading] = useState(false);
  const [formErrors, setFormErrors] = useState({});
  const allUser = useSelector((state) => state.USER.users);
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isEmailFieldTouched, setIsEmailFieldTouched] = useState(false);
  const [isStatusTouched, setIsStatusTouched] = useState(false);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const param = useParams();
  let id = param.id;
  useEffect(() => {
    if (id) {
      setFormErrors({
        dob: "",
        email: "",
        gender: "",
        name: "",
        phone: "",
        role: "",
        status: "",
      });
    }
    if (allUser?.users && allUser.users.length > 0) {
      let item = allUser?.users?.find((e) => e.email === id);
      if (item) {
        setFormData(item);
      }
    }
  }, [id, allUser.users]);
  useEffect(() => {
    dispatch(actUserGetAsyncAPI(token));
  }, []);

  const isPhoneNumberValid = (phone) => {
    const numericPhone = phone.replace(/\D/g, "");
    return numericPhone.length === 10;
  };

  const validateForm = () => {
    const errors = {};
    if (!formData?.role) {
      errors.role = "role is required";
    }
    if (!formData?.name) {
      errors.name = "Name is required";
    }
    if (!formData?.email) {
      errors.email = "Email is required";
    }
    if (!formData?.phone) {
      errors.phone = "Phone is required";
    }
    if (!formData?.dob) {
      errors.dob = "Date of Birth is required";
    }
    if (!formData?.gender) {
      errors.gender = "Gender is required";
    }
    if (!formData?.status) {
      errors.status = "Status is required";
    }

    setFormErrors(errors);

    return Object.keys(errors).length === 0;
  };

  const handleChange = (e) => {
    const { name, value, type, checked } = e.target;
    if (type === "radio") {
      if (checked) {
        setFormData({ ...formData, [name]: value });
        setFormErrors((prevErrors) => {
          return { ...prevErrors, [name]: "" };
        });
      }
    } else {
      setFormData({ ...formData, [name]: value });
      setFormErrors((prevErrors) => {
        return { ...prevErrors, [name]: "" };
      });
    }
    if (name === "email") {
      const isValid = isEmailValidFormat(value);
      setIsEmailValid(isValid);
    }
    if (name === "phone") {
      const isValid = isPhoneNumberValid(value);
      setIsPhoneValid(isValid);
    }
  };

  const handleSwitchChange = (checked) => {
    setFormData({ ...formData, status: checked ? true : false });
    setIsStatusTouched(true);
  };

  const isEmailValidFormat = (email) => {
    const emailPattern = /^[a-zA-Z0-9._%+-]+@gmail(\.[a-zA-Z]{2,})?$/;
    return emailPattern.test(email);
  };

  const handleSubmit = () => {
    const isValid = validateForm();
    const isEmailValid = isEmailValidFormat(formData?.email);
    const isPhoneValid = isPhoneNumberValid(formData?.phone);
    if (id) {
      formData.modifiedBy = currentUser?.name;
      dispatch(actUserUpdateAsyncAPI(id, formData, token)).then(() => {
        setIsLoading(false);
      });
      handleClose();
      setFormData({
        role: "",
        name: "",
        email: "",
        phone: "",
        dob: "",
        gender: "",
        status: "",
      });
      navigate("/usermanage");
    } else {
      if (isValid && isEmailValid && isPhoneValid) {
        setIsLoading(true);
        formData.createdBy = currentUser?.name;
        formData.modifiedBy = currentUser?.name;
        dispatch(actPostUserAsyncAPI(formData, token)).then(() => {
          setIsLoading(false);
        });
        handleClose();
        setFormData({
          role: "",
          name: "",
          email: "",
          phone: "",
          dob: "",
          gender: "",
          status: "",
        });
        navigate("/usermanage");
      } else if (formData.status !== true) {
        toast.error("Status must be checked to submit the form.");
      } else {
        toast.error("Form is not valid. Please check the errors.");
      }
    }
  };

  const handleCloseForm = () => {
    setFormData({
      role: "",
      name: "",
      email: "",
      phone: "",
      dob: "",
      gender: "",
      status: "",
    });
    navigate("/usermanage");
    handleClose();
  };

  const { t } = useTranslation();

  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header
          style={{
            color: "white",
            backgroundColor: "#2e3647",
            padding: "1rem",
          }}
        >
          <Modal.Title>Create User</Modal.Title>
          <button
            className="close"
            style={{
              color: "white",
              background: "transparent",
              border: "none",
              padding: 0,
              cursor: "pointer",
              position: "absolute",
              top: 0,
              right: 0,
              margin: "1rem",
            }}
            onClick={handleClose}
          >
            <FontAwesomeIcon icon={faTimes} />
          </button>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={(e) => handleSubmit(e, formData)}>
            <Form.Group controlId="role">
              <Form.Label style={{ color: "black" }}>{t("type")}</Form.Label>
              <Form.Select
                name="role"
                value={formData?.role}
                onChange={handleChange}
              >
                <option value="0">---Choose---</option>
                {role !== "CLASS_ADMIN" && (
                  <option value="SUPER_ADMIN">Super Admin</option>
                )}
                <option value="CLASS_ADMIN">Admin</option>
                <option value="TRAINER">Trainer</option>
                <option value="USER">Trainee</option>
              </Form.Select>
              {formErrors.role && (
                <div className="text-danger">{formErrors.role}</div>
              )}
            </Form.Group>
            <Form.Group controlId="name">
              <Form.Label style={{ color: "black" }}>{t("name")}</Form.Label>
              <Form.Control
                type="text"
                name="name"
                value={formData?.name}
                onChange={handleChange}
              />
              {formErrors.name && (
                <div className="text-danger">{formErrors.name}</div>
              )}
            </Form.Group>
            {!id && (
              <Form.Group controlId="email">
                <Form.Label style={{ color: "black" }}>{t("email")}</Form.Label>
                <Form.Control
                  type="email"
                  name="email"
                  value={formData?.email}
                  onChange={handleChange}
                  onBlur={() => setIsEmailFieldTouched(true)}
                />
                {isEmailFieldTouched && formErrors?.email && (
                  <div className="text-danger">{formErrors?.email}</div>
                )}
                {!id && !isEmailValid && formData.email !== "" && (
                  <div className="text-danger">
                    Please enter a valid email address.
                  </div>
                )}
              </Form.Group>
            )}
            <Form.Group controlId="phone">
              <Form.Label style={{ color: "black" }}>{t("phone")}</Form.Label>
              <Form.Control
                type="text"
                name="phone"
                value={formData?.phone}
                onChange={handleChange}
              />
              {isPhoneValid || (
                <div className="text-danger">
                  Phone number must have 10 digits
                </div>
              )}
            </Form.Group>
            <Form.Group controlId="dob">
              <Form.Label style={{ color: "black" }}>{t("dob")}</Form.Label>
              <Form.Control
                type="date"
                name="dob"
                value={formData?.dob}
                onChange={handleChange}
                max={new Date().toISOString().split("T")[0]}
              />
              {formErrors?.dob && (
                <div className="text-danger">{formErrors?.dob}</div>
              )}
            </Form.Group>
            <Form.Group controlId="gender">
              <Form.Label style={{ color: "black" }}>{t("gender")}</Form.Label>
              <div>
                <Form.Check
                  inline
                  type="radio"
                  label="Male"
                  name="gender"
                  value="male"
                  checked={formData?.gender === "male"}
                  onChange={handleChange}
                />
                <Form.Check
                  inline
                  type="radio"
                  label="Female"
                  name="gender"
                  value="female"
                  checked={formData?.gender === "female"}
                  onChange={handleChange}
                />
              </div>
              {formErrors?.gender && (
                <div className="text-danger">{formErrors?.gender}</div>
              )}
            </Form.Group>
            <Form.Group
              controlId="status"
              style={{ display: "flex", alignItems: "center" }}
            >
              <Form.Label style={{ color: "black" }}> {t("status")}</Form.Label>
              <Switch
                name="status"
                className="status-switch"
                checked={formData?.status}
                onChange={handleSwitchChange}
              />
              {formData.status === false &&
                (formErrors?.status || "Status is required") && (
                  <div className="text-danger">
                    {formErrors?.status || "Status is required"}
                  </div>
                )}
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseForm}>
            {t("close")}
          </Button>
          <Button variant="primary" onClick={() => handleSubmit(formData)}>
            {t("save")}
          </Button>
        </Modal.Footer>
      </Modal>
      {isLoading ? (
        <>
          <FontAwesomeIcon
            style={{
              fontSize: 30,
              position: "fixed",
              right: "75px",
              color: "blue",
              marginTop: "10px",
            }}
            icon={faSpinner}
            spin
          />
        </>
      ) : (
        ""
      )}
    </>
  );
};

export default UserFormModal;
