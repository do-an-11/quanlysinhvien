import React, { useEffect, useState } from "react";

import { faCheck, faUpload } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useTranslation } from "react-i18next";
import Modal from "react-modal";
import { useDispatch, useSelector } from "react-redux";
import Select from "react-select";
import styles from "../../css/class.module.css";
import { actSyllabusGetAsync } from "../../store/class/action";
import { actProgramAPINewGetAsync } from "../../store/program/action";

const modalStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

Modal.setAppElement("#root");

export const TrainingProgram = ({ listItem, item, id }) => {
  const [trainers, setTrainers] = useState([]);
  const [searchText, setSearchText] = useState("");
  const [options, setOptions] = useState([]);
  const [selectedCourseName, setSelectedCourseName] = useState(null);
  const [filteredTrainingData, setFilteredTrainingData] = useState([]);
  const [selectedItems, setSelectedItems] = useState([]);
  const [form, setForm] = useState({});
  const [isTrainerModalOpen, setIsTrainerModalOpen] = useState(false);
  const [selectedItemToEdit, setSelectedItemToEdit] = useState(null);
  const [selectedTrainer, setSelectedTrainer] = useState([]);
  const [selectedTest, setSelectedTest] = useState("");

  const token = localStorage.getItem("ACCESS_TOKEN");

  const openTrainerModal = () => {
    setIsTrainerModalOpen(true);
  };

  const closeTrainerModal = () => {
    setIsTrainerModalOpen(false);
  };

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actSyllabusGetAsync());
  }, []);

  useEffect(() => {
    setForm({ ...item });
    if (Array.isArray(item?.trainer)) {
      const list = item?.trainer?.map((e) => {
        return { label: e.gmail, value: e.gmail };
      });
      setTrainers(list);
    }
  }, [item]);

  const handleSelectChange = (selectedOption) => {
    listItem({
      ...form,
      trainingProgram: selectedOption.label,
    });
    const update = form?.trainer?.map((e) => {
      return { gmail: e.gmail, classCode: [] };
    });
    if (selectedOption.label !== form?.trainingProgram) {
      listItem({
        ...form,
        trainingProgram: selectedOption.label,
        trainer: update,
      });
    }
  };

  useEffect(() => {
    dispatch(actProgramAPINewGetAsync(token));
  }, []);

  const listProgram = useSelector((state) => state.program.programsNew);

  useEffect(() => {
    if (id && item) {
      setSelectedCourseName(item?.trainingProgram);
    }
  }, [id, item]);

  useEffect(() => {
    const apiOptions = listProgram.map((e) => ({
      label: e.title,
      value: e.id,
    }));
    setOptions(apiOptions);
  }, [listProgram]);

  useEffect(() => {
    if (item && item.trainer && item.trainer.length > 0) {
      const newSelectedItems = item.trainer.map((trainer) => trainer.classCode);
      const filteredArray = newSelectedItems.filter((item) =>
        Array.isArray(item)
      );
      const flatArray = [].concat(...filteredArray);
      setSelectedItems(flatArray);
    }
  }, [item]);

  useEffect(() => {
    if (!form?.trainingProgram) {
      setSelectedCourseName(form?.trainingProgram);
    }
    const filteredCourse = listProgram.find(
      (course) => course.title === form?.trainingProgram
    );
    if (filteredCourse) {
      const filteredResults = filteredCourse?.list?.filter((item) =>
        item?.title?.toLowerCase()?.includes(searchText.toLowerCase())
      );
      setFilteredTrainingData(filteredResults);
      if (filteredTrainingData && searchText) {
        let data = filteredTrainingData.filter((e) =>
          e.title.includes(searchText)
        );
        setFilteredTrainingData(data);
      }
    }
  }, [form, searchText]);

  const handleCheckButton = (item) => {
    setSelectedTest(item);
    if (Array.isArray(selectedItems)) {
      if (selectedItems.includes(item)) {
        if (window.confirm(`You want to delete syllabus`) == true) {
          const updatedItems = selectedItems.filter(
            (selectedItem) => selectedItem !== item
          );
          setSelectedItems(updatedItems);
          let update = form?.trainer?.map((e) => {
            let data = e?.classCode?.filter((i) => i !== item);
            return { ...e, classCode: data };
          });
          listItem({
            ...form,
            trainer: update,
          });
        } else {
          return;
        }
      } else {
        openTrainerModal();
      }
    } else {
      setSelectedItems([item]);
    }
  };

  const handleTrainerSelect = (item, selectedTrainers) => {
    let updatedForm = { ...form };
    let updatedTrainingSyllabus = [...form?.trainer];
    const selectedTrainerValues = selectedTrainers.map(
      (trainer) => trainer.value
    );

    selectedTrainerValues.forEach((selectedTrainer) => {
      const trainerIndex = updatedTrainingSyllabus.findIndex(
        (trainer) => trainer.gmail === selectedTrainer
      );
      if (trainerIndex !== -1) {
        const trainer = updatedTrainingSyllabus[trainerIndex];
        if (Array.isArray(trainer.classCode)) {
          trainer.classCode.push(item);
          setSelectedTrainer([]);
        } else {
          trainer.classCode = [item];
          setSelectedTrainer([]);
        }
      } else {
        updatedTrainingSyllabus.push({
          gmail: selectedTrainer,
          classCode: [item],
        });
      }
    });
    updatedForm.trainer = updatedTrainingSyllabus;
    listItem(updatedForm);
  };

  const { t } = useTranslation();

  return (
    <div>
      <Modal
        isOpen={isTrainerModalOpen}
        onRequestClose={closeTrainerModal}
        style={modalStyles}
      >
        <h2>Select Trainer for {selectedItemToEdit?.title}</h2>
        <Select
          isMulti
          options={trainers}
          value={selectedTrainer}
          onChange={(selectedOption) => setSelectedTrainer(selectedOption)}
        />
        <button
          onClick={closeTrainerModal}
          style={{
            backgroundColor: "red",
            marginRight: "10px",
            marginTop: "10px",
            borderRadius: "10px",
            padding: "3px 10px",
            color: "white",
          }}
        >
          {t("cancel")}
        </button>
        <button
          onClick={() => {
            handleTrainerSelect(selectedTest, selectedTrainer);
            closeTrainerModal();
          }}
          style={{
            backgroundColor: "green",
            marginTop: "10px",
            borderRadius: "10px",
            padding: "3px 10px",
          }}
        >
          {t("save")}
        </button>
      </Modal>

      <div className={styles.header_content}>
        <div>{t("training_program_name")}</div>
        <div>
          <Select
            className={styles.showCourse}
            options={options}
            onChange={handleSelectChange}
            value={{
              label: form?.trainingProgram,
              value: form?.trainingProgram,
            }}
          />
        </div>
      </div>
      <div style={{ marginBottom: "10px" }}>
        <div>
          <div className={styles.trainingHeader}>
            <div>
              <div>
                <div style={{ fontSize: "30px", fontFamily: "A" }}>
                  {t("training_program_of")}{" "}
                  <b style={{ textDecoration: "underline" }}>
                    {form?.nameClass}
                  </b>
                </div>
                <div style={{ display: "flex" }}>
                  <>
                    <b style={{ fontSize: "40px" }}>{form?.trainingProgram}</b>
                    <p>Inactive</p>
                  </>
                </div>
              </div>
            </div>
          </div>
          <div className={styles.trainingContent}>
            <h3>
              <b>{t("content")}</b>
            </h3>
            {filteredTrainingData &&
              filteredTrainingData?.map((item) => {
                const isSelected = selectedItems?.includes(item?.nameCode);
                return (
                  <div key={item.id} className={styles.trainingBody}>
                    <div className="col-lg-11">
                      <div style={{ display: "flex", height: "100%" }}>
                        <div className={`col-lg-3 ${styles.upload}`}>
                          <div>
                            <FontAwesomeIcon icon={faUpload} />
                          </div>
                        </div>
                        <div className={`col-lg-9 ${styles.contentTrain}`}>
                          <div className="col-lg-4" style={{ display: "flex" }}>
                            <h3>{item.title}</h3>
                            <div
                              className="col-lg-3"
                              style={{
                                margin: "5px 0 0 20px",

                                background: "#002761",
                                color: "white",
                                height: "25px",
                                textAlign: "center",
                                borderRadius: "20px",
                              }}
                            >
                              Active
                            </div>
                          </div>
                          <div style={{ display: "flex" }}></div>
                        </div>
                      </div>
                    </div>
                    <div
                      className={
                        isSelected
                          ? `col-lg-1 ${styles.btnCheck} `
                          : `col-lg-1 ${styles.btnCheckFirst} `
                      }
                      onClick={() => handleCheckButton(item.nameCode)}
                    >
                      <div>
                        <FontAwesomeIcon icon={faCheck} />
                      </div>
                    </div>
                  </div>
                );
              })}
          </div>
          <div className={styles.trainingAction}>
            <div className={`col-lg-6 ${styles.searchTraining}`}>
              <b style={{ marginRight: "20px" }}>{t("select")}</b>
              <input
                placeholder={t("search")}
                value={searchText}
                onChange={(e) => setSearchText(e.target.value)}
              />
            </div>
          </div>
        </div>
      </div>
      <div className={styles.footer_content}></div>
    </div>
  );
};
