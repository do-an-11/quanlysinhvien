import React, { useState } from "react";

import styles from "../../css/class.module.css";
import { useTranslation } from "react-i18next";

const CreateClass = ({ open, value, id }) => {
  const [className, setClassName] = useState("");
  const [alert, setAlert] = useState("");

  const handleCreateButton = () => {
    if (className.trim() !== "") {
      open(true);
      value(className);
      setClassName("");
      setAlert("");
    } else {
      setAlert(t("enter_value"));
    }
  };

  const { t } = useTranslation();

  return (
    <div className={styles.containerClass}>
      {id ? (
        <h1 style={{ marginLeft: "40px" }}>{t("update_class")}</h1>
      ) : (
        <h1 style={{ marginLeft: "40px" }}>{t("create_class")}</h1>
      )}
      <div className={styles.createClass}>
        <input
          type="text"
          placeholder={t("type_class_name")}
          value={className}
          onChange={(e) => setClassName(e.target.value)}
        />
        <button onClick={handleCreateButton}>
          {id === undefined ? t("create") : t("update")}
        </button>
        <div style={{ marginTop: "3px", color: "red" }}>{alert}</div>
      </div>
    </div>
  );
};

export default CreateClass;
