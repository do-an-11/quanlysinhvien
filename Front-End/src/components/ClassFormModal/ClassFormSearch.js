import React, { useCallback, useEffect, useState } from "react";
import _ from "lodash";
import { MDBDataTableV5 } from "mdbreact";
import { Dropdown, DropdownButton } from "react-bootstrap";
import { ImSearch } from "react-icons/im";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import Select from "react-select";
import { actClassDeleteAsyncAPI } from "../../store/class/action";
import FilterComponent from "../FIlter";
import ConfirmationPopup from "./Confirmation";

import styles from "../../css/class.module.css";
import "../../css/button.css";
import { useTranslation } from "react-i18next";

function ClassFormSearch({ tableData }) {
  const [isPopupVisible, setIsPopupVisible] = useState(false);
  const [selectedClassData, setSelectedClassData] = useState(null);
  const [selectedOptions, setSelectedOptions] = useState([]);
  const [list, setList] = useState([]);
  const [listClass, setListClass] = useState([]);
  let token = localStorage.getItem("ACCESS_TOKEN");

  const dispatch = useDispatch();
  const listOptions = tableData?.map((e) => {
    return { value: e.nameClass, label: e.nameClass };
  });

  const closePopup = useCallback(() => {
    setSelectedClassData(null);
    setIsPopupVisible(false);
  }, []);
  const openPopup = (data) => {
    setSelectedClassData(data);
    setIsPopupVisible(true);
  };

  const handleSelectChange = (selected) => {
    setSelectedOptions(selected);
  };
  const handleClearFilters = () => {};

  const [isConfirmationVisible, setIsConfirmationVisible] = useState(false);
  const [confirmationMessage, setConfirmationMessage] = useState("");
  const [deleteId, setDeleteId] = useState(null);

  
  useEffect(() => {
    setListClass(tableData);
  }, [tableData]);

  useEffect(() => {
    const selectedClass = selectedOptions.map((option) => option.value);
    let filteredList = tableData.filter((e) =>
      selectedClass.includes(e.nameClass)
    );
    setList(filteredList);
  }, [selectedOptions]);

  function handleDeleteClick(id) {
    if (window.confirm(`You want to delete Class id: ${id}`) == true) {
      dispatch(actClassDeleteAsyncAPI(id, token));
    } else {
      return;
    }
  }

  function handleDeleteClick(id) {
    setDeleteId(id);
    setConfirmationMessage(`You want to delete Class id: ${id}`);
    setIsConfirmationVisible(true);
  }

  const handleConfirmDelete = () => {
    if (deleteId) {
      dispatch(actClassDeleteAsyncAPI(deleteId, token));
      setIsConfirmationVisible(false);
      setDeleteId(null);
    }
  };

  const handleCancelDelete = () => {
    setIsConfirmationVisible(false);
    setDeleteId(null);
  };

  const handeleFilter = (handeleFilter) => {
    let selectedLocation = handeleFilter.selectedLocation.map((e) => e.value);
    if (selectedLocation && selectedLocation.length > 0) {
      let cloneListClass = _.cloneDeep(tableData);
      cloneListClass = cloneListClass.filter((item) =>
        selectedLocation.includes(item.location)
      );
      if (cloneListClass) {
        let newstartDate = new Date(handeleFilter.startDate).getTime();
        let newendDate = new Date(handeleFilter.endDate).getTime();
        let cloneListClass1 = cloneListClass.filter((e) => {
          let newStart = new Date(e.startDate).getTime();
          let newEnd = new Date(e.endDate).getTime();
          if (newStart >= newstartDate && newEnd <= newendDate) {
            return e;
          }
        });
        if (cloneListClass1) {
          let cloneListClass2 = cloneListClass1.filter((e) => {
            if (handeleFilter.isCheckedMorning === true) {
              if (e.classTimeFrom >= "06:00" && e.classTimeTo <= "12:00") {
                return e;
              }
            }
            if (handeleFilter.isCheckedNoon === true) {
              if (e.classTimeFrom >= "12:00" && e.classTimeTo <= "18:00") {
                return e;
              }
            }
            if (handeleFilter.isCheckedNight === true) {
              if (e.classTimeFrom >= "18:00" && e.classTimeTo <= "23:59") {
                return e;
              }
            }
          });
          if (cloneListClass2) {
            let cloneListClass3 = cloneListClass2.filter((e) => {
              if (handeleFilter.isCheckedPlanning === true) {
                if (e.status === "Planning") {
                  return e;
                }
              }
              if (handeleFilter.isCheckedScheduled === true) {
                if (e.status === "Scheduled") {
                  return e;
                }
              }
              if (handeleFilter.isCheckedOpening === true) {
                if (e.status === "Opening") {
                  return e;
                }
              }
              if (handeleFilter.isCheckedCompleted === true) {
                if (e.status === "Completed") {
                  return e;
                }
              }
            });
            if (cloneListClass3) {
              let selectedFsu = handeleFilter.selectedFsu.value;
              let cloneListClass4 = cloneListClass3.filter((item) =>
                selectedFsu.includes(item.fsu)
              );
              if (cloneListClass4) {
                let selectedTrainer = handeleFilter.selectedTrainer.map(
                  (e) => e.value
                );
                let cloneListClass5 = cloneListClass4.filter((item) => {
                  return item.trainer.some((e) =>
                    selectedTrainer.includes(e.gmail)
                  );
                });
                setListClass(cloneListClass5);
              }
            }
          }
        }
      }
    } else {
      setListClass(_.cloneDeep(tableData));
    }
  };
  const { t } = useTranslation();

  return (
    <div className={styles.manager}>
      <div className="row">
        <div
          className="search and filter"
          style={{ display: "flex", columnGap: "2rem" }}
        >
          <Select
            className="col-md-2"
            isMulti
            options={listOptions}
            value={selectedOptions}
            onChange={handleSelectChange}
            placeholder={
              <>
                <ImSearch /> {t("search")}...
              </>
            }
          />
          <div className="col-md-10">
            <FilterComponent
              handleClear={handleClearFilters}
              handeleFilter={handeleFilter}
            />
          </div>
        </div>
      </div>

      <div className="row" style={{ marginTop: "20px" }}>
        {isPopupVisible && selectedClassData && (
          <div className="classDetail">
            <div className="col-md-2"></div>
            <div className={styles.popupBackground} style={{ display: "flex" }}>
              <div className="col-md-2"></div>
              <div className="col-md-8">
                <div className={styles.popup}>
                  <div className={styles.classInfoHeader}>
                    <span> </span>
                    <h2>Class Information</h2>
                    <button onClick={closePopup} className={styles.closeButton}>
                      &times;
                    </button>
                  </div>
                  <div class={styles.classInfoBody}>
                    <div class="col-md-4">
                      <div
                        className="row1"
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          fontSize: "25px",
                          borderRight: "2px solid var(--color-dark)",
                        }}
                      >
                        <p className="boldText">Class Name:</p>
                        <p className="boldText">Class Code:</p>
                        <p className="boldText">Created On:</p>
                        <p className="boldText">Created By: </p>
                        <p className="boldText">Duration: </p>
                        <p className="boldText">Status: </p>
                        <p className="boldText">Location: </p>
                        <p className="boldText">FSU: </p>
                      </div>
                    </div>
                    <div className="col-md-8">
                      <div
                        className="row2"
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          fontSize: "25px",
                          color: "black",
                        }}
                      >
                        <p>{selectedClassData.nameClass}</p>
                        <p>{selectedClassData.classCode}</p>
                        <p>{selectedClassData.createdDate}</p>
                        <p>{selectedClassData.created.userName}</p>
                        <p>{selectedClassData.totalTimeLearning}</p>
                        <p>
                          {selectedClassData.status && (
                            <span
                              style={{
                                padding: " 1px",
                                color: "white",
                                textAlign: "center",

                                backgroundColor:
                                  selectedClassData.status === "Planning"
                                    ? "#285d9a"
                                    : selectedClassData.status === "Opening"
                                      ? "#d45b13"
                                      : selectedClassData.status === "Completed"
                                        ? "#2d3748"
                                        : selectedClassData.status === "Scheduled"
                                          ? "#2f903f"
                                          : "white",
                              }}
                            >
                              {selectedClassData.status}
                            </span>
                          )}
                        </p>

                        <p>{selectedClassData.location}</p>
                        <p>{selectedClassData.fsu}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-2"></div>
            </div>
          </div>
        )}
        {isConfirmationVisible && (
        <ConfirmationPopup
          message={confirmationMessage}
          onCancel={handleCancelDelete}
          onConfirm={handleConfirmDelete}
        />
      )}

        <div className={styles.tableContainer}>
          <MDBDataTableV5
            className={styles.customTable}
            data={{
              columns: [
                {
                  label: t("class"),
                  field: "class",
                  width: 200,
                },
                {
                  label: t("class_code"),
                  field: "classCode",
                  width: 150,
                },

                {
                  label: t("create_by"),
                  field: "createBy",
                  width: 200,
                },

                {
                  label: t("status"),
                  field: "status",
                  width: 100,
                },
                {
                  label: t("location"),
                  field: "location",
                  width: 150,
                },
                {
                  label: t("fsu"),
                  field: "fsu",
                  width: 150,
                },
                {
                  label: t("options"),
                  field: "option",
                  width: 100,
                },
              ],
              rows:
                list.length > 0
                  ? list.map((data) => ({
                    class: (
                      <Link onClick={() => openPopup(data)}>
                        {data.nameClass}
                      </Link>
                    ),
                    classCode: data.classCode,
                    createBy: data.created,
                    status: data.status && (
                      <span
                        style={{
                          padding: "5px 10px",
                          color: "white",
                          borderRadius: "20px",
                          backgroundColor:
                            data.status === "Planning"
                              ? "#285d9a"
                              : data.status === "Opening"
                                ? "#d45b13"
                                : data.status === "Completed"
                                  ? "#2d3748"
                                  : data.status === "Scheduled"
                                    ? "#2f903f"
                                    : "white",
                        }}
                      >
                        {data.status}
                      </span>
                    ),
                    location: data.location,
                    fsu: data.fsu,
                    option: (
                      <div
                        style={{
                          maxWidth: "40px",
                          maxHeight: "30px",
                        }}
                      >
                        <DropdownButton>
                          <Dropdown.Item>
                            <Link
                              to={`/createclass/${data?.classCode +
                                "_" +
                                (data?.location).trim().toLowerCase()
                                }`}
                            >
                              Edit
                            </Link>
                          </Dropdown.Item>
                          <Dropdown.Item
                            style={{
                              marginTop: "5px",
                            }}
                            onClick={() => {
                              handleDeleteClick(
                                data?.classCode +
                                "_" +
                                (data?.location).trim().toLowerCase()
                              );
                            }}
                          >
                            Delete
                          </Dropdown.Item>
                        </DropdownButton>
                      </div>
                    ),
                  }))
                  : listClass.map((data) => ({
                    class: (
                      <Link onClick={() => openPopup(data)}>
                        {data.nameClass}
                      </Link>
                    ),
                    classCode: data.classCode,
                    createBy: data.created,
                    status: data.status && (
                      <span
                        style={{
                          padding: "5px 10px",
                          color: "white",
                          borderRadius: "20px",
                          backgroundColor:
                            data.status === "Planning"
                              ? "#285d9a"
                              : data.status === "Opening"
                                ? "#d45b13"
                                : data.status === "Completed"
                                  ? "#2d3748"
                                  : data.status === "Scheduled"
                                    ? "#2f903f"
                                    : "white",
                        }}
                      >
                        {data.status}
                      </span>
                    ),

                    location: data.location + " ",
                    fsu: data.fsu + " ",
                    option: (
                      <div
                        style={{
                          maxWidth: "40px",
                          maxHeight: "30px",
                          textAlign: "center",
                        }}
                      >
                        <DropdownButton>
                          <Dropdown.Item>
                            <Link
                              to={`/createclass/${data?.classCode +
                                "_" +
                                (data?.location).trim().toLowerCase()
                                }`}
                            >
                              Edit
                            </Link>
                          </Dropdown.Item>
                          <Dropdown.Item
                            style={{
                              marginTop: "5px",
                            }}
                            onClick={() => {
                              handleDeleteClick(
                                data?.classCode +
                                "_" +
                                (data?.location).trim().toLowerCase()
                              );
                            }}
                          >
                            Delete
                          </Dropdown.Item>
                        </DropdownButton>
                      </div>
                    ),
                  })),
            }}
            hover
            entriesOptions={[5, 10, 20]}
            entries={10}
            pagesAmount={5}
            paging={true}
            searchBottom={false}
          />
        </div>
      </div>
    </div>
  );
}

export default ClassFormSearch;
