import React from "react";
import styles from "../../css/Confirm.module.css";

const ConfirmationPopup = ({ message, onCancel, onConfirm }) => {
  return (
      <div className={styles.overlay}>
        <div className={styles.popup}>
          <p>{message}</p>
          <div className={styles.btncontainer}>
            <button onClick={onConfirm} style={{ color: "white", backgroundColor: "#1b691b" }}>Confirm</button>
            <button onClick={onCancel} style={{ color: "white", backgroundColor: "#a72c0d" }}>Cancel</button>
          </div>
        </div>
      </div>
  );
};

export default ConfirmationPopup;
