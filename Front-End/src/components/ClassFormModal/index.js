import React, { useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";
import { actClassAPIGetAsync } from "../../store/class/action";
import ClassFormSearch from "./ClassFormSearch";

import styles from "../../css/class.module.css";

export const ClassFormModal = () => {
  let token = localStorage.getItem("ACCESS_TOKEN");

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actClassAPIGetAsync(token));
  }, []);
  
  const listClass = useSelector((state) => state.class.classes);

  return (
    <div>
      <div className={styles.trainingClass}></div>
      <ClassFormSearch tableData={listClass} />
    </div>
  );
};
