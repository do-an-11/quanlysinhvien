import React, { useState } from "react";

import { AttendeeClass } from "./AttendeeClass";
import AttendeeListComponent from "../AttendeeListComponent";
import { CalendarClass } from "../CalendarClass/CalendarClass";
import { GeneralClass } from "./GeneralClass";
import { HeaderCreatedClass } from "./HeaderCreatedClass";
import { TrainingProgram } from "./TrainingProgram";

import styles from "../../css/class.module.css";
import { useTranslation } from "react-i18next";

export const CreatedClass = ({ isOpen, item, listItem, clearForm, id }) => {
  const { t } = useTranslation();
  const [activeButton, setActiveButton] = useState(null);

  const handleButtonClick = (index) => {
    if (activeButton === index) {
      setActiveButton(null);
    } else {
      setActiveButton(index);
    }
  };

  const handleNextButtonClick = () => {
    if (activeButton !== null && activeButton < items.length - 1) {
      setActiveButton(activeButton + 1);
    }
  };

  const items = [t("training_program"), t("attendee_list")];

  return (
    <div>
      <div style={{ marginBottom: "20px" }}>
        <HeaderCreatedClass
          id={id}
          listItem={listItem}
          item={item}
          isOpen={isOpen}
          clearForm={clearForm}
        />
      </div>
      <div>
        <div className="row" style={{ marginBottom: "20px" }}>
          <div className="col-lg-5">
            <GeneralClass
              id={id}
              open={isOpen}
              item={item}
              listItem={listItem}
              clearForm={clearForm}
            />
          </div>
          <div className="col-lg-7">
            <CalendarClass
              id={id}
              open={isOpen}
              listItem={listItem}
              item={item}
              clearForm={clearForm}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-lg-5">
            <AttendeeClass
              id={id}
              open={isOpen}
              item={item}
              listItem={listItem}
              clearForm={clearForm}
            />
          </div>
        </div>
        <div className="row" style={{ textAlign: "center", margin: "0 " }}>
          {items.map((item, index) => (
            <div
              key={index}
              className={`col-lg-2 ${activeButton === index ? "clicked" : ""}`}
              style={{
                color: "white",
                background: activeButton === index ? "#002761" : "#6d7684",
                borderTopLeftRadius: "10px",
                borderTopRightRadius: "10px",
                cursor: "pointer",
                margin: "20px 0 5px 0",
              }}
              onClick={() => handleButtonClick(index)}
            >
              {item}
            </div>
          ))}
        </div>
        <div>
          {activeButton !== null && (
            <>
              {activeButton === 0 && (
                <TrainingProgram listItem={listItem} item={item} id={id} />
              )}
              {activeButton === 1 && (
                <AttendeeListComponent
                  listItem={listItem}
                  item={item}
                  id={id}
                />
              )}
            </>
          )}
        </div>
      </div>
      <div className={styles.btnContainer}>
        <button onClick={handleNextButtonClick}>{t("next")}</button>
      </div>
    </div>
  );
};
