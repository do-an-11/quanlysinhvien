import React, { useEffect, useState } from "react";

import Select from "react-select";

import styles from "../../css/class.module.css";
import { useTranslation } from "react-i18next";

export const HeaderCreatedClass = ({ isOpen, item, listItem, id }) => {
  const [form, setForm] = useState({});
  const [selectedStatus, setSelectedStatus] = useState(null);

  function handleChange(e) {
    let name = e.target.name;
    let value = e.target.value;
    setForm({
      ...form,
      [name]: value,
    });
    const updatedForm = {
      ...form,
      [name]: value,
    };
    listItem(updatedForm);
  }

  useEffect(() => {
    setForm({ ...item, status: "Planning" });
  }, [item]);

  useEffect(() => {
    if (id) {
      setSelectedStatus({ label: item?.status, value: item?.status });
    }
  }, [item, id]);

  const startTime = item?.classTimeFrom;
  const endTime = item?.classTimeTo;
  const parseTime = (timeStr) => {
    if (timeStr) {
      const [hours, minutes] = timeStr.split(":").map(Number);
      return hours + minutes / 60;
    }
    return 0;
  };
  
  const startHours = parseTime(startTime);
  const endHours = parseTime(endTime);

  let durationInHours;

  if (endHours <= startHours) {
    durationInHours = endHours + 24 - startHours;
  } else {
    durationInHours = endHours - startHours;
  }

  const options = [
    { label: "Planning", value: "Planning" },
    { label: "Scheduled", value: "Scheduled" },
    { label: "Opening", value: "Opening" },
    { label: "Completed", value: "Completed" },
  ];

  const handleSelectChange = (selectedOption) => {
    setSelectedStatus(selectedOption);
    listItem({
      ...form,
      status: selectedOption.value,
    });
  };
  const listDayArray = Array.isArray(item?.listDay) ? item?.listDay : [];

  const totalDays = listDayArray.length;

  const totalHours = durationInHours * totalDays;

  const { t } = useTranslation();

  return (
    <>
      {isOpen && (
        <div
          className={styles.classContainer}
          style={{
            background: "#002761",
            color: "white",
            padding: "10px 25px",
          }}
        >
          <div className="row">
            <div
              className="col-lg-4"
              style={{
                fontFamily: "Arial Black",
              }}
            >
              {t("class")}
              <h2>
                <b>{form?.nameClass}</b>
              </h2>
              <input
                placeholder={t("input_class_code")}
                style={{ backgroundColor: "#002761", color: "white" }}
                name="classCode"
                value={form?.classCode}
                onChange={handleChange}
              />
            </div>
            <div
              className="col-lg-6"
              style={{ margin: "auto 0", textAlign: "center" }}
            >
              <p
                style={{
                  border: "1px solid #b9b9b9",
                  width: "150px",
                  borderRadius: "10px",
                  background: "#b9b9b9",
                }}
              >
                {id ? (
                  <>
                    <Select
                      className={styles.showCourse}
                      options={options}
                      onChange={handleSelectChange}
                      value={selectedStatus}
                    />
                  </>
                ) : (
                  "Planning"
                )}
              </p>
            </div>
          </div>
          <div>
            <div
              style={{
                marginTop: "10px",
                fontWeight: "300",
                fontStyle: "italic",
              }}
            >
              <b
                style={{
                  fontSize: "25px",
                  fontWeight: "700",
                  fontStyle: "normal",
                }}
              >
                {item?.listDay?.length}
              </b>{" "}
              days {totalHours} hours
            </div>
          </div>
        </div>
      )}
    </>
  );
};
