import React, { useEffect, useState } from "react";

import {
  faCaretDown,
  faCaretLeft,
  faStar,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Dropdown } from "../Dropdown/Dropdown";

import styles from "../../css/class.module.css";
import { useTranslation } from "react-i18next";

export const AttendeeClass = ({ open, item, listItem, clearForm, id }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOptions, setSelectedOptions] = useState();
  const [formData, setFormData] = useState({});
  useEffect(() => {
    if (open) setIsOpen(open);
  }, [open]);

  useEffect(() => {
    if (id) {
      setSelectedOptions({
        label: item?.attendee,
      });
    }
  }, [id, item]);

  useEffect(() => {
    setFormData({
      ...item,
      attendee: "",
      attendeePlanned: "",
      attendeeAccepted: "",
      attendeeActual: "",
    });
    setSelectedOptions({ label: formData.attendee });
  }, [clearForm]);

  useEffect(() => {
    setFormData({ ...item });
  }, [item]);
  const [listOpen, setListOpen] = useState(false);

  const handleOpen = () => {
    setListOpen(!listOpen);
  };

  const handleFormClick = () => {
    if (isOpen) {
      setIsOpen(false);
    } else {
      setIsOpen(true);
    }
  };
  
  const handleChange = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    const planned = parseInt(formData.attendeePlanned, 10);
    const accepted = parseInt(formData.attendeeAccepted, 10);
    const actual = parseInt(formData.attendeeActual, 10);

    if (
      (name === "attendeePlanned" &&
        (planned < accepted || planned < actual)) ||
      (name === "attendeeAccepted" && accepted < actual)
    ) {
      setFormData({
        ...formData,
        attendeePlanned: "",
        attendeeAccepted: "",
        attendeeActual: "",
      });
      const updatedForm = {
        ...formData,
        attendeePlanned: "",
        attendeeAccepted: "",
        attendeeActual: "",
      };

      listItem(updatedForm);
    } else {
      setFormData({
        ...formData,
        [name]: value,
      });
      const updatedForm = {
        ...formData,
        [name]: value,
      };

      listItem(updatedForm);
    }
  };

  function handleOptionChange(selected) {
    setSelectedOptions(selected);
    setFormData({
      ...formData,
      attendee: selected.value,
    });
    const updatedForm = {
      ...formData,
      attendee: selected.value,
    };
    listItem(updatedForm);
  }

  const { t } = useTranslation();

  return (
    <div>
      <div
        style={{
          color: "white",
          borderRadius: "10px",
          padding: "5px 15px",
          display: "flex",
          cursor: "pointer",
          margin: " auto 0",
        }}
        className={`${styles.formAll} ${isOpen ? "" : styles.closed}`}
      >
        <div className="col-lg-11" style={{ display: "flex" }}>
          <div>
            <FontAwesomeIcon icon={faStar} /> Attendee
          </div>
          <div
            className="col-lg-6"
            style={{ marginLeft: "30px", color: "black" }}
          >
            {
              <Dropdown
                open={listOpen}
                trigger={
                  <button
                    style={{
                      borderStyle: "solid",
                      borderRadius: "5px",
                      borderColor: "grey",
                      height: "80%",
                    }}
                    onClick={handleOpen}
                    className={styles.selectAttendee}
                  >
                    {id
                      ? selectedOptions
                        ? selectedOptions.label
                        : t("experience")
                      : (selectedOptions
                          ? selectedOptions.label
                          : t("experience")) || t("experience")}
                    {<FontAwesomeIcon icon={faCaretDown} />}
                  </button>
                }
                options={[
                  { value: "Intern", label: "Intern" },
                  { value: "Fresher", label: "Fresher" },
                  { value: "Junior", label: "Junior" },
                  { value: "Senior", label: "Senior" },
                ]}
                selected={selectedOptions}
                onChange={handleOptionChange}
              />
            }
          </div>
        </div>
        <div
          className="col-lg-1"
          onClick={handleFormClick}
          style={{ textAlign: "center", cursor: "pointer" }}
        >
          {isOpen ? (
            <FontAwesomeIcon icon={faCaretDown} />
          ) : (
            <FontAwesomeIcon icon={faCaretLeft} />
          )}
        </div>
      </div>
      {isOpen && (
        <div
          style={{
            display: "flex",
            textAlign: "center",
            fontWeight: "700",
            borderRadius: "10px",
            margin: "2px 0",
            color: "white",
            boxShadow: "0 3px 0 3px #e6e6e6",
            padding: "10px",
          }}
        >
          <div
            className="col-lg-4"
            style={{
              background: "#2d3748",
              borderRadius: "10px",
            }}
          >
            <div style={{ margin: "10px 0" }}>{t("planned")}</div>
            <p style={{ fontSize: "25px" }}>
              <input
                name="attendeePlanned"
                value={formData?.attendeePlanned}
                onChange={handleChange}
                style={{ width: "50px", textAlign: "center" }}
              />
            </p>
          </div>
          <div
            className="col-lg-4"
            style={{
              background: "#285d9a",
              margin: "0 2px",
              borderRadius: "10px",
            }}
          >
            <div style={{ margin: "10px 0" }}>{t("accepted")}</div>
            <p style={{ fontSize: "25px" }}>
              <input
                name="attendeeAccepted"
                value={formData?.attendeeAccepted}
                onChange={handleChange}
                style={{ width: "50px", textAlign: "center" }}
              />
            </p>
          </div>
          <div
            className="col-lg-4"
            style={{
              background: "#f1f1f1",
              borderRadius: "10px",
              color: "black",
            }}
          >
            <div style={{ margin: "10px 0" }}>{t("actual")}</div>
            <p style={{ fontSize: "25px" }}>
              <input
                name="attendeeActual"
                value={formData?.attendeeActual}
                onChange={handleChange}
                style={{ width: "50px", textAlign: "center" }}
              />
            </p>
          </div>
        </div>
      )}
    </div>
  );
};
