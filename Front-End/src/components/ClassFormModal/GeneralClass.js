import React, { useEffect, useState } from "react";

import styles from "../../css/class.module.css";

import {
  faCalendar,
  faCaretDown,
  faCaretLeft,
  faClock,
  faMapLocation,
  faStar,
  faStarHalfAlt,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useSelector } from "react-redux";
import Select from "react-select";
import TimePicker from "react-time-picker";
import "react-time-picker/dist/TimePicker.css";
import { useTranslation } from "react-i18next";

const locationOptions = [
  { label: "Ho Chi Minh", value: "Ho Chi Minh" },
  { label: "Ha Noi", value: "Ha Noi" },
  { label: "Da Nang", value: "Da Nang" },
  { label: "Can Tho", value: "Can Tho" },
];

export const GeneralClass = (props) => {
  let { open, item, listItem, clearForm, id } = props;
  const [isOpen, setIsOpen] = useState(false);
  const [selectedLocation, setSelectedLocation] = useState([]);
  const [startTime, setStartTime] = useState("");
  const [endTime, setEndTime] = useState("");
  const [selectedTrainer, setSelectedTrainer] = useState([]);
  const [selectedAdmin, setSelectedAdmin] = useState([]);
  const [selectedFsu, setSelectedFsu] = useState([]);
  const [selectedReview, setSelectReview] = useState([]);
  const [selectApprove, setSelectedApprove] = useState([]);
  const [formData, setFormData] = useState({});
  const trainer = useSelector((state) => state.USER.trainer);
  const admin = useSelector((state) => state.USER.admin);
  const adminAndSuperAdmin = useSelector(
    (state) => state.USER.adminAndSuperAdmin
  );

  const trainerOptions = trainer?.map((e) => {
    return { label: e.email, value: e.email };
  });

  const adminOptions = admin?.map((e) => {
    return { label: e.email, value: e.email };
  });

  const fsuOptions = [
    { label: "Fsu 1", value: "fsu1" },
    { label: "Fsu 2", value: "fsu2" },
    { label: "Fsu 3", value: "fsu3" },
    { label: "Fsu 4", value: "fsu4" },
  ];

  const reviewOptions = adminAndSuperAdmin?.map((e) => {
    return { label: e.email, value: e.email };
  });

  const approveOptions = adminAndSuperAdmin?.map((e) => {
    return { label: e.email, value: e.email };
  });

  const user = useSelector((state) => state.USER.currentUser);
  useEffect(() => {
    if (clearForm) {
      setStartTime("");
      setEndTime("");
      setSelectedLocation("");
      setSelectedAdmin("");
      setSelectedFsu("");
      setSelectedTrainer("");
      setFormData({
        ...item,
        review: "",
        approve: "",
      });
    }
  }, [clearForm]);

  useEffect(() => {
    setFormData({ ...item });
  }, [item]);

  useEffect(() => {
    if (id) {
      setStartTime(item?.classTimeFrom);
      setEndTime(item?.classTimeTo);
      const initialLocation = locationOptions?.filter((option) =>
        item?.location?.includes(option.value)
      );
      setSelectedLocation(initialLocation);

      const initialTrainers = trainerOptions?.filter((option) => {
        const trainers = item?.trainer;
        if (trainers && Array.isArray(trainers)) {
          const test = trainers.map((e) => e?.gmail);
          return test.includes(option.value);
        }
        return false;
      });

      setSelectedTrainer(initialTrainers);

      const initialAdmins = adminOptions?.filter((option) =>
        item?.admin?.includes(option.value)
      );
      setSelectedAdmin(initialAdmins);

      const initialFsu = fsuOptions?.filter((option) =>
        item?.fsu.includes(option.value)
      );
      setSelectedFsu(initialFsu);

      const initialReivew = reviewOptions?.filter((option) =>
        item?.review.includes(option.value)
      );
      setSelectReview(initialReivew);

      const initialApprove = approveOptions?.filter((option) =>
        item?.approve.includes(option.value)
      );
      setSelectedApprove(initialApprove);
    }
  }, [id, item]);

  const startParts = startTime?.split(":") || 0;
  const endParts = endTime?.split(":") || 0;
  const startHours = parseInt(startParts[0]);
  const endHours = parseInt(endParts[0]);

  let durationInHours;

  if (endHours <= startHours) {
    durationInHours = endHours + 24 - startHours;
  } else {
    durationInHours = endHours - startHours;
  }

  const handleFormClick = () => {
    setIsOpen(!isOpen);
  };
  useEffect(() => {
    if (open) setIsOpen(open);
  }, [open]);
  const handleLocationChange = (selected) => {
    setSelectedLocation(selected);
    listItem({
      ...formData,
      location: selected.value,
      created: user?.email,
    });
  };

  const handleTrainerChange = (selected) => {
    setSelectedTrainer(selected);
    listItem({
      ...formData,
      trainer: selected.map((item) => {
        return { gmail: item.value };
      }),
    });
  };

  const handleAdminChange = (selected) => {
    setSelectedAdmin(selected);
    listItem({
      ...formData,
      admin: selected.map((item) => item.value),
    });
  };

  const handleFsuChange = (selected) => {
    setSelectedFsu(selected);
    listItem({
      ...formData,
      fsu: selected.value,
    });
  };
  const handleChangeStart = (value) => {
    setStartTime(value);

    listItem({
      ...formData,
      classTimeFrom: value,
    });
  };
  const handleChangeEnd = (value) => {
    setEndTime(value);

    listItem({
      ...formData,
      classTimeTo: value,
    });
  };
  function handleApproveChange(selected) {
    setSelectedApprove(selected);
    listItem({
      ...formData,
      approve: selected.value,
    });
  }
  function handleReviewChange(selected) {
    setSelectReview(selected);
    listItem({
      ...formData,
      review: selected.value,
    });
  }

  const { t } = useTranslation();

  return (
    <div>
      <div className={`${styles.formAll} ${isOpen ? "" : styles.closed}`}>
        <div className="col-lg-11">
          <FontAwesomeIcon icon={faCalendar} /> {t("general")}
        </div>
        <div
          className="col-lg-1"
          onClick={handleFormClick}
          style={{ textAlign: "center", cursor: "pointer" }}
        >
          {isOpen ? (
            <FontAwesomeIcon icon={faCaretDown} />
          ) : (
            <FontAwesomeIcon icon={faCaretLeft} />
          )}
        </div>
      </div>
      {isOpen && (
        <div className={styles.formOpen}>
          <div
            style={{ marginBottom: "10px", borderBottom: "2px solid #e6e6e6" }}
          >
            <div className={styles.className} style={{ display: "flex" }}>
              <div className="col-lg-4" style={{ display: "flex" }}>
                <div>
                  <FontAwesomeIcon
                    icon={faClock}
                    style={{ color: "#b2c5db" }}
                  />{" "}
                </div>
                <div style={{ marginLeft: "8px" }}>
                  <b>{t("class_time")}</b>
                </div>
              </div>
              <div className={`col-lg-8 ${styles.timePicker}`}>
                <div>
                  <label>{t("start_time")}:</label>
                  <TimePicker
                    onChange={handleChangeStart}
                    value={startTime}
                    clockIcon={true}
                    disableClock
                  />
                </div>
                <div>
                  <label>{t("end_time")}:</label>
                  <TimePicker
                    onChange={handleChangeEnd}
                    value={endTime}
                    clockIcon={true}
                    disableClock
                  />
                </div>
              </div>
            </div>
            <div style={{ display: "flex", marginBottom: "4px" }}>
              <div className="col-lg-4" style={{ display: "flex" }}>
                <div>
                  <FontAwesomeIcon
                    icon={faMapLocation}
                    style={{ color: "#b2c5db" }}
                  />{" "}
                </div>
                <div style={{ marginLeft: "8px" }}>
                  <b>{t("location")}</b>
                </div>
              </div>
              <div className="col-lg-8">
                {" "}
                {
                  <Select
                    // isMulti
                    className={styles.selectInput}
                    value={selectedLocation}
                    onChange={handleLocationChange}
                    options={[
                      { label: "Ho Chi Minh", value: "Ho Chi Minh" },
                      { label: "Ha Noi", value: "Ha Noi" },
                      { label: "Da Nang", value: "Da Nang" },
                      { label: "Can Tho", value: "Can Tho" },
                    ]}
                  />
                }
              </div>
            </div>
            <div style={{ display: "flex", marginBottom: "4px" }}>
              <div className="col-lg-4" style={{ display: "flex" }}>
                <div>
                  <FontAwesomeIcon icon={faUser} style={{ color: "#b2c5db" }} />{" "}
                </div>
                <div style={{ marginLeft: "8px" }}>
                  <b>{t("trainer_class")}</b>
                </div>
              </div>
              <div className="col-lg-8">
                {" "}
                {
                  <Select
                    isMulti
                    className={styles.selectInput}
                    value={selectedTrainer}
                    onChange={handleTrainerChange}
                    options={trainerOptions}
                  />
                }
              </div>
            </div>
            <div style={{ display: "flex", marginBottom: "4px" }}>
              <div className="col-lg-4" style={{ display: "flex" }}>
                <div>
                  <FontAwesomeIcon icon={faStar} style={{ color: "#b2c5db" }} />{" "}
                </div>
                <div style={{ marginLeft: "8px" }}>
                  <b>{t("admin")}</b>
                </div>
              </div>
              <div className="col-lg-8">
                {
                  <Select
                    isMulti
                    className={styles.selectInput}
                    value={selectedAdmin}
                    onChange={handleAdminChange}
                    options={adminOptions}
                  />
                }
              </div>{" "}
            </div>
            <div style={{ display: "flex", marginBottom: "4px" }}>
              <div className="col-lg-4" style={{ display: "flex" }}>
                <div>
                  <FontAwesomeIcon
                    icon={faStarHalfAlt}
                    style={{ color: "#b2c5db" }}
                  />{" "}
                </div>
                <div style={{ marginLeft: "8px" }}>
                  <b>{t("fsu")}</b>
                </div>
              </div>
              <div className="col-lg-8">
                {" "}
                {
                  <Select
                    className={styles.selectInput}
                    value={selectedFsu}
                    onChange={handleFsuChange}
                    options={[
                      { label: "Fsu 1", value: "fsu1" },
                      { label: "Fsu 2", value: "fsu2" },
                      { label: "Fsu 3", value: "fsu3" },
                      { label: "Fsu 4", value: "fsu4" },
                    ]}
                  />
                }
              </div>
            </div>
          </div>
          <div style={{ marginBottom: "10px", display: "flex" }}>
            <div className={`col-lg-4 ${styles.generalLeft}`}>
              <b>{t("created")}</b>
            </div>
            <div className={`col-lg-8 ${styles.generalHeader}`}>
              {item === "Review" || item === "Approve" ? (
                <input />
              ) : (
                <input
                  readOnly
                  value={formData?.created}
                />
              )}
            </div>
          </div>
          <div style={{ marginBottom: "10px", display: "flex" }}>
            <div className={`col-lg-4 ${styles.generalLeft}`}>
              <b>{t("review")}</b>
            </div>
            <div className={`col-lg-8 ${styles.generalRight}`}>
              <Select
                className={styles.selectInput}
                value={selectedReview}
                onChange={handleReviewChange}
                options={reviewOptions}
              />
            </div>
          </div>
          <div style={{ marginBottom: "10px", display: "flex" }}>
            <div className={`col-lg-4 ${styles.generalLeft}`}>
              <b>{t("approve")}</b>
            </div>
            <div className={`col-lg-8 ${styles.generalRight}`}>
              <Select
                className={styles.selectInput}
                value={selectApprove}
                onChange={handleApproveChange}
                options={approveOptions}
              />
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
