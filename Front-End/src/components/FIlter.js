import React, { useState, useEffect, useRef } from "react";

import styles from "../css/FilterComponent.module.css";

import DatePicker from "react-datepicker";
import Select from "react-select";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useSelector } from "react-redux";
import "react-time-picker/dist/TimePicker.css";
import { faFilter } from "@fortawesome/free-solid-svg-icons";
import { useTranslation } from "react-i18next";

const locationOptions = [
  { label: "Ho Chi Minh", value: "Ho Chi Minh" },
  { label: "Ha Noi", value: "Ha Noi" },
  { label: "Da Nang", value: "Da Nang" },
  { label: "Can Tho", value: "Can Tho" },
];

export const FilterComponent = (props) => {
  let { open, item, clearForm, id, handeleFilter } = props;
  const [isOpen, setIsOpen] = useState(false);
  const [isHovered, setIsHovered] = useState(false);
  const [selectedLocation, setSelectedLocation] = useState([]);            
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [selectedTrainer, setSelectedTrainer] = useState([]);
  const [selectedFsu, setSelectedFsu] = useState([]);
  const [formData, setFormData] = useState({});
  const trainer = useSelector((state) => state.USER.trainer);

  const [isCheckedMorning, setIsCheckedMorning] = useState(false);
  const [isCheckedNoon, setIsCheckedNoon] = useState(false);
  const [isCheckedNight, setIsCheckedNight] = useState(false);
  const [isCheckedOnline, setIsCheckedOnline] = useState(false);

  const [isCheckedPlanning, setIsCheckedPlanning] = useState(false);
  const [isCheckedScheduled, setIsCheckedScheduled] = useState(false);
  const [isCheckedOpening, setIsCheckedOpening] = useState(false);
  const [isCheckedCompleted, setIsCheckedCompleted] = useState(false);

  const trainerOptions = trainer?.map((e) => {
    return { label: e.email, value: e.email };
  });

  const filterRef = useRef(null);
  const handleClickOutside = (event) => {
    if (filterRef.current && !filterRef.current.contains(event.target)) {
      setIsFilterOpen(true);
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  useEffect(() => {
    if (open) {
      setIsOpen(open);
      setIsFilterOpen(open);
    }
  }, [open]);

  useEffect(() => {
    const handleRouteChange = () => {
      setIsFilterOpen(false);
    };
    window.addEventListener("popstate", handleRouteChange);

    return () => {
      window.removeEventListener("popstate", handleRouteChange);
    };
  }, []);
  const fsuOptions = [
    { label: "Fsu 1", value: "fsu1" },
    { label: "Fsu 2", value: "fsu2" },
    { label: "Fsu 3", value: "fsu3" },
    { label: "Fsu 4", value: "fsu4" },
  ];

  const user = useSelector((state) => state.USER.currentUser);

  useEffect(() => {
    if (clearForm) {
      setStartDate("");
      setEndDate("");
      setSelectedLocation("");
      setSelectedFsu("");
      setSelectedTrainer("");
      setFormData({
        ...item,
      });
    }
  }, [clearForm]);

  useEffect(() => {
    setFormData({ ...item });
  }, [item]);

  useEffect(() => {
    if (open) setIsOpen(open);
  }, [open]);

  const handleLocationChange = (selected) => {
    setSelectedLocation(selected);
  };

  const handleTrainerChange = (selected) => {
    setSelectedTrainer(selected);
  };

  const handleFsuChange = (selected) => {
    setSelectedFsu(selected);
  };

  const [isFilterOpen, setIsFilterOpen] = useState(
    localStorage.getItem("isFilterOpen") === "true" || false
  );

  useEffect(() => {
    localStorage.setItem("isFilterOpen", isFilterOpen);
  }, [isFilterOpen]);

  const handleFilterClick = () => {
    setIsFilterOpen(!isFilterOpen);
  };

  const handleClear = () => {
    setSelectedLocation([]);
    setStartDate("");
    setEndDate("");
    setSelectedTrainer([]);
    setSelectedFsu([]);
    setIsCheckedMorning(false);
    setIsCheckedNoon(false);
    setIsCheckedNight(false);
    setIsCheckedOnline(false);
    setIsCheckedPlanning(false);
    setIsCheckedScheduled(false);
    setIsCheckedOpening(false);
    setIsCheckedCompleted(false);
  };

  const handleSearch = () => {
    handleClear()
    handeleFilter({ selectedLocation, startDate, endDate, selectedTrainer, selectedFsu, isCheckedMorning, isCheckedNoon, isCheckedNight, isCheckedOnline, isCheckedPlanning, isCheckedScheduled, isCheckedOpening, isCheckedCompleted })
  }

  const { t } = useTranslation();

  return (
    <div
      className={styles.filterContainer}
      style={{ width: "5rem" }}
      ref={filterRef}
    >
      <div
        className={styles.filterHeader}
        style={{ cursor: "pointer" }}
        onClick={handleFilterClick}
      >
        <FontAwesomeIcon icon={faFilter} /> {t("filter")}
      </div>
      {!isFilterOpen && (
        <div
          className={styles.filter}
          style={{
            zIndex: "99",
            position: "fixed",
            backgroundColor: "none",
            width: "fitContent",
            paddingTop: "1rem",
          }}
        >
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              padding: "1rem",
              border: "1px solid black",
              borderRadius: "10px",
              backgroundColor: "#f9f9f9",
            }}
          >
            <div
              className="row1"
              style={{ display: "flex", marginBottom: "1rem" }}
            >
              <div
                className="col-md-6"
                style={{ display: "flex", flexDirection: "column" }}
              >
                <div>
                  <div>
                    <b>{t("class_location")}</b>
                  </div>{" "}
                </div>
                <div>
                  {
                    <Select
                      isMulti
                      className={styles.selectInput}
                      value={selectedLocation}
                      onChange={handleLocationChange}
                      options={[
                        { label: "Ho Chi Minh", value: "Ho Chi Minh" },
                        { label: "Ha Noi", value: "Ha Noi" },
                        { label: "Da Nang", value: "Da Nang" },
                        { label: "Can Tho", value: "Can Tho" },
                      ]}
                    />
                  }
                </div>
              </div>
              <div
                className="col-md-6"
                style={{
                  display: "flex",
                  flexDirection: "column",
                  paddingLeft: "0.5rem",
                }}
              >
                <div style={{ display: "flex" }}>
                  <div>
                    <b>{t("class_time_frame")} </b>
                  </div>
                </div>
                <div style={{ display: "flex" }}>
                  <div
                    className={`${styles.timePicker}`}
                    style={{ display: "flex", columnGap: "0.5rem" }}
                  >
                    <div style={{ display: "flex", columnGap: "0.5rem" }}>
                      <label style={{ color: "black" }}>{t("from")}:</label>
                      <DatePicker
                        selected={startDate}
                        onChange={(date) => setStartDate(date)}
                        disableClock
                        clearIcon
                      />
                    </div>
                    <div style={{ display: "flex", columnGap: "0.5rem" }}>
                      <label style={{ color: "black" }}>{t("to")}:</label>
                      <DatePicker
                        selected={endDate}
                        onChange={(date) => setEndDate(date)}
                        minDate={startDate}
                        disableClock
                        clearIcon
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              className="row2"
              style={{
                display: "flex",
                marginBottom: "1rem",
                width: "fitContent",
              }}
            >
              {" "}
              <div className="col-md-3" style={{ display: "flex" }}>
                <label style={{ color: "black" }}>{t("class_time")}</label>
                <div
                  style={{
                    paddingLeft: "1rem",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <div style={{ display: "flex" }}>
                    <input
                      type="checkbox"
                      id="checkboxMorning"
                      name="checkboxMorning"
                      checked={isCheckedMorning}
                      onChange={() => setIsCheckedMorning(!isCheckedMorning)}
                    />
                    <p
                      style={{
                        color: "black",
                        paddingLeft: "0.5rem",
                        marginBottom: "0",
                        cursor: "pointer",
                      }}
                      htmlFor="checkboxMorning"
                      onClick={() => setIsCheckedMorning(!isCheckedMorning)}
                    >
                      Morning
                    </p>
                  </div>
                  <div style={{ display: "flex" }}>
                    {" "}
                    <input
                      type="checkbox"
                      id="checkboxNoon"
                      name="checkboxNoon"
                      checked={isCheckedNoon}
                      onChange={() => setIsCheckedNoon(!isCheckedNoon)}
                    />
                    <p
                      style={{
                        color: "black",
                        paddingLeft: "0.5rem",
                        marginBottom: "0",
                        cursor: "pointer",
                      }}
                      htmlFor="checkboxNoon"
                      onClick={() => setIsCheckedNoon(!isCheckedNoon)}
                    >
                      Noon
                    </p>
                  </div>
                  <div style={{ display: "flex" }}>
                    <input
                      type="checkbox"
                      id="checkboxNight"
                      name="checkboxNight"
                      checked={isCheckedNight}
                      onChange={() => setIsCheckedNight(!isCheckedNight)}
                    />
                    <p
                      style={{
                        color: "black",
                        paddingLeft: "0.5rem",
                        marginBottom: "0",
                        cursor: "pointer",
                      }}
                      htmlFor="checkboxNight"
                      onClick={() => setIsCheckedNight(!isCheckedNight)}
                    >
                      Night
                    </p>
                  </div>
                  <div style={{ display: "flex" }}>
                    <input
                      type="checkbox"
                      id="checkboxOnline"
                      name="checkboxOnline"
                      checked={isCheckedOnline}
                      onChange={() => setIsCheckedOnline(!isCheckedOnline)}
                    />
                    <p
                      style={{
                        color: "black",
                        paddingLeft: "0.5rem",
                        marginBottom: "0",
                        cursor: "pointer",
                      }}
                      htmlFor="checkboxOnline"
                      onClick={() => setIsCheckedOnline(!isCheckedOnline)}
                    >
                      Online
                    </p>
                  </div>
                </div>
              </div>
              <div
                className="col-md-3"
                style={{ display: "flex", paddingLeft: "1rem" }}
              >
                <label style={{ color: "black" }}>{t("status")}</label>
                <div
                  style={{
                    paddingLeft: "1rem",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <div style={{ display: "flex" }}>
                    {" "}
                    <input
                      type="checkbox"
                      id="checkboxPlanning"
                      name="checkboxPlanning"
                      checked={isCheckedPlanning}
                      onChange={() => setIsCheckedPlanning(!isCheckedPlanning)}
                    />
                    <p
                      style={{
                        color: "black",
                        paddingLeft: "0.5rem",
                        marginBottom: "0",
                        cursor: "pointer",
                      }}
                      htmlFor="checkboxPlanning"
                      onClick={() => setIsCheckedPlanning(!isCheckedPlanning)}
                    >
                      Planning
                    </p>
                  </div>
                  <div style={{ display: "flex" }}>
                    {" "}
                    <input
                      type="checkbox"
                      id="checkboxScheduled"
                      name="checkboxScheduled"
                      checked={isCheckedScheduled}
                      onChange={() =>
                        setIsCheckedScheduled(!isCheckedScheduled)
                      }
                    />
                    <p
                      style={{
                        color: "black",
                        paddingLeft: "0.5rem",
                        marginBottom: "0",
                        cursor: "pointer",
                      }}
                      htmlFor="checkboxScheduled"
                      onClick={() =>
                        setIsCheckedScheduled(!isCheckedScheduled)
                      }
                    >
                      Scheduled
                    </p>
                  </div>
                  <div style={{ display: "flex" }}>
                    <input
                      type="checkbox"
                      id="checkboxOpening"
                      name="checkboxOpening"
                      checked={isCheckedOpening}
                      onChange={() => setIsCheckedOpening(!isCheckedOpening)}
                    />
                    <p
                      style={{
                        color: "black",
                        paddingLeft: "0.5rem",
                        marginBottom: "0",
                        cursor: "pointer",
                      }}
                      htmlFor="checkboxOpening"
                      onClick={() => setIsCheckedOpening(!isCheckedOpening)}
                    >
                      Opening
                    </p>
                  </div>
                  <div style={{ display: "flex" }}>
                    <input
                      type="checkbox"
                      id="checkboxCompleted"
                      name="checkboxCompleted"
                      checked={isCheckedCompleted}
                      onChange={() => setIsCheckedCompleted(!isCheckedCompleted)}
                    />
                    <p
                      style={{
                        color: "black",
                        paddingLeft: "0.5rem",
                        marginBottom: "0",
                        cursor: "pointer",
                      }}
                      htmlFor="checkboxCompleted"
                      onClick={() => setIsCheckedCompleted(!isCheckedCompleted)}
                    >
                      Completed
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div
              className="row3"
              style={{
                display: "flex",
                justifyContent: "space-between",
                width: "fitContent",
              }}
            >
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  width: "fitContent",
                }}
              >
                <div
                  style={{
                    width: "fitContent",
                    justifyContent: "center",
                    alignItems: "center",
                    alignContent: "center",
                    display: "flex",
                    marginBottom: "4px",
                  }}
                >
                  <div
                    style={{
                      width: "fitContent",
                      justifyContent: "center",
                      alignItems: "center",
                      alignContent: "center",
                    }}
                  ></div>
                  <div
                    style={{
                      width: "fitContent",
                      justifyContent: "center",
                      alignItems: "center",
                      alignContent: "center",
                      paddingLeft: "0.5rem",
                    }}
                  ></div>
                </div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    width: "fitContent",
                  }}
                >
                  <div
                    style={{
                      width: "fitContent",
                      justifyContent: "center",
                      alignItems: "center",
                      alignContent: "center",
                      display: "flex",
                      marginBottom: "4px",
                    }}
                  >
                    <div
                      style={{
                        width: "fitContent",
                        justifyContent: "center",
                        alignItems: "center",
                        alignContent: "center",
                      }}
                    >
                      <b>FSU</b>
                    </div>
                    <div
                      style={{
                        width: "fitContent",
                        justifyContent: "center",
                        alignItems: "center",
                        alignContent: "center",
                        paddingLeft: "0.5rem",
                      }}
                    >
                      {
                        <Select
                          className={styles.selectInput}
                          value={selectedFsu}
                          onChange={handleFsuChange}
                          options={[
                            { label: "Fsu 1", value: "fsu1" },
                            { label: "Fsu 2", value: "fsu2" },
                            { label: "Fsu 3", value: "fsu3" },
                            { label: "Fsu 4", value: "fsu4" },
                          ]}
                        />
                      }
                    </div>
                  </div>
                  <div
                    style={{
                      width: "fitContent",
                      justifyContent: "center",
                      alignItems: "center",
                      alignContent: "center",
                      display: "flex",
                      marginBottom: "4px",
                    }}
                  >
                    <div
                      style={{
                        width: "fitContent",
                        justifyContent: "center",
                        alignItems: "center",
                        alignContent: "center",
                        paddingLeft: "0.5rem",
                      }}
                    >
                      <b>{t("trainer")}</b>
                    </div>
                    <div
                      style={{
                        width: "fitContent",
                        justifyContent: "center",
                        alignItems: "center",
                        alignContent: "center",
                        paddingLeft: "0.5rem",
                      }}
                    >
                      {
                        <Select
                          isMulti
                          className={styles.selectInput}
                          value={selectedTrainer}
                          onChange={handleTrainerChange}
                          options={trainerOptions}
                        />
                      }
                    </div>
                  </div>

                  <div
                    style={{
                      width: "fitContent",
                      justifyContent: "center",
                      alignItems: "center",
                      alignContent: "center",
                      display: "flex",
                      marginBottom: "4px",
                    }}
                  >
                  </div>
                </div>
              </div>
            </div>
            <div
              className="row4"
              style={{
                display: "flex",
                justifyContent: "flex-end",
                columnGap: "0.5rem",
                paddingTop: "8px",
              }}
            >
              <div className="col-md-2">
                <button
                  onClick={handleClear}
                  style={{
                    border: "solid 1px",
                    borderRadius: "5px",
                    textAlign: "center",
                    color: "white",
                    backgroundColor: "#696e79",
                    width: "100%",
                  }}
                >
                  {t("clear")}
                </button>
              </div>
              <div className="col-md-2">
                <button
                  onClick={handleSearch}
                  style={{
                    border: "solid 1px",
                    borderRadius: "5px",
                    textAlign: "center",
                    color: "white",
                    backgroundColor: "#2E3647",
                    width: "100%"
                  }}
                >
                  {t("search")}
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default FilterComponent;
