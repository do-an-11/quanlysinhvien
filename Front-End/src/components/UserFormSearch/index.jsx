import React, { useEffect, useState } from "react";

import axios from "axios";
import { MDBDataTableV5 } from "mdbreact";
import { useTranslation } from "react-i18next";
import { FcBusinessman, FcBusinesswoman } from "react-icons/fc";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import Select from "react-select";
import {
  actUserDeleteAsyncAPI,
  actUserGetAsyncAPI,
  actUserRecoverAsyncAPI,
} from "../../store/user/action";

import "./user-form-search.css";
import styles from "../../css/Confirm.module.css";

function UserFormSearch() {
  const { t } = useTranslation();
  const [selectedOptions, setSelectedOptions] = useState([]);
  const allUser = useSelector((state) => state.USER.users);
  const [filteredData, setFilteredData] = useState(allUser.users);

  const handleSelectChange = (selected) => {
    setSelectedOptions(selected);
  };
  const dispatch = useDispatch();
  const role = useSelector((state) => state.USER.role);
  const options = allUser.users?.map((e) => {
    return { value: e.email, label: e.email };
  });

  const [currentPage, setCurrentPage] = useState(1);
  const [entriesPerPage, setEntriesPerPage] = useState(10);
  const [totalRecords, setTotalRecords] = useState(0);
  const token = localStorage.getItem("ACCESS_TOKEN");

  const [showConfirmation, setShowConfirmation] = useState(false);
  const [confirmationAction, setConfirmationAction] = useState('');
  const [confirmationUserId, setConfirmationUserId] = useState('');

  const showConfirmationModal = (action, userId) => {
    setConfirmationAction(action);
    setConfirmationUserId(userId);
    setShowConfirmation(true);
  };

  const hideConfirmationModal = () => {
    setShowConfirmation(false);
  };

  const handleConfirmation = (confirmed) => {
    if (confirmed) {
      if (confirmationAction === 'delete') {
        dispatch(actUserDeleteAsyncAPI(confirmationUserId, token));
      } else if (confirmationAction === 'recover') {
        dispatch(actUserRecoverAsyncAPI(confirmationUserId, token));
      }
    }
    hideConfirmationModal();
  };

  useEffect(() => {
    const startIndex = (currentPage - 1) * entriesPerPage;

    axios
      .get(`/api/users?page=${currentPage}&limit=${entriesPerPage}`)
      .then((response) => {
        const { data, totalRecords } = response.data;
        setFilteredData(data);
        setTotalRecords(totalRecords);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }, [currentPage, entriesPerPage]);

  useEffect(() => {
    dispatch(actUserGetAsyncAPI(token));
  }, []);

  useEffect(() => {
    const selectedEmails = selectedOptions.map((option) => option.value);
    const filteredData = allUser?.users?.filter((user) =>
      selectedEmails.includes(user.email)
    );
    setFilteredData(filteredData);
  }, [selectedOptions, allUser.users]);

  const handleDelete = (id) => {
    showConfirmationModal('delete', id);
  };

  const handleRecover = (id) => {
    showConfirmationModal('recover', id);
  };
  const rows =
    filteredData?.length <= 0
      ? allUser?.users?.map((employee) => ({
        Id: employee.userId,
        fullName: employee.name,
        dateOfBirth: employee.dob,
        email: employee.email,
        type: employee.role && (
          <span
            style={{
              paddingLeft: "10px",
              paddingRight: "10px",
              borderRadius: "5px",
              fontWeight: "bold",
              backgroundColor:
                employee.role === "SUPER_ADMIN"
                  ? "#ffcfd8"
                  : employee.role === "CLASS_ADMIN"
                    ? "#f1ffdc"
                    : employee.role === "TRAINER"
                      ? "#d2fdff"
                      : "#fdf7ec",
              color:
                employee.role === "SUPER_ADMIN"
                  ? "palevioletred"
                  : employee.role === "CLASS_ADMIN"
                    ? "#8acc40"
                    : employee.role === "TRAINER"
                      ? "turquoise"
                      : "#c5b48e",
            }}
          >
            {employee.role}
          </span>
        ),
        gender:
          employee.gender === "male" ? (
            <FcBusinessman style={{ fontSize: 30 }} />
          ) : (
            <FcBusinesswoman style={{ fontSize: 30 }} />
          ),
        status: employee.status === "Active" ? "Active" : "DeActive",
        options:
          role === "CLASS_ADMIN" && employee.role === "SUPER_ADMIN" ? (
            <span
              style={{
                backgroundColor: "#ffcfd8",
                color: "palevioletred",
                fontWeight: "bold",
                paddingLeft: "5px",
                paddingRight: "5px",
                borderRadius: "5px",
              }}
            >
              SuperAdmin
            </span>
          ) : employee?.status ? (
            <Link to={`/manage/${employee.email}`}>Edit</Link>
          ) : (
            <button
              style={{
                borderRadius: "5px",
                backgroundColor: "greenyellow",
                color: "blueviolet",
                fontWeight: 600,
              }}
              onClick={() => handleRecover(employee?.userId)}
            >
              Recover
            </button>
          ),
        deleteButton:
          role === "SUPER_ADMIN" ? (
            employee?.status === true ? (
              <button
                style={{ fontSize: "17px", color: "white", fontWeight: "bold", }}
                className="delete-button"
                onClick={() => handleDelete(employee.userId)}
              >
                Inactive
              </button>
            ) : (
              <></> // Empty fragment or null if there's no action needed
            )
          ) : (
            (employee.role === "USER" || employee.role === "TRAINER") &&
            (employee?.status === true ? (
              <button
                className="delete-button"
                onClick={() => handleDelete(employee.userId)}
              >
                Inactive
              </button>
            ) : (
              <></> // Empty fragment or null if there's no action needed
            ))
          ),
      }))
      : filteredData?.map((employee) => ({
        Id: employee.userId,
        fullName: employee.name,
        dateOfBirth: employee.dob,
        email: employee.email,
        type: employee.role && (
          <span
            style={{
              paddingLeft: "5px",
              paddingRight: "5px",
              borderRadius: "5px",
              backgroundColor:
                employee.role === "SUPER_ADMIN"
                  ? "#ffcfd8"
                  : employee.role === "CLASS_ADMIN"
                    ? "#f1ffdc"
                    : employee.role === "TRAINER"
                      ? "#d2fdff"
                      : "#fdf7ec",
              color:
                employee.role === "SUPER_ADMIN"
                  ? "palevioletred"
                  : employee.role === "CLASS_ADMIN"
                    ? "#8acc40"
                    : employee.role === "TRAINER"
                      ? "turquoise"
                      : "#c5b48e",
            }}
          >
            {employee.role}
          </span>
        ),
        gender:
          employee.gender === "male" ? (
            <FcBusinessman style={{ fontSize: 30 }} />
          ) : (
            <FcBusinesswoman style={{ fontSize: 30 }} />
          ),
        options:
          role === "CLASS_ADMIN" && employee.role === "SUPER_ADMIN" ? (
            <span
              style={{
                backgroundColor: "#ffcfd8",
                color: "palevioletred",
                fontWeight: "bold",
                paddingLeft: "5px",
                paddingRight: "5px",
                borderRadius: "5px",
              }}
            >
              SuperAdmin
            </span>
          ) : employee?.status ? (
            <Link to={`/manage/${employee.email}`}>Edit</Link>
          ) : (
            <button
              style={{
                borderRadius: "5px",
                backgroundColor: "greenyellow",
                color: "blueviolet",
                fontWeight: 600,
              }}
              onClick={() => handleRecover(employee?.userId)}
            >
              Recover
            </button>
          ),
        deleteButton:
          role === "SUPER_ADMIN" ? (
            employee?.status === true ? (
              <button
                className="delete-button"
                onClick={() => handleDelete(employee.userId)}
              >
                Inactive
              </button>
            ) : (
              <></>
            )
          ) : (
            (employee.role === "USER" || employee.role === "TRAINER") &&
            (employee?.status === true ? (
              <button
                className="delete-button"
                onClick={() => handleDelete(employee.userId)}
              >
                Inactive
              </button>
            ) : (
              <></>
            ))
          ),
      }));

  return (
    <div className="manager">
      <div className="row">
        <div className="col-md-3">
          <Select
            isMulti
            options={options}
            value={selectedOptions}
            onChange={handleSelectChange}
            placeholder={<>Select email...</>}
          />
        </div>
      </div>
      {showConfirmation && (
        <div className={styles.overlay}>
          <div className={styles.popup}>
            <p>Are you sure you want to {confirmationAction} this user?</p>
            <div className={styles.btncontainer}>
              <button onClick={() => handleConfirmation(true)} style={{ color: "white", backgroundColor: "#1b691b" }}>Confirm</button>
              <button onClick={() => handleConfirmation(false)} style={{ color: "white", backgroundColor: "#a72c0d" }}>Cancel</button>
            </div>
          </div>
        </div>
      )}
      <div className="row" style={{ marginTop: "20px" }}>
        <div className="table-container">
          <MDBDataTableV5
            className="custom-table"
            data={{
              columns: [
                {
                  label: "ID",
                  field: "Id",
                  width: 150,
                },
                {
                  label: t("full_name"),
                  field: "fullName",
                  width: 150,
                },
                {
                  label: t("dob"),
                  field: "dateOfBirth",
                  width: 150,
                },
                {
                  label: t("email"),
                  field: "email",
                  width: 200,
                },
                {
                  label: t("type"),
                  field: "type",
                  width: 150,
                },
                {
                  label: t("gender"),
                  field: "gender",
                  width: 150,
                },
                {
                  label: t("options"),
                  field: "options",
                  sort: "disabled",
                  width: 100,
                },
                {
                  label: t("delete"),
                  field: "deleteButton",
                  width: 100,
                  attributes: {
                    className: "delete-column",
                    style: {
                      backgroundColor: "red",
                      color: "white",
                      border: "none",
                    },
                  },
                },
              ],
              rows: rows,
            }}
            hover
            entriesOptions={[5, 10, 20]}
            entries={10}
            pagesAmount={5}
            paging={true}
            searchTop={false}
            searchBottom={false}
          />
        </div>
      </div>
    </div>
  );
}

export default UserFormSearch;
