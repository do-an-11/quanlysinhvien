import React, { useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { actUserNotFetchMe } from "../store/user/action";

import { faCircleUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import i18next from "i18next";
import { Dropdown } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import { FaGlobe } from "react-icons/fa";

import "../css/style.css";
import styles from "../css/Confirm.module.css";

const languages = [
  {
    code: "en",
    name: "English",
    country_code: "gb",
  },
  {
    code: "vi",
    name: "Tiếng Việt",
    country_code: "vn",
  },
  {
    code: "zh",
    name: "中文",
    country_code: "cn",
  },
  {
    code: "ja",
    name: "日本語",
    country_code: "jp",
  },
  {
    code: "ko",
    name: "한국어",
    country_code: "kr",
  },
  {
    code: "fr",
    name: "Français",
    country_code: "fr",
  },
  {
    code: "pt",
    name: "Português",
    country_code: "pt",
  },
  {
    code: "es",
    name: "Español",
    country_code: "es",
  },
  {
    code: "de",
    name: "Deutsch",
    country_code: "de",
  },
  {
    code: "ru",
    name: "Русский",
    country_code: "ru",
  },
];

export default function Header() {
  const tokenUser = localStorage.getItem("ACCESS_TOKEN");
  const currentUser = useSelector((state) => state.USER.currentUser);
  const role = useSelector((state) => state.USER.role);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [showLogoutConfirmation, setShowLogoutConfirmation] = useState(false);

  function handleLogout() {
    setShowLogoutConfirmation(true);
  }

  function cancelLogout() {
    setShowLogoutConfirmation(false);
  }

  function proceedLogout() {
    dispatch(actUserNotFetchMe(null));
    navigate("/login");
    setShowLogoutConfirmation(false);
  }

  const { t } = useTranslation();

  return (
    <div className="header-container">
      {" "}
      <div className="logo">
      </div>
      <div className="account-container" style={{ margin: "auto 0" }}>
        {tokenUser !== "null" ? (
          <div className="profile-image" style={{ display: "flex" }}>
            <div>
              <Link to={"/profile"} style={{ color: "white" }}>
                <FontAwesomeIcon
                  style={{
                    fontSize: 40,
                  }}
                  icon={faCircleUser}
                />
              </Link>
            </div>
            <div style={{ margin: "auto 0" }}>
              <button
                className="logoutbtn"
                style={{ marginLeft: "10px" }}
                onClick={handleLogout}
              >
                {t("log_out")}
              </button>
              {showLogoutConfirmation && (
                <div className={styles.overlay}>
                  <div className={styles.popup}>
                    <p>{t("Are you sure you want to log out ?")}</p>
                    <div className={styles.btncontainer}>
                      <button onClick={proceedLogout} style={{ color: "white", backgroundColor: "#1b691b" }}>{t("Logout")}</button>
                      <button onClick={cancelLogout} style={{ color: "white", backgroundColor: "#a72c0d" }}>{t("Cancel")}</button>
                    </div>
                  </div>
                </div>
              )}
            </div>
            <Dropdown style={{ marginLeft: "20px" }}>
              <Dropdown.Toggle variant="success" id="dropdown-basic">
                <FaGlobe /> {t("language")}
              </Dropdown.Toggle>
              <Dropdown.Menu>
                {languages.map(({ code, name, country_code }) => (
                  <Dropdown.Item
                    eventKey={name}
                    key={country_code}
                    to="/"
                    onClick={() => i18next.changeLanguage(code)}
                  >
                    <span
                      className={`flag-icon flag-icon-${country_code}`}
                    ></span>{" "}
                    {name}
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </div>
        ) : (
          ""
        )}
      </div>
    </div>
  );
}
