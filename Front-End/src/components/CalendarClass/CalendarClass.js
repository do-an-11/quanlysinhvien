import React, { useEffect, useState } from "react";

import { faCaretDown, faCaretLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { format, isAfter, isSameDay, parse } from "date-fns";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import DatePicker from "react-multi-date-picker";
import { useSelector } from "react-redux";
import { addDays, startOfToday } from "date-fns";

import styles from "../../css/class.module.css";
import { useTranslation } from "react-i18next";

export const CalendarClass = ({ open, item, listItem, clearForm, id }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [formData, setFormData] = useState({});
  const [selectedDates, setSelectedDates] = useState([]);
  const [dateList, setDateList] = useState([]);
  const [totalTimeLearning, setTotalTimeLearning] = useState("");
  const [oldStartDate, setoldStartDate] = useState("");
  const [oldEndDate, setoldEndDate] = useState("");
  const [oldList, setOldList] = useState([]);
  useEffect(() => {
    setFormData({ ...item });
  }, [item]);
  useEffect(() => {
    if (clearForm) {
      setStartDate("");
      setEndDate("");
      setSelectedDates([]);
    }
  }, [clearForm]);

  const listClass = useSelector((state) => state.class.classes);
  const oneClass = listClass.find(
    (e) => e?.classCode + "_" + (e?.location).trim().toLowerCase() == id
  );
  
  useEffect(() => {
    if (id) {
      setoldStartDate(oneClass?.startDate);
      setoldEndDate(oneClass?.endDate);
      setOldList(oneClass?.listDay);
    }
  }, [id, item]);

  useEffect(() => {
    if (open) setIsOpen(open);
  }, [open]);

  const handleDateChange = (newDates) => {
    const validDates = newDates?.filter((date) => {
      const dateObj = parse(date, "MM/dd/yyyy", new Date());
      return (
        (!startDate ||
          isSameDay(dateObj, startDate) ||
          isAfter(dateObj, startDate)) &&
        (!endDate || isSameDay(dateObj, endDate) || isAfter(endDate, dateObj))
      );
    });

    setSelectedDates(validDates);

    const dateStrings = validDates
      ?.map((date) => {
        try {
          const formattedDate = format(date, "mm/dd/yyyy");
          return formattedDate;
        } catch (error) {
          return null;
        }
      })
      .filter((dateString) => dateString !== null);

    setDateList(dateStrings);
  };

  const handleFormClick = () => {
    setIsOpen(!isOpen);
  };

  const handleCalendarChange = (value) => {
    if (value.length === 2) {
      let startDate = format(value[0], "MM/dd/yyyy");
      let endDate = format(value[1], "MM/dd/yyyy");
      setFormData({
        ...formData,
        startDate,
        endDate,
      });
      const updateForm = {
        ...formData,
        startDate,
        endDate,
      };
      listItem(updateForm);
      setStartDate(value[0]);
      setEndDate(value[1]);
    } else {
      setStartDate(null);
      setEndDate(null);
    }

    const validDates = selectedDates?.filter((date) => {
      return (
        (!startDate ||
          isSameDay(date, startDate) ||
          isAfter(date, startDate)) &&
        (!endDate || isSameDay(date, endDate) || isAfter(endDate, date))
      );
    });

    const dateStrings = validDates?.map((date) => format(date, "MM/dd/yyyy"));
    setDateList(dateStrings);
  };

  const startTime = item?.classTimeFrom;
  const endTime = item?.classTimeTo;
  const parseTime = (timeStr) => {
    if (timeStr) {
      const [hours, minutes] = timeStr.split(":").map(Number);
      return hours + minutes / 60;
    }
    return 0;
  };

  const startHours = parseTime(startTime);
  const endHours = parseTime(endTime);
  let durationInHours;
  if (endHours <= startHours) {
    durationInHours = endHours + 24 - startHours;
  } else {
    durationInHours = endHours - startHours;
  }

  useEffect(() => {
    let arr = selectedDates?.map((date) => {
      return date.format("MM/DD/YYYY");
    });
    const totalLearningTime = `${arr?.length || 0} days ${
      durationInHours * (arr?.length || 0)
    } hours`;
    setTotalTimeLearning(totalLearningTime);
    setFormData({
      ...formData,
      listDay: arr,
    });
    let update = {
      ...formData,
      listDay: arr,
      totalTimeLearning: totalLearningTime,
    };
    listItem(update);
  }, [selectedDates]);

  const { t } = useTranslation();

  return (
    <div>
      <div
        style={{
          color: "white",
          borderRadius: "10px",
          padding: "5px 15px",
          cursor: "pointer",
          margin: "auto 0",
        }}
        className={`${styles.formAll} ${isOpen ? "" : styles.closed}`}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            minWidth: "100%",
          }}
        >
          <div className="col-lg-11">
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <div>{t("time_frame")}</div>
              <div style={{ marginLeft: "40px" }}>
                {startDate && endDate ? (
                  <p
                    style={{
                      height: "5px",
                      fontStyle: "italic",
                    }}
                  >
                    <span>{t("start")}: </span>{" "}
                    {format(startDate, "MM/dd/yyyy")} -<span> {t("end")}:</span>{" "}
                    {format(endDate, "MM/dd/yyyy")}
                  </p>
                ) : (
                  <p></p>
                )}
                {oldEndDate && oldEndDate ? (
                  <p
                    style={{
                      height: "5px",
                      fontStyle: "italic",
                    }}
                  >
                    {t("time_old")}: {oldStartDate} - {oldEndDate}
                  </p>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
          <div
            className="col-lg-1"
            onClick={handleFormClick}
            style={{
              textAlign: "center",
              cursor: "pointer",
              width: "50px",
            }}
          >
            {isOpen ? (
              <FontAwesomeIcon icon={faCaretDown} />
            ) : (
              <FontAwesomeIcon icon={faCaretLeft} />
            )}
          </div>
        </div>
      </div>
      {isOpen && (
        <div
          style={{
            boxShadow: "0 3px 0 3px #e6e6e6",
            borderRadius: "10px",
            marginTop: "3px",
            padding: "10px",
          }}
        >
          <div style={{ textAlign: "center" }}>
            <div
              style={{
                display: "flex",
                justifyContent: "space-around",
                marginTop: "20px",
                background: "#b2c5db",
                padding: "30px 0",
                borderRadius: "20px",
              }}
            >
              <div>
                <Calendar
                  onChange={handleCalendarChange}
                  selectRange={true}
                  value={[startDate, endDate]}
                  minDate={addDays(startOfToday(), 1)}
                />
              </div>
              <div>
                <DatePicker
                  value={selectedDates}
                  onChange={handleDateChange}
                  format="MM/DD/YYYY"
                  multiple
                  calendarPosition="bottom"
                  showFooter={false}
                />
                <div style={{ textAlign: "center" }}>
                  {selectedDates?.length > 0 && (
                    <ul>
                      {selectedDates.map((date) => (
                        <li key={date.format("MM/DD/YYYY")}>
                          {date.format("MM/DD/YYYY")}
                        </li>
                      ))}
                    </ul>
                  )}
                </div>
              </div>
              <ul>{oldList && oldList.map((e) => <li>{e}</li>)}</ul>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
