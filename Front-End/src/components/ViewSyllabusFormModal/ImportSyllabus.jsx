import { useState } from "react";

import Papa from "papaparse";
import { Button, Form, Modal } from "react-bootstrap";
import Col from "react-bootstrap/Col";
import { CSVLink } from "react-csv";
import { toast } from "react-toastify";
import { useTranslation } from "react-i18next";

const ImportSyllabus = (props) => {
  const {
    show,
    handleClose,
    dataFromParent,
    updateFileCSV,
  } = props;
  const [dataExport, setDataExport] = useState([]);
  const [file, setfile] = useState([]);
  const handleSaveUser = () => {
    Papa.parse(file, {
      complete: function (results) {
        let rawCSV = results.data;
        if (rawCSV.length > 0) {
          if (rawCSV[0] && rawCSV[0].length === 7) {
            if (
              rawCSV[0][0] != "Syllabus" ||
              rawCSV[0][1] != "Code" ||
              rawCSV[0][2] != "Created on" ||
              rawCSV[0][3] != "Created by" ||
              rawCSV[0][4] != "Duration" ||
              rawCSV[0][5] != "Output standard" ||
              rawCSV[0][6] != "Status"
            ) {
            } else {
              let result = [];
              rawCSV.map((item, index) => {
                if (index > 0 && item.length === 7) {
                  let obj = {};
                  obj.syllabusName = item[0];
                  obj.syllabusCode = item[1];
                  obj.createdOn = item[2];
                  if (!obj.createdBy) {
                    obj.createdBy = {};
                  }
                  obj.createdBy.userName = item[3];
                  obj.duration = item[4];
                  obj.syllabusObjectiveList = item[5];
                  obj.syllabusStatus = item[6];
                  result.push(obj);
                }
              });
              updateFileCSV(result);
            }
          } else {
          }
        } else {
        }
      },
    });
  };

  const handleImportCSV = (even) => {
    if (even.target && even.target.files && even.target.files[0]) {
      let file = even.target.files[0];
      if (file.type !== "text/csv") {
        toast.error("CSV?");
      }
      setfile(file);
    }
  };

  const getUsersExport = (event, done) => {
    let result = [];
    if (dataFromParent && dataFromParent.length > 0) {
      result.push([
        "Syllabus",
        "Code",
        "Created on",
        "Created by",
        "Duration",
        "Output standard",
        "Status",
      ]);
      dataFromParent.map((item, index) => {
        let arr = [];
        arr[0] = item.syllabusName;
        arr[1] = item.syllabusCode;
        arr[2] = item.createdOn;
        arr[3] = item?.modifiedBy?.userName || item?.createdBy?.userName;
        arr[4] = item.duration;
        arr[5] = item.syllabusObjectiveList
        arr[6] = item.syllabusStatus;
        result.push(arr);
      });
      setDataExport(result);
      done();
    }
  };

  const { t } = useTranslation();

  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>{t("import_syllabus")}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form noValidate>
            <div className="row">
              <div className="col-sm-4">
                <Form.Label style={{ color: "black" }}>
                  {t("import_setting")}
                </Form.Label>
              </div>
              <div className="col-sm-4">
                <Form.Group controlId="File" as={Col} md="4">
                  <div>
                    {t("file")}(csv)<i style={{ color: "red" }}>*</i>
                  </div>
                </Form.Group>
              </div>
              <div className="col-sm-4">
                <label htmlFor="Import" className="btn btn-warning">
                  {t("select")}
                </label>
                <input
                  id="Import"
                  type="file"
                  hidden
                  onChange={(even) => handleImportCSV(even)}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-sm-4"></div>
              <div className="col-sm-4">
                <Form.Group controlId="Encodingtype">
                  <div>{t("encoding_type")}</div>
                </Form.Group>
              </div>
              <div className="col-sm-4">
                <Form.Select name="userType">
                  <option value="Autodetect">Auto detect </option>
                </Form.Select>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-4"></div>
              <div className="col-sm-4">
                <Form.Group controlId="Columnseperator">
                  <div>{t("column_separator")}</div>
                </Form.Group>
              </div>
              <div className="col-sm-4">
                <Form.Select name="userType">
                  <option value="Autodetect">Comma </option>
                </Form.Select>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-4"></div>
              <div className="col-sm-4">
                <Form.Group controlId="Importtemplate">
                  <div>{t("import_template")}</div>
                </Form.Group>
              </div>
              <div className="col-sm-4">
                <CSVLink
                  style={{ display: "flex-start" }}
                  data={dataExport}
                  filename={"users.csv"}
                  asyncOnClick={true}
                  onClick={getUsersExport}
                >
                  {t("download")}
                </CSVLink>
              </div>
            </div>
          </Form>
          <hr></hr>
          <Form noValidate>
            <div className="row">
              <div className="col-sm-4">
                <Form.Label style={{ color: "black" }}>
                  {t("duplicate_control")}
                </Form.Label>
              </div>
              <div className="col-sm-8">
                <Form.Group>
                  <div> {t("scanning")}</div>
                </Form.Group>
                <div style={{ textIndent: "5px", display: "inline-flex" }}>
                  <div>
                    <input type="checkbox" id="gridCheck" />
                    <label
                      className="form-check-label"
                      for="gridCheck"
                      style={{ fontWeight: "normal" }}
                    >
                      {t("program_id")}
                    </label>
                  </div>
                  <div>
                    <input type="checkbox" id="gridCheck1" />
                    <label
                      className="form-check-label"
                      for="gridCheck1"
                      style={{ fontWeight: "normal" }}
                    >
                      {t("program_name")}
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <br />
            <div className="row">
              <div className="col-sm-4" />
              <div className="col-sm-8">
                <Form.Group>
                  <div>{t("duplicate_handle")}</div>
                </Form.Group>
                <Form.Group>
                  <div style={{ textIndent: "5px", display: "inline-flex" }}>
                    <div>
                      <input type="radio" name="radioCheck" />
                      <label
                        className="form-check-label"
                        for="radioCheck"
                        style={{ fontWeight: "normal" }}
                      >
                        {t("allow")}
                      </label>
                    </div>
                    <div>
                      <input type="radio" name="radioCheck1" />
                      <label
                        className="form-check-label"
                        for="radioCheck1"
                        style={{ fontWeight: "normal" }}
                      >
                        {t("replace")}
                      </label>
                    </div>
                    <div>
                      <input type="radio" name="radioCheck2" />
                      <label
                        className="form-check-label"
                        for="radioCheck2"
                        style={{ fontWeight: "normal" }}
                      >
                        {t("skip")}
                      </label>
                    </div>
                  </div>
                </Form.Group>
              </div>
            </div>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            {t("close")}
          </Button>
          <Button variant="primary" onClick={() => handleSaveUser()}>
            {t("save_changes")}
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default ImportSyllabus;
