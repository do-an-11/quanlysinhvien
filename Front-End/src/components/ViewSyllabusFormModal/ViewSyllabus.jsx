import _ from "lodash";
import { useEffect, useState } from "react";
import { Dropdown, DropdownButton } from "react-bootstrap";
import Table from "react-bootstrap/Table";
import DatePicker from "react-datepicker";
import { useTranslation } from "react-i18next";
import { BiPlusCircle } from "react-icons/bi";
import { FaPlusCircle, FaSortAmountDownAlt, FaSortAmountUp } from "react-icons/fa";
import { HiDuplicate } from "react-icons/hi";
import { ImSearch } from "react-icons/im";
import { MdDelete, MdEdit, MdFileUpload } from "react-icons/md";
import ReactPaginate from 'react-paginate';
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import Select from "react-select";
import "../../css/style.css";
import style from "../../css/syllabus.module.css";
import { actSyllabusDeleteAPIAsync, actSyllabusDuplicateAPIAsync, actSyllabusListAPIGetAsync, actSyllabusRecoverAPIAsync } from "../../store/syllabus/action";
import DeleteSyllabus from "./DeleteSyllabus";
import ImportSyllabus from "./ImportSyllabus";

function ViewSyllabus() {
  const [listSyllabuss, setListSyllabuss] = useState([]);
  const [IsShowModalImport, setIsShowModalImport] = useState(false);

  const [IsShowModalDelete, setIsShowModalDelete] = useState(false);
  const [dataDelete, setDataDelete] = useState();

  const [soreBy, setSoreBy] = useState("asc");
  const [sortField, setSortField] = useState("");

  const [selectedOptions, setSelectedOptions] = useState([]);

  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  const [pageNumber, setPageNumber] = useState(0);
  const itemsPerPage = 5; // Số lượng mục trên mỗi trang
  const pagesVisited = pageNumber * itemsPerPage;
  const currentItems = listSyllabuss.slice(pagesVisited, pagesVisited + itemsPerPage);
  const pageCount = Math.ceil(listSyllabuss.length / itemsPerPage);

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  const dispatch = useDispatch();
  let token = localStorage.getItem("ACCESS_TOKEN");

  let getallsyllabus = useSelector((state) => state.SYLLABUS.syllabus);
  console.log(getallsyllabus);
  useEffect(() => {
    dispatch(actSyllabusListAPIGetAsync(token));
    getSyllabuss();
  }, []);

  useEffect(() => {
    getSyllabuss();
  }, [getallsyllabus]);

  const handleClose = () => {
    setIsShowModalImport(false);
    setIsShowModalDelete(false);
  };

  const getSyllabuss = () => {
    setListSyllabuss(getallsyllabus);
  };

  const handleSort = (soreBy, sortField) => {
    setSoreBy(soreBy);
    setSortField(sortField);
    let cloneListSyllabuss = _.cloneDeep(listSyllabuss);
    cloneListSyllabuss = _.orderBy(cloneListSyllabuss, [sortField], [soreBy]);
    setListSyllabuss(cloneListSyllabuss);
  };

  const handleSelectChange = (selected) => {
    setSelectedOptions(selected);
    let term = selected.map((e) => e.value);
    if (term && term.length > 0) {
      let cloneListSyllabuss = _.cloneDeep(getallsyllabus);
      cloneListSyllabuss = getallsyllabus.filter((item) =>
        term.includes(item.syllabusName)
      );
      setListSyllabuss(cloneListSyllabuss);
    } else {
      setListSyllabuss(_.cloneDeep(getallsyllabus));
    }
  };

  const handleDate = (date) => {
    getSyllabuss();
    const [start, end] = date;
    setStartDate(start);
    setEndDate(end);
    if (start && end) {
      let arr = listSyllabuss.map((e) => new Date(e.createdOn).getTime());
      let newStartTime = new Date(start).getTime();
      let newEndTime = new Date(end).getTime();
      let first = arr.filter((e) => e >= newStartTime);
      let second = first.filter((e) => e <= newEndTime);
      let cloneListSyllabuss = listSyllabuss.filter((item) => {
        return second.includes(new Date(item.createdOn).getTime());
      });
      setListSyllabuss(cloneListSyllabuss);
    } else {
      getSyllabuss();
    }
  };

  const updateFileCSV = (syllabus) => {
    setIsShowModalImport(false);
    setListSyllabuss(syllabus);
  };

  const clonesearch = getallsyllabus.map((e) => {
    return { value: e.syllabusName, label: e.syllabusName };
  });

  const customStyles = {
    control: (provided) => ({
      ...provided,
      border: "none",
      boxShadow: "none",
    }),
  };

  const CustomMultiValue = ({ children, ...props }) => (
    <div {...props}>
      <span style={{ display: "none" }}>{children}</span>
    </div>
  );
  const handleDelete = (topicCode) => {
    setIsShowModalDelete(true);
    setDataDelete(topicCode);
  }
  const handleDeleteSyllabusFromModal = (topicCode) => {
    dispatch(actSyllabusDeleteAPIAsync(topicCode, token))
  }
  const handleRecover = (topicCode) => {
    dispatch(actSyllabusRecoverAPIAsync(topicCode, token));
  }
  const handleDuplicate = (topicCode) => {
    dispatch(actSyllabusDuplicateAPIAsync(topicCode, token))
  }
  const { t } = useTranslation();

  return (
    <>
      <div>
        <span style={{ fontSize: "50px" }}>{t("syllabus")}</span>
      </div>
      <div className={`my-3 ${style.add_new} d-sm-flex`}>
        <div className="my-3 d-sm-flex">
          <div style={{ width: "200px" }}>
            <Select
              isMulti
              value={selectedOptions}
              options={clonesearch}
              onChange={(selected) => handleSelectChange(selected)}
              placeholder={
                <>
                  <ImSearch /> {t("select")} ...
                </>
              }
              components={{
                MultiValue: CustomMultiValue,
              }}
              isClearable={false}
            />
          </div>
          <div style={{ padding: "5px" }}> </div>
          <DatePicker
            selected={startDate}
            onChange={(date) => handleDate(date)}
            maxDate={new Date()}
            selectsRange
            startDate={startDate}
            endDate={endDate}
            showIcon
          />
        </div>
        <div className={`${style.group_btns} mt-sm-0 mt-2`}>
          <label
            className="btn btn-warning"
            onClick={() => setIsShowModalImport(true)}
          >
            <MdFileUpload />
            {t("import")}
          </label>
          <Link to="/syllabuscreate" className="btn btn-success">
            <BiPlusCircle />
            {t("add_syllabus")}
          </Link>
        </div>
      </div>
      <div style={{ width: "400px", marginTop: "-20px" }}>
        <Select
          components={{ DropdownIndicator: null }}
          onChange={(selected) => handleSelectChange(selected)}
          styles={customStyles}
          isMulti
          menuIsOpen={false}
          value={selectedOptions}
          isClearable={false}
          placeholder={<></>}
        />
      </div>
      <div className={`${style.customize_table}`} style={{ width: "100%" }}>
        <Table striped hover>
          <thead>
            <tr>
              <th style={{ textAlign: "center" }}>
                <div className="sort-header">
                  <span>{t("syllabus")}</span>
                  <span> </span>
                  <span>
                    {sortField === "syllabusName" ? (
                      <></>
                    ) : (
                      <button
                        onClick={() => handleSort("desc", "syllabusName")}
                      >
                        <FaSortAmountDownAlt />
                      </button>
                    )}
                  </span>

                  {sortField === "syllabusName" && (
                    <button
                      onClick={() =>
                        handleSort(
                          soreBy === "asc" ? "desc" : "asc",
                          "syllabusName"
                        )
                      }
                    >
                      {soreBy === "asc" ? (
                        <FaSortAmountDownAlt />
                      ) : (
                        <FaSortAmountUp />
                      )}
                    </button>
                  )}
                </div>
              </th>
              <th style={{ textAlign: "center" }}>
                <div className="sort-header">
                  <span>{t("code")}</span>
                  <span> </span>
                  <span>
                    {sortField === "syllabusCode" ? (
                      <></>
                    ) : (
                      <button
                        onClick={() => handleSort("desc", "syllabusCode")}
                      >
                        <FaSortAmountDownAlt />
                      </button>
                    )}
                  </span>

                  {sortField === "syllabusCode" && (
                    <button
                      onClick={() =>
                        handleSort(
                          soreBy === "asc" ? "desc" : "asc",
                          "syllabusCode"
                        )
                      }
                    >
                      {soreBy === "asc" ? (
                        <FaSortAmountDownAlt />
                      ) : (
                        <FaSortAmountUp />
                      )}
                    </button>
                  )}
                </div>
              </th>
              <th style={{ textAlign: "center" }}>
                <div className="sort-header">
                  <span>{t("create_on")}</span>
                  <span> </span>
                  <span>
                    {sortField === "createdOn" ? (
                      <></>
                    ) : (
                      <button onClick={() => handleSort("desc", "createdOn")}>
                        <FaSortAmountDownAlt />
                      </button>
                    )}
                  </span>

                  {sortField === "createdOn" && (
                    <button
                      onClick={() =>
                        handleSort(
                          soreBy === "asc" ? "desc" : "asc",
                          "createdOn"
                        )
                      }
                    >
                      {soreBy === "asc" ? (
                        <FaSortAmountDownAlt />
                      ) : (
                        <FaSortAmountUp />
                      )}
                    </button>
                  )}
                </div>
              </th>
              <th style={{ textAlign: "center" }}>
                <div className="sort-header">
                  <span>{t("create_by")}</span>
                  <span> </span>
                  <span>
                    {sortField === "createdBy.userName" ? (
                      <></>
                    ) : (
                      <button onClick={() => handleSort("desc", "createdBy.userName")}>
                        <FaSortAmountDownAlt />
                      </button>
                    )}
                  </span>
                  {sortField === "createdBy.userName" && (
                    <button
                      onClick={() =>
                        handleSort(
                          soreBy === "asc" ? "desc" : "asc",
                          "createdBy.userName"
                        )
                      }
                    >
                      {soreBy === "asc" ? (
                        <FaSortAmountDownAlt />
                      ) : (
                        <FaSortAmountUp />
                      )}
                    </button>
                  )}
                </div>
              </th>
              <th style={{ textAlign: "center" }}>
                <div className="sort-header">
                  <span>{t("duration")}</span>
                  <span> </span>
                  <span>
                    {sortField === "duration" ? (
                      <></>
                    ) : (
                      <button onClick={() => handleSort("desc", "duration")}>
                        <FaSortAmountDownAlt />
                      </button>
                    )}
                  </span>
                  {sortField === "duration" && (
                    <button
                      onClick={() =>
                        handleSort(
                          soreBy === "asc" ? "desc" : "asc",
                          "duration"
                        )
                      }
                    >
                      {soreBy === "asc" ? (
                        <FaSortAmountDownAlt />
                      ) : (
                        <FaSortAmountUp />
                      )}
                    </button>
                  )}
                </div>
              </th>
              <th style={{ textAlign: "center" }}> {t("Output standard")}</th>
              <th style={{ textAlign: "center" }}> {t("status")}</th>
              <th style={{ textAlign: "center" }}> {t("option")}</th>
            </tr>
          </thead>
          <tbody>
            {currentItems &&
              currentItems.length > 0 &&
              currentItems.map((item, index) => {
                return (
                  <tr key={`Syllabuss-${index}`}>
                    <td style={{ fontWeight: "bold" }}>{item.syllabusName}</td>
                    <td>{item.syllabusCode}</td>
                    <td>
                      {item.createdOn}
                    </td>
                    <td>{item.createdBy?.userName}</td>
                    <td>{item.duration} days</td>
                    <td>
                      {item.syllabusObjectiveList.length > 0 ?
                        <div
                          style={{
                            width: "100px",
                            padding: "3px 7px 5px 5px",
                            borderRadius: "10px",
                            fontWeight: "bold",
                            fontSize: "18px",
                            backgroundColor: "#abab63",
                            color: "#f2f2aa",
                            marginLeft: "30%"
                          }}
                        >
                          {item.syllabusObjectiveList}
                        </div>
                        :
                        <>
                        </>
                      }
                    </td>
                    <td style={{ width: "150px" }}>
                      {item.syllabusStatus === "active" ? (
                        <div
                          style={{
                            padding: "3px 7px 5px 5px",
                            borderRadius: "10px",
                            fontWeight: "bold",
                            fontSize: "18px",
                            backgroundColor:
                              item.syllabusStatus === "active" ? "#b7ffbf" : "#",
                            color: item.syllabusStatus === "active" ? "#00ab08" : "",
                          }}
                        >
                          {item.syllabusStatus}
                        </div>
                      ) : (
                        <>
                          {item.syllabusStatus === "inactive" ? (
                            <div
                              style={{
                                padding: "3px 5px 5px 5px",
                                borderRadius: "10px",
                                fontWeight: "bold",
                                fontSize: "18px",
                                backgroundColor:
                                  item.syllabusStatus === "inactive" ? "#fcc1c1" : "",
                                color:
                                  item.syllabusStatus === "inactive" ? "#ff0000" : "",
                              }}
                            >
                              {item.syllabusStatus}
                            </div>
                          ) : (
                            <>
                              {item.syllabusStatus === "draft" ? (
                                <div
                                  style={{
                                    padding: "3px 5px 5px 5px",
                                    borderRadius: "10px",
                                    fontWeight: "bold",
                                    fontSize: "18px",
                                    backgroundColor:
                                      item.syllabusStatus === "draft" ? "#9da19e" : "#",
                                    color: item.syllabusStatus === "draft" ? "#e1e6e2" : "",
                                  }}
                                >
                                  {item.syllabusStatus}
                                </div>
                              ) :
                                (<></>)
                              }
                            </>
                          )}
                        </>
                      )}
                    </td>
                    <td>
                      {item.syllabusStatus === "active" ?
                        <DropdownButton
                          title={t("options")}
                          variant="warning"
                          style={{ width: "100%" }}
                        >
                          <Dropdown.Item
                            style={{
                              borderRadius: "5px",
                              backgroundColor: "#85c5c7",
                            }}
                          >

                            <Link to="/createprogram" style={{ color: "black" }}>
                              <FaPlusCircle />
                              <>  </>
                              {t("add")}
                            </Link>

                          </Dropdown.Item>
                          <Dropdown.Item
                            style={{
                              borderRadius: "5px",
                              backgroundColor: "#85c5c7",
                              marginTop: "6px",
                            }}
                          >
                            <Link to={`/syllabuscreate/${item?.syllabusCode}`} style={{ color: "black" }}>
                              <MdEdit />
                              <>  </>
                              {t("edit")}
                            </Link>

                          </Dropdown.Item>

                          <Dropdown.Item
                            style={{
                              borderRadius: "5px",
                              backgroundColor: "#85c5c7",
                              marginTop: "6px",
                            }}
                            onClick={() => handleDuplicate(item.syllabusCode)}
                          >
                            <HiDuplicate />
                            <>  </>
                            {t("duplicate")}
                          </Dropdown.Item>

                          <Dropdown.Item
                            style={{
                              borderRadius: "5px",
                              backgroundColor: "#85c5c7",
                              marginTop: "6px",
                            }}
                            onClick={() => handleDelete(item.syllabusCode)}
                          >
                            <MdDelete />
                            <>  </>
                            {t("delete")}
                          </Dropdown.Item>
                        </DropdownButton>
                        :
                        <DropdownButton
                          title={t("options")}
                          variant="warning"
                          style={{ width: "100%" }}
                        >
                          <Dropdown.Item
                            style={{
                              borderRadius: "5px",
                              backgroundColor: "#85c5c7",
                            }}
                            onClick={() => handleRecover(item.syllabusCode)}
                          >
                            {t("recover")}
                          </Dropdown.Item>
                        </DropdownButton>
                      }

                    </td>
                  </tr>
                );
              })}
          </tbody>
        </Table>
      </div>
      <ReactPaginate
        breakLabel="..."
        nextLabel="next >"
        onPageChange={changePage}
        pageCount={pageCount}
        previousLabel="< previous"

        pageClassName='page-item'
        pageLinkClassName='page-link'
        previousClassName='page-item'
        previousLinkClassName='page-link'
        nextClassName='page-item'
        nextLinkClassName='page-link'
        breakClassName='page-item'
        breakLinkClassName='page-link'
        containerClassName='pagination'
        activeClassName='active'
      />
      <ImportSyllabus
        show={IsShowModalImport}
        handleClose={handleClose}
        dataFromParent={listSyllabuss}
        updateFileCSV={updateFileCSV}
      />
      <DeleteSyllabus
        show={IsShowModalDelete}
        handleClose={handleClose}
        dataDelete={dataDelete}
        handleDeleteSyllabusFromModal={handleDeleteSyllabusFromModal}
      />
    </>
  );
}
export default ViewSyllabus;
