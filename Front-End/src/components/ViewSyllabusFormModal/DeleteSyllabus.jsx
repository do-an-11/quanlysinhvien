
import { Button, Form, Modal } from "react-bootstrap";
const DeleteSyllabus = (props) => {
  const { show, handleClose, dataDelete, handleDeleteSyllabusFromModal } = props;

  const confirmDelete = () => {
    handleClose();
    handleDeleteSyllabusFromModal(dataDelete);
  };

  return (
    <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false}>
      <Modal.Header closeButton>
        <Modal.Title>Delete Day</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form.Group controlId="Syllabus">
          <div>Are you sure you want to delete syllabus?</div>
        </Form.Group>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="danger" color="error" onClick={() => confirmDelete()}>
          Delete
        </Button>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
export default DeleteSyllabus;
