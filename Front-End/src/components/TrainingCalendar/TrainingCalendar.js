import React, { useState } from "react";

import moment from "moment";
import { Calendar, momentLocalizer } from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";
import events from "./event";

moment.locale("en-GB");
const localizer = momentLocalizer(moment);

export function TrainingCalendar() {
  const [eventsData, setEventsData] = useState(events);

  const handleSelect = ({ start, end }) => {
    const title = window.prompt("New Event name");
    if (title) {
      let segment = "Morning";
      const startHour = start.getHours();
      if (startHour >= 12) segment = "Noon";
      if (startHour >= 18) segment = "Night";

      setEventsData([
        ...eventsData,
        {
          start,
          end,
          title,
          segment,
        },
      ]);
    }
  };

  const eventPropGetter = (event) => {
    switch (event.segment) {
      case "Morning":
        return {
          style: {
            backgroundColor: "lightblue",
          },
        };
      case "Noon":
        return {
          style: {
            backgroundColor: "lightgreen",
          },
        };
      case "Night":
        return {
          style: {
            backgroundColor: "lightpink",
          },
        };
      default:
        return {};
    }
  };

  return (
    <div className="App">
      <Calendar
        views={["day", "agenda", "work_week", "month"]}
        selectable
        localizer={localizer}
        defaultDate={new Date()}
        defaultView="month"
        events={eventsData}
        eventPropGetter={eventPropGetter} 
        style={{ height: "100vh" }}
        onSelectEvent={(event) => alert(event.title)}
        onSelectSlot={handleSelect}
      />
    </div>
  );
}
