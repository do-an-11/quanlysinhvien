import React, { useState } from "react";

import styles from "../../css/class.module.css";

export const Dropdown = ({ open, trigger, options, selected, onChange }) => {
  const [isDropdownOpen, setDropdownOpen] = useState(open);

  const handleTriggerClick = () => {
    setDropdownOpen(!isDropdownOpen);
  };

  const handleOptionClick = (option) => {
    onChange(option);
    setDropdownOpen(false);
  };

  return (
    <div className="dropdown">
      <button
        onClick={handleTriggerClick}
        style={{
          borderStyle: "solid",
          borderRadius: "8px",
          borderColor: "grey",
          marginTop: "0.4px",
        }}
      >
        {trigger}
      </button>
      {isDropdownOpen && (
        <ul className={styles.menu}>
          {options?.map((option, index) => (
            <li
              style={{ cursor: "pointer", backgroundColor: "#F0F0F0" }}
              key={index}
              className="menu-item"
              onClick={() => handleOptionClick(option)}
            >
              {option.label}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};
