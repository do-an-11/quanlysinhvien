import React from "react";

import { useTranslation } from "react-i18next";

import "../css/style.css";

export default function Footer() {
  const { t } = useTranslation();
  return <div className="footer-container">{t("footer")}</div>;
}
