import React, { useEffect, useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import Select from "react-select";
import {
  actUserGetAsync,
  actUserGetTraineeAsyncAPI,
} from "../../store/user/action";

import styles from "../../css/class.module.css";
import { useTranslation } from "react-i18next";

function AttendeeListComponent({ listItem, item }) {
  const [selectedAttendeeName, setSelectedAttendeeName] = useState(null);
  const [options, setOptions] = useState([]);
  const dispatch = useDispatch();
  const listUser = useSelector((state) => state.USER.trainee);
  const [form, setForm] = useState({});
  const token = localStorage.getItem("ACCESS_TOKEN");
  const handleSelectChange = (selectedOption) => {
    setSelectedAttendeeName(selectedOption);
    listItem({
      ...form,
      attendeeList: selectedOption.map((item) => item.value),
    });
  };

  useEffect(() => {
    dispatch(actUserGetAsync());
    dispatch(actUserGetTraineeAsyncAPI(token));
  }, []);
  useEffect(() => {
    setForm({ ...item });
    setSelectedAttendeeName(
      item?.attendeeList?.map((e) => {
        return { label: e, value: e };
      })
    );
  }, [item]);

  useEffect(() => {
    const apiOptions = listUser.map((e) => ({
      label: e.email,
      value: e.email,
    }));
    setOptions(apiOptions);
  }, [listUser]);

  const { t } = useTranslation();

  return (
    <div>
      <div className={styles.header_content}>
        <div>{t("attendee_list")}</div>
        <div className={styles.showCourse}>
          <Select
            isMulti
            onChange={handleSelectChange}
            options={options}
            value={selectedAttendeeName}
          />
        </div>
      </div>
      {selectedAttendeeName?.length > 0 && (
        <table className="table">
          <thead>
            <tr style={{ textAlign: "center" }}>
              <th>{t("name")}</th>
              <th>{t("email")}</th>
              <th>{t("phone")}</th>
              <th>{t("gender")}</th>
              <th>{t("dob")}</th>
            </tr>
          </thead>
          <tbody>
            {listUser
              ?.filter((item) =>
                selectedAttendeeName.some(
                  (selected) => selected.label === item.email
                )
              )
              .map((item) => (
                <tr key={item.id}>
                  <td>{item.name}</td>
                  <td>{item.email}</td>
                  <td>{item.phone}</td>
                  <td>{item.gender}</td>
                  <td>{item.dob}</td>
                </tr>
              ))}
          </tbody>
        </table>
      )}
      <div className={styles.footer_content}></div>
    </div>
  );
}

export default AttendeeListComponent;
