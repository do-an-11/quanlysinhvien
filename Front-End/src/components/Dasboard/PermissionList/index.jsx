import React, { useState } from "react";
import Select from "react-select";
import "./user-form-search.css";
import { MDBDataTableV5 } from "mdbreact";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import { ImSearch } from "react-icons/im";
import { BiFilter } from "react-icons/bi";

const data = [
  {
    employeeId: "EMP001",
    fullName: "John Doe",
    dateOfBirth: "1990-05-15",
    email: "john.doe@example.com",
    gender: "Female",
    status: "Active",
    options: "",
  },
  {
    employeeId: "EMP002",
    fullName: "Jane Smith",
    dateOfBirth: "1985-08-22",
    email: "jane.smith@example.com",
    gender: "Female",
    status: "Inactive",
    options: "",
  },
  {
    employeeId: "EMP003",
    fullName: "Alice Johnson",
    dateOfBirth: "1993-03-10",
    email: "alice.johnson@example.com",
    phone: "555-123-4567",
    department: "Finance",
    job: "Financial Analyst",
    status: "Active",
    options: "",
  },
  {
    employeeId: "EMP003",
    fullName: "Alice Johnson",
    dateOfBirth: "1993-03-10",
    email: "alice.johnson@example.com",
    phone: "555-123-4567",
    department: "Finance",
    job: "Financial Analyst",
    status: "Active",
    options: "",
  },
  {
    employeeId: "EMP003",
    fullName: "Alice Johnson",
    dateOfBirth: "1993-03-10",
    email: "alice.johnson@example.com",
    phone: "555-123-4567",
    department: "Finance",
    job: "Financial Analyst",
    status: "Active",
    options: "",
  },
  {
    employeeId: "EMP003",
    fullName: "Alice Johnson",
    dateOfBirth: "1993-03-10",
    email: "alice.johnson@example.com",
    phone: "555-123-4567",
    department: "Finance",
    job: "Financial Analyst",
    status: "Active",
    options: "",
  },
  {
    employeeId: "EMP003",
    fullName: "Alice Johnson",
    dateOfBirth: "1993-03-10",
    email: "alice.johnson@example.com",
    phone: "555-123-4567",
    department: "Finance",
    job: "Financial Analyst",
    status: "Active",
    options: "",
  },
  {
    employeeId: "EMP003",
    fullName: "Alice Johnson",
    dateOfBirth: "1993-03-10",
    email: "alice.johnson@example.com",
    phone: "555-123-4567",
    department: "Finance",
    job: "Financial Analyst",
    status: "Active",
    options: "",
  },

  {
    employeeId: "EMP003",
    fullName: "Alice Johnson",
    dateOfBirth: "1993-03-10",
    email: "alice.johnson@example.com",
    phone: "555-123-4567",
    department: "Finance",
    job: "Financial Analyst",
    status: "Active",
    options: "",
  },
  {
    employeeId: "EMP003",
    fullName: "Alice Johnson",
    dateOfBirth: "1993-03-10",
    email: "alice.johnson@example.com",
    phone: "555-123-4567",
    department: "Finance",
    job: "Financial Analyst",
    status: "Active",
    options: "",
  },
];
const options = [
  { value: "apple", label: "Apple" },
  { value: "banana", label: "Banana" },
  { value: "cherry", label: "Cherry" },
  { value: "date", label: "Date" },
  { value: "grape", label: "Grape" },
];
function UserFormSearch() {
  const [selectedOptions, setSelectedOptions] = useState([]);

  const handleSelectChange = (selected) => {
    setSelectedOptions(selected);
  };

  const handleFilterClick = () => {};

  return (
    <div className="manager">
      <div className="row">
        <div className="col-md-2">
          {" "}
          <Select
            isMulti
            options={options}
            value={selectedOptions}
            onChange={handleSelectChange}
            placeholder={
              <>
                <ImSearch /> Select fruits...{" "}
              </>
            }
          />
        </div>
        <div className="col-md-6">
          {" "}
          <Button variant="primary" onClick={handleFilterClick}>
            <BiFilter /> Filter
          </Button>
        </div>
      </div>

      <div className="row">
        <div className="table-container">
          <MDBDataTableV5
            className="custom-table"
            data={{
              columns: [
                {
                  label: "Employee ID",
                  field: "employeeId",
                  width: 150,
                },
                {
                  label: "Full Name",
                  field: "fullName",
                  width: 150,
                },
                {
                  label: "Date of Birth",
                  field: "dateOfBirth",
                  width: 150,
                },
                {
                  label: "Email",
                  field: "email",
                  width: 200,
                },
                {
                  label: "Phone",
                  field: "phone",
                  width: 150,
                },
                {
                  label: "Department",
                  field: "department",
                  width: 150,
                },
                {
                  label: "Job",
                  field: "job",
                  width: 150,
                },
                {
                  label: "Status",
                  field: "status",
                  width: 100,
                },
                {
                  label: "Option",
                  field: "options",
                  sort: "disabled",
                  width: 100,
                },
              ],
              rows: data.map((employee) => ({
                employeeId: employee.employeeId,
                fullName: employee.fullName,
                dateOfBirth: employee.dateOfBirth,
                email: employee.email,
                phone: employee.phoneNumber,
                department: employee.department,
                job: employee.job,
                status: employee.status ? "Active" : "Disable",
                options: (
                  <Link to={`/manage/${employee.employeeId}`}>Edit</Link>
                ),
              })),
            }}
            hover
            entriesOptions={[5, 10, 20]}
            entries={10}
            pagesAmount={5}
            paging={true}
            searchTop={false}
            searchBottom={false}
            tbodyCustomRow={(row, rowIndex) => {
              return {
                onDoubleClick: row.clickEvent,
              };
            }}
          />
        </div>
      </div>
    </div>
  );
}

export default UserFormSearch;
