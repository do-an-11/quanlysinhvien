import React from "react";

import "react-big-calendar/lib/css/react-big-calendar.css";
import "react-calendar/dist/Calendar.css";
import DepartmentCounter from "./DepartmentCounter";
import EmployeeCounter from "./EmployeeCounter";
import JobCounter from "./JobCounter";

function Overall(props) {
  return (
    <div className="col-lg-8">
      <div className="col-12">
        <div className="static">
          <h3>Overall</h3>
          <hr />
          <p messages={{ year: "Year" }}> </p>
          <h5>Departments</h5>
          <DepartmentCounter />
          <hr />
          <h5>Employess</h5>
          <EmployeeCounter />
          <hr />
          <h5>Jobs</h5>
          <JobCounter />
        </div>
      </div>
    </div>
  );
}

export default Overall;
