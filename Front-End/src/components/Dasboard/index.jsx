import React from "react";

import CheckLog from "./CheckLog";
import Overall from "./Overall";
import Status from "./Status";
import TotalDepartment from "./TotalDepartment";
import TotalEmployee from "./TotalEmployee";

function Dasboard(props) {
  return (
    <div className="dashboardpage">
      <div className="news">
        <div className="row">
          <TotalEmployee />
          <Status />
          <TotalDepartment />
        </div>
        <div className="row" style={{ alignItems: "center" }}>
          <Overall />
          <CheckLog />
        </div>
      </div>
    </div>
  );
}

export default Dasboard;
