import React from "react";

import Chart from "react-apexcharts";

function CheckLog(props) {
  const dataPie = {
    labels: ["Hours Worked", "Remaining Hours"],
    series: [8, 3],
  };

  const options = {
    labels: ["Hours Worked", "Remaining Hours"],
    colors: ["#84E0BE", "#9A3434"],
  };

  return (
    <div className="col-lg-4">
      <div className="col-lg-12">
        <div className="checklog">
          <div className="row">
            <h2>Attendance</h2>
          </div>
          <div className="row">
            <div>
              <Chart options={options} series={dataPie.series} type="donut" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CheckLog;
