import React, { useEffect } from "react";

import { Route, Routes, useNavigate } from "react-router-dom";
import ChangePassWord from "../pages/ChangePassWord";
import ClassPage from "../pages/ClassPage";
import HomePage from "../pages/HomePage";
import LearningMaterialsPage from "../pages/LearningMaterialsPage";
import LoginPage from "../pages/LoginPage";
import ProfilePage from "../pages/ProfilePage";
import ProjectPage from "../pages/ProjectPage";
import { SyllabusCreate } from "../pages/SyllabusCreate";
import SyllabusPage from "../pages/SyllabusPage";
import TrainingCalendarPage from "../pages/TrainingCalendarPage";
import TrainingProgramPage from "../pages/TrainingProgramPage";
import Navbar from "./Navbar";

import PermissionPage from "../pages/PermissionPage";
import SettingPage from "../pages/SettingPage";
import UserListPage from "../pages/UserListPage";

import { useDispatch } from "react-redux";
import { CreateClassPage } from "../pages/CreateClassPage";
import { CreateProgramPage } from "../pages/CreateProgramPage";
import ForgotPassword from "../pages/ForgotPasswordPage";
import PermissionDenied from "../pages/PermissionDenied";
import { UserServices } from "../services/userServices";
import { actGetPermissionsAsync } from "../store/permission/action";
import { actUserGetAsyncAPI, actUserLogin } from "../store/user/action";

export default function Body() {
  const [isSidebarOpen, setIsSidebarOpen] = React.useState(false);
  const toggleSidebar = () => {
    setIsSidebarOpen(!isSidebarOpen);
  };
  const token = localStorage.getItem("ACCESS_TOKEN");
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    UserServices.fetchMe(token)
      .then((res) => {
        if (res.data && res.data.payload) {
          const currentUser = res.data.payload;
          const role = res.data.message;
          dispatch(actUserLogin(currentUser, token, role));
          dispatch(actGetPermissionsAsync(token));
          dispatch(actUserGetAsyncAPI(token));
        } else {
          alert("Please login");
        }
      })
      .catch((err) => {
        if (err.response) {
        } else {
          alert("An error occurred. Please login.");
        }
        navigate("/login");
      });
  }, []);

  return (
    <div className={`main-body ${isSidebarOpen ? "sidebar-open" : ""}`}>
      <div className="main-content">
        {token !== "null" && (
          <Navbar isSidebarOpen={isSidebarOpen} toggleSidebar={toggleSidebar} />
        )}
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/learning" element={<LearningMaterialsPage />} />
          <Route path="/usermanage" element={<UserListPage />} />
          <Route path="/permissionmanage" element={<PermissionPage />} />
          <Route path="/manage/:id" element={<UserListPage />} />
          <Route path="/profile" element={<ProfilePage />} />
          <Route path="/password" element={<ChangePassWord />} />
          <Route path="/project" element={<ProjectPage />} />
          <Route path="/syllabus" element={<SyllabusPage />} />
          <Route path="/syllabuscreate" element={<SyllabusCreate />} />
          <Route path="/syllabuscreate/:id" element={<SyllabusCreate />} />
          <Route path="/training" element={<TrainingCalendarPage />} />
          <Route path="/program" element={<TrainingProgramPage />} />
          <Route path="/class" element={<ClassPage />} />
          <Route path="/createclass/:id" element={<CreateClassPage />} />
          <Route path="/createclass" element={<CreateClassPage />} />
          <Route path="/createprogram/:id" element={<CreateProgramPage />} />
          <Route path="/createprogram" element={<CreateProgramPage />} />
          <Route path="/setting" element={<SettingPage />} />
          <Route
            path="/login"
            element={<LoginPage setIsSidebarOpen={setIsSidebarOpen} />}
          />
          <Route path="/forgot" element={<ForgotPassword />} />
          <Route path="/denied" element={<PermissionDenied />} />
        </Routes>
      </div>
    </div>
  );
}
