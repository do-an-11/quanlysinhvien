import React, { useState } from "react";

import {
  faBars,
  faBook,
  faBriefcase,
  faCalendarAlt,
  faChalkboardTeacher,
  faChevronDown,
  faCog,
  faFileAlt,
  faHome,
  faTimes,
  faUsers,
} from "@fortawesome/free-solid-svg-icons";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useSelector } from "react-redux";
import { Link, NavLink } from "react-router-dom";

import "../css/style.css";
import { useTranslation } from "react-i18next";

export default function Navbar({ isSidebarOpen, toggleSidebar }) {
  const [activeSubMenu, setActiveSubMenu] = useState(null);

  const role = useSelector((state) => state.USER.role);

  const toggleSubMenu = (menuName) => {
    setActiveSubMenu(activeSubMenu === menuName ? null : menuName);
  };

  const [color, setColor] = useState(null);

  function handleChange(e) {
    setColor(e);
  }

  const { t } = useTranslation();

  return (
    <div className={`sidebar ${isSidebarOpen ? "open" : ""}`}>
      <button className="toggle-button" onClick={toggleSidebar}>
        {isSidebarOpen ? (
          <FontAwesomeIcon icon={faTimes} />
        ) : (
          <FontAwesomeIcon icon={faBars} />
        )}
      </button>
      <ul className="navlist">
        <NavLink
          to="/"
          activeClassName="active-link"
          onClick={() => handleChange("home")}
        >
          <li>
            <span style={color === "home" ? { color: "#f37022" } : {}}>
              <FontAwesomeIcon icon={faHome} /> {t("home")}
            </span>
          </li>
        </NavLink>
        <li onClick={() => toggleSubMenu("syllabus")}>
          <span>
            <FontAwesomeIcon icon={faBook} /> {t("syllabus")}
            <span
              className={`menu-icon ${activeSubMenu === "syllabus" ? "open" : ""
                }`}
            ></span>{" "}
            <FontAwesomeIcon icon={faChevronDown} />
          </span>
          {(activeSubMenu === "syllabus" ||
            color === "syllabus" ||
            color === "create-syllabus") && (
              <ul className="sub-menu">
                <Link
                  to="/syllabus"
                  activeClassName="active-link"
                  onClick={() => handleChange("syllabus")}
                >
                  <li style={color === "syllabus" ? { color: "#f37022" } : {}}>
                    {t("view_syllabus")}
                  </li>
                </Link>
                {role !== "USER" && (
                  <Link
                    to="/syllabuscreate"
                    activeClassName="active-link"
                    onClick={() => handleChange("create-syllabus")}
                  >
                    <li
                      style={
                        color === "create-syllabus" ? { color: "#f37022" } : {}
                      }
                    >
                      {t("create_syllabus")}
                    </li>
                  </Link>
                )}
              </ul>
            )}
        </li>
        {role !== "USER" && (
          <li onClick={() => toggleSubMenu("trainingProgram")}>
            <span>
              <FontAwesomeIcon icon={faBriefcase} /> {t("training_program")}
              <span
                className={`menu-icon ${activeSubMenu === "trainingProgram" ? "open" : ""
                  }`}
              ></span>{" "}
              <FontAwesomeIcon icon={faChevronDown} />
            </span>
            {(activeSubMenu === "trainingProgram" ||
              color === "view-program" ||
              color === "create-program") && (
                <ul className="sub-menu">
                  <Link
                    to="/program"
                    activeClassName="active-link"
                    onClick={() => handleChange("view-program")}
                  >
                    <li
                      style={color === "view-program" ? { color: "#f37022" } : {}}
                    >
                      {t("view_training_program")}
                    </li>
                  </Link>
                </ul>
              )}
          </li>
        )}
        {role !== "USER" && (
          <li onClick={() => toggleSubMenu("class")}>
            <span>
              <FontAwesomeIcon icon={faChalkboardTeacher} /> {t("class")}
              <span
                className={`menu-icon ${activeSubMenu === "class" ? "open" : ""
                  }`}
              ></span>{" "}
              <FontAwesomeIcon icon={faChevronDown} />
            </span>
            {(activeSubMenu === "class" ||
              color === "view-class" ||
              color === "create-class") && (
                <ul className="sub-menu">
                  <NavLink
                    to="/class"
                    activeClassName="active-link"
                    onClick={() => handleChange("view-class")}
                  >
                    <li
                      style={color === "view-class" ? { color: "#f37022" } : {}}
                    >
                      {t("view_class")}
                    </li>
                  </NavLink>
                </ul>
              )}
          </li>
        )}
        <NavLink to="/training" onClick={() => handleChange("training")}>
          <li style={color === "training" ? { color: "#f37022" } : {}}>
            <FontAwesomeIcon icon={faCalendarAlt} /> {t("training_calendar")}
          </li>
        </NavLink>
        {role !== "USER" && role !== "TRAINER" && (
          <li onClick={() => toggleSubMenu("userManagement")}>
            <span>
              <FontAwesomeIcon icon={faUsers} /> {t("user_management")}
              <span
                className={`menu-icon ${activeSubMenu === "userManagement" ? "open" : ""
                  }`}
              >
                <FontAwesomeIcon icon={faChevronDown} />
              </span>
            </span>
            {(activeSubMenu === "userManagement" ||
              color === "userList" ||
              color === "userPermission") && (
                <ul className="sub-menu">
                  <NavLink
                    to="/usermanage"
                    activeClassName="active-link"
                    onClick={() => handleChange("userList")}
                  >
                    <li style={color === "userList" ? { color: "#f37022" } : {}}>
                      {t("user_list")}
                    </li>
                  </NavLink>
                  <NavLink
                    to="/permissionmanage"
                    activeClassName="active-link"
                    onClick={() => handleChange("userPermission")}
                  >
                    <li
                      style={
                        color === "userPermission" ? { color: "#f37022" } : {}
                      }
                    >
                      {t("user_permission")}
                    </li>
                  </NavLink>
                </ul>
              )}
          </li>
        )}
        {role !== "USER" && (
          <NavLink
            to="/learning"
            activeClassName="active-link"
            onClick={() => handleChange("learning")}
          >
            <li style={color === "learning" ? { color: "#f37022" } : {}}>
              <FontAwesomeIcon icon={faFileAlt} /> {t("learn_material")}
            </li>
          </NavLink>
        )}
        <li onClick={() => toggleSubMenu("setting")}>
          <span>
            <FontAwesomeIcon icon={faCog} /> {t("setting")}
            <span
              className={`menu-icon ${activeSubMenu === "setting" ? "open" : ""
                }`}
            ></span>
            <FontAwesomeIcon icon={faChevronDown} />
          </span>
          {(activeSubMenu === "setting" ||
            color === "account-setting" ||
            color === "general-setting") && (
              <ul className="sub-menu">
                <NavLink
                  to="/setting"
                  activeClassName="active-link"
                  onClick={() => handleChange("account-setting")}
                >
                  <li
                    style={
                      color === "account-setting" ? { color: "#f37022" } : {}
                    }
                  >
                    {t("acc_setting")}
                  </li>
                </NavLink>
                <NavLink
                  to="/setting"
                  activeClassName="active-link"
                  onClick={() => handleChange("general-setting")}
                >
                  <li
                    style={
                      color === "general-setting" ? { color: "#f37022" } : {}
                    }
                  >
                    {t("general_setting")}
                  </li>
                </NavLink>
              </ul>
            )}
        </li>
      </ul>
    </div>
  );
}
