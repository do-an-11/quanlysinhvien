import { toast } from "react-toastify";
import { UserServices } from "../../services/userServices";

export const ADD_USER = "ADD_USER";
export const UPDATE_USER = "UPDATE_USER";
export const GET_ALL_USER = "GET_ALL_USER";
export const ACT_USER_LOGIN = "ACT_USER_LOGIN";
export const ACT_USER_NOT_FETCH_ME = "ACT_USER_NOT_FETCH_ME";
export const GET_ALL_USER_FAKE = "GET_ALL_USER_FAKE";
export const GET_ALL_TRAINER_USER = "GET_ALL_TRAINER_USER";
export const GET_ALL_ADMIN_USER = "GET_ALL_ADMIN_USER";
export const GET_ALL_ADMIN_AND_SUPERADMIN_USER = "GET_ALL_ADMIN_AND_SUPERADMIN_USER";
export const GET_ALL_TRAINEE_USER = "GET_ALL_TRAINEE_USER";
export function actUserGet(list) {
  return {
    type: GET_ALL_USER,
    payload: list,
  };
}
export function actUserTrainerGet(list) {
  return {
    type: GET_ALL_TRAINER_USER,
    payload: list,
  };
}
export function actUserTraineeGet(list) {
  return {
    type: GET_ALL_TRAINEE_USER,
    payload: list,
  };
}
export function actUserAdminGet(list) {
  return {
    type: GET_ALL_ADMIN_USER,
    payload: list,
  };
}
export function actUserAdminAndSuperAdminGet(list) {
  return {
    type: GET_ALL_ADMIN_AND_SUPERADMIN_USER,
    payload: list,
  };
}
export function actUserFakeGet(list) {
  return {
    type: GET_ALL_USER_FAKE,
    payload: list,
  };
}
export function actUserGetAsync() {
  return async (dispatch) => {
    const response = await UserServices.getAllUser();
    if (response.status === 200 || response.status === 201) {
      let item = response.data.filter((e) => e.status === "Active");
      dispatch(actUserFakeGet(item));
    } else {
      toast.error("get all user to fail");
    }
  };
}
function formatDob(dob) {
  const date = new Date(dob);
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, "0");
  const day = String(date.getDate()).padStart(2, "0");
  return `${year}-${month}-${day}`;
}
export function actUserGetAsyncAPI(token) {
  return async (dispatch) => {
    try {
      const response = await UserServices.getAllUserAPI(token);
      if (response.status === 200 || response.status === 201) {
        let data = response.data.payload;
        const formattedData = data.map((item) => ({
          name: item.name,
          role: item.role,
          phone: item.phone,
          status: item.status,
          email: item.email,
          dob: formatDob(item.dob),
          gender: item.gender,
          userId: item.userId,
          modifiedBy: item.modifiedBy,
          createdBy: item.createdBy,
        }));
        dispatch(actUserGet(formattedData));
      } else {
        toast.error("Get all user failed");
      }
    } catch (error) {
    }
  };
}

export function actUserGetTrainerAsyncAPI(token) {
  return async (dispatch) => {
    try {
      const response = await UserServices.getAllTrainerAPI(token);
      if (response.status === 200 || response.status === 201) {
        let data = response.data.payload;
        const formattedData = data.map((item) => ({
          name: item.name,
          role: item.role,
          phone: item.phone,
          status: item.status,
          email: item.email,
          dob: formatDob(item.dob),
          gender: item.gender,
          userId: item.userId,
          modifiedBy: item.modifiedBy,
          createdBy: item.createdBy,
        }));
        dispatch(actUserTrainerGet(formattedData));
      } else {
        toast.error("Get all user failed");
      }
    } catch (error) {
    }
  };
}
export function actUserGetTraineeAsyncAPI(token) {
  return async (dispatch) => {
    try {
      const response = await UserServices.getAllTraineeAPI(token);
      if (response.status === 200 || response.status === 201) {
        let data = response.data.payload;
        const formattedData = data.map((item) => ({
          name: item.name,
          role: item.role,
          phone: item.phone,
          status: item.status,
          email: item.email,
          dob: formatDob(item.dob),
          gender: item.gender,
          userId: item.userId,
          modifiedBy: item.modifiedBy,
          createdBy: item.createdBy,
        }));
        dispatch(actUserTraineeGet(formattedData));
      } else {
        toast.error("Get all user failed");
      }
    } catch (error) {
    }
  };
}

export function actUserGetAdminAsyncAPI(token) {
  return async (dispatch) => {
    try {
      const response = await UserServices.getAllAdminAPI(token);
      if (response.status === 200 || response.status === 201) {
        let data = response.data.payload;
        const formattedData = data.map((item) => ({
          name: item.name,
          role: item.role,
          phone: item.phone,
          status: item.status,
          email: item.email,
          dob: formatDob(item.dob),
          gender: item.gender,
          userId: item.userId,
          modifiedBy: item.modifiedBy,
          createdBy: item.createdBy,
        }));
        dispatch(actUserAdminGet(formattedData));
      } else {
        toast.error("Get all user failed");
      }
    } catch (error) {
    }
  };
}
export function actUserGetAdminAndSuperAdminAsyncAPI(token) {
  return async (dispatch) => {
    try {
      const response = await UserServices.getAllAdminAndSuperAdminAPI(token);
      if (response.status === 200 || response.status === 201) {
        let data = response.data.payload;
        const formattedData = data.map((item) => ({
          name: item.name,
          role: item.role,
          phone: item.phone,
          status: item.status,
          email: item.email,
          dob: formatDob(item.dob),
          gender: item.gender,
          userId: item.userId,
          modifiedBy: item.modifiedBy,
          createdBy: item.createdBy,
        }));
        dispatch(actUserAdminAndSuperAdminGet(formattedData));
      } else {
        toast.error("Get all user failed");
      }
    } catch (error) {
    }
  };
}

export function actPostUserAsync(user) {
  return async (dispatch) => {
    try {
      const response = await UserServices.postUser(user);
      if (response.status === 200 || response.status === 201) {
        toast.success("New User has been added successfully ~");
      }
      dispatch(actUserGetAsync());
    } catch (error) {
      console.error("An error occurred while making the request:", error);
      toast.error("An error occurred while making the request");
    }
  };
}
export function actPostUserAsyncAPI(data, token) {
  return async (dispatch) => {
    try {
      const response = await UserServices.postUserAPI(data, token);
      if (response.status === 200 || response.status === 201) {
        toast.success("New User has been added successfully ~");
      }
      dispatch(actUserGetAsyncAPI(token));
    } catch (error) {
      console.error("An error occurred while making the request:", error);
    }
  };
}
export function actUserUpdateAsync(id, data) {
  return async (dispatch) => {
    const response = await UserServices.updateUser(id, data);
    if (response.status === 200 || response.status === 201) {
      toast.success(`User ${id} has been update successfully ~`);
    } else {
      toast.error("Update to fail!!!");
    }
    dispatch(actUserGetAsync());
  };
}
export function actUserUpdateAsyncAPI(id, formData, token) {
  return async (dispatch) => {
    try {
      const response = await UserServices.updateUserAPI(id, formData, token);
      if (response.status === 200 || response.status === 201) {
        toast.success(`User ${id} has been update successfully ~`);
        dispatch(actUserGetAsyncAPI(token));
      } else {
        toast.error("Update to fail!!!");
      }
    } catch (error) {
    }
  };
}
export function actUserDeleteAsyncAPI(id, token) {
  return async (dispatch) => {
    const response = await UserServices.deleteUserAPI(id, token);
    if (response.status === 200 || response.status === 201) {
      toast.success(`User ${id} has been delete successfully ~`);
    } else {
      toast.error("Delete to fail!!!");
    }
    dispatch(actUserGetAsyncAPI(token));
  };
}
export function actUserRecoverAsyncAPI(id, token) {
  return async (dispatch) => {
    const response = await UserServices.recoverUserAPI(id, token);
    if (response.status === 200 || response.status === 201) {
      toast.success(`User ${id} has been recover successfully ~`);
    } else {
      toast.error("Delete to fail!!!");
    }
    dispatch(actUserGetAsyncAPI(token));
  };
}
export function actUserDeleteAsync(id) {
  return async (dispatch) => {
    const response = await UserServices.deleteUser(id);
    if (response.status === 200 || response.status === 201) {
      toast.success(`User ${id} has been delete successfully ~`);
    } else {
      toast.error("Deleted to fail!!!");
    }
    dispatch(actUserGetAsync());
  };
}
export function actUserLogin(currentUser, token, role) {
  return {
    type: ACT_USER_LOGIN,
    payload: {
      currentUser,
      token,
      role,
    },
  };
}
export function actUserNotFetchMe(token) {
  return {
    type: ACT_USER_NOT_FETCH_ME,
    payload: token,
  };
}
