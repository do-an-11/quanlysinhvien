import {
  ADD_SYLLABUS,
  UPDATE_SYLLABUS,
  DELETE_SYLLABUS,
  GET_ALL_SYLLABUS,
  GET_ALL_SYLLABUS_ACTIVE,
} from "./action";

const initialState = {
  syllabus: [],
  activeSyllabus: [],
};
const syllabusReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_SYLLABUS:
      return {
        ...state,
        syllabus: action.payload,
      };
    case GET_ALL_SYLLABUS_ACTIVE:
      return {
        ...state,
        activeSyllabus: action.payload,
      };
    case ADD_SYLLABUS:
      return {
        ...state,
        syllabus: action.payload,
      };
    case UPDATE_SYLLABUS:
      return {
        ...state,
        syllabus: [...state.syllabus, action.payload],
        error: null,
      };
    case DELETE_SYLLABUS:
      return {
        ...state,
        syllabus: [...state.syllabus, action.payload],
        error: null,
      };
    default:
      return state;
  }
};

export default syllabusReducer;
