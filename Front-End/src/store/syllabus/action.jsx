import { toast } from "react-toastify";
import { SyllabusServices } from "../../services/syllabusServices";

export const ADD_SYLLABUS = "ADD_SYLLABUS";
export const UPDATE_SYLLABUS = "UPDATE_SYLLABUS";
export const DELETE_SYLLABUS = "DELETE_SYLLABUS";
export const GET_ALL_SYLLABUS = "GET_ALL_SYLLABUS";
export const GET_ALL_SYLLABUS_ACTIVE = "GET_ALL_SYLLABUS_ACTIVE";
export const addSyllabus = (syllabus) => {
  return {
    type: ADD_SYLLABUS,
    payload: syllabus,
  };
};
export const updateSyllabus = (syllabus) => {
  return {
    type: UPDATE_SYLLABUS,
    payload: syllabus,
  };
};
export const deleteSyllabus = (syllabus) => {
  return {
    type: DELETE_SYLLABUS,
    payload: syllabus,
  };
};
export const getAllSyllabus = (list) => {
  return {
    type: GET_ALL_SYLLABUS,
    payload: list,
  };
};
export const getAllSyllabusActive = (list) => {
  return {
    type: GET_ALL_SYLLABUS_ACTIVE,
    payload: list,
  };
};
export const addAllSyllabus = (list) => {
  return {
    type: "ADD_SYLLABUS",
    payload: list,
  };
};
export function actSyllabusListGetAsync() {
  return async (dispatch) => {
    const response = await SyllabusServices.getAllSyllabus();
    if (response.status === 200 || response.status === 201) {
      dispatch(getAllSyllabus(response.data));
    } else {
      toast.error("get all program to fail");
    }
  };
}
export function actSyllabusListAPIGetAsync(token) {
  return async (dispatch) => {
    const response = await SyllabusServices.getAllSyllabusAPI(token);
    if (response.status === 200 || response.status === 201) {
      dispatch(addAllSyllabus(response.data));
    } else {
      toast.error("get all syllabus to fail");
    }
  };
}
export function actSyllabusListActiveAPIGetAsync(token) {
  return async (dispatch) => {
    try {
      const response = await SyllabusServices.getAllActiveSyllabusAPI(token);

      if (response.status === 200 || response.status === 201) {
        dispatch(getAllSyllabusActive(response.data.payload));
      } else {
        toast.error("Get all syllabus failed");
      }
    } catch (error) {
      console.error("Error fetching active syllabus:", error);
      toast.error("An error occurred while fetching active syllabus");
    }
  };
}

export function actPostSyllabusAsync(syllabus) {
  return async (dispatch) => {
    try {
      const response = await SyllabusServices.addSyllabus(syllabus);
      if (response.status === 200 || response.status === 201) {
        toast.success("New Syllabus has been added successfully!!!");
      }
      dispatch(getAllSyllabus());
    } catch (error) {
      console.error("An error occurred while making the request:", error);
      toast.error("An error occurred while making the request");
    }
  };
}
export function actPostSyllabusAPIAsync(data, token) {
  return async (dispatch) => {
    const response = await SyllabusServices.postSyllabusAPI(data, token);
    if (response.status === 200 || response.status === 201) {
      dispatch(actSyllabusListAPIGetAsync(token));
      toast.success("Created Succesfully!!");
    } else {
      toast.error("Add Failed!!!");
    }
  };
}
export function actSyllabusUpdateAsync(id, data) {
  return async (dispatch) => {
    const response = await SyllabusServices.updateSyllabus(id, data);
    if (response.status === 200 || response.status === 201) {
      toast.success(`Syllabus ${id} has been update successfully ~`);
    } else {
      toast.error("Update to fail!!!");
    }
    dispatch(getAllSyllabus());
  };
}

export function actSyllabusUpdateAPIAsync(data, token) {
  return async (dispatch) => {
    const response = await SyllabusServices.updateSyllabusAPI(data, token);
    if (response.status === 200 || response.status === 201) {
      toast.success(`Syllabus ${data.topicCode} has been updated `);
    } else {
      toast.error("Update Failed!!!");
    }
    dispatch(actSyllabusListAPIGetAsync(token));
  };
}
export function actSyllabusGetOutlineAsync(token, data) {
  return async (dispatch) => {
    const response = await SyllabusServices.CreateOutline(token, data);
    if (response.status === 200 || response.status === 201) {
      toast.success(`Create outline syllabus has been successfully ~`);
    } else {
      toast.error("Create outline syllabus to fail!!!");
    }
    dispatch(actSyllabusListAPIGetAsync());
  };
}
export function actSyllabusDeleteAPIAsync(topicCode, token) {
  return async (dispatch) => {
    const response = await SyllabusServices.deleteSyllabusAPI(topicCode, token);
    if (response.status === 200 || response.status === 201) {
      toast.success(`Syllabus ${topicCode} has been delete successfully `);
    } else {
      toast.error("Deleted to fail!!!");
    }
    dispatch(actSyllabusListAPIGetAsync(token));
  };
}
export function actSyllabusRecoverAPIAsync(topicCode, token) {
  return async (dispatch) => {
    const response = await SyllabusServices.activeSyllabusAPI(topicCode, token);
    if (response.status === 200 || response.status === 201) {
      toast.success(`Syllabus ${topicCode} has been Active successfully ~`);
    } else {
      toast.error("Actived to fail!!!");
    }
    dispatch(actSyllabusListAPIGetAsync(token));
  };
}
export function actSyllabusDuplicateAPIAsync(topicCode, token) {
  return async (dispatch) => {
    const response = await SyllabusServices.duplicateSyllabusAPI(topicCode, token);
    if (response.status === 200 || response.status === 201) {
      toast.success(`Syllabus ${topicCode} has been Duplicate successfully ~`);
    } else {
      toast.error("Duplicate to fail!!!");
    }
    dispatch(actSyllabusListAPIGetAsync(token));
  };
}
