import { toast } from "react-toastify";
import { ClassServices } from "../../services/classServices";

export const addClass = (className) => {
  return {
    type: "ADD_CLASS",
    payload: className,
  };
};
export const addAllSyllabus = (list) => {
  return {
    type: "ADD_SYLLABUS",
    payload: list,
  };
};
export function actSyllabusGetAsync() {
  return async (dispatch) => {
    const response = await ClassServices.getAllSyllabus();
    if (response.status === 200 || response.status === 201) {
      dispatch(addAllSyllabus(response.data));
    } else {
      toast.error("get all syllabus to fail");
    }
  };
}
export function actClassGetAsync() {
  return async (dispatch) => {
    const response = await ClassServices.getListClass();
    if (response.status === 200 || response.status === 201) {
      dispatch(addClass(response.data));
    } else {
      toast.error("get all class to fail");
    }
  };
}
export function actClassAPIGetAsync(token) {
  return async (dispatch) => {
    const response = await ClassServices.getListClassAPI(token);
    let listIteam = [...response.data];
    let data = listIteam.map((e) => {
      return {
        admin: e?.admin?.map((e) => {
          return e?.userEmail;
        }),
        attendeeList: e?.attendeeList?.map((e) => {
          return e?.userEmail;
        }),
        trainer: e?.trainer?.map((e) => {
          return { gmail: e?.userEmail, classCode: e?.syllabusList };
        }),
        created: e?.created?.userEmail,
        approve: e?.approve?.userEmail,
        review: e?.review?.userEmail,
        trainingProgram: e?.trainingProgram,
        attendee: e?.attendee,
        attendeeAccepted: e?.attendeeAccepted,
        attendeeActual: e?.attendeeActual,
        attendeePlanned: e?.attendeePlanned,
        classCode: e?.classCode,
        nameClass: e?.nameClass,
        classTimeFrom: e?.classTimeFrom,
        classTimeTo: e?.classTimeTo,
        startDate: e?.startDate,
        endDate: e?.endDate,
        location: e?.location,
        totalTimeLearning: e?.totalTimeLearning,
        status: e?.status,
        fsu: e?.fsu,
        listDay: e?.listDay,
      };
    });
    if (response.status === 200 || response.status === 201) {
      dispatch(addClass(data));
    } else {
      toast.error("get all class to fail");
    }
  };
}
export function actClassPostAsync(data) {
  return async (dispatch) => {
    const response = await ClassServices.postClass(data);
    if (response.status === 200 || response.status === 201) {
      toast.success("New Class has been added successfully ~");
      dispatch(actClassGetAsync());
    } else {
      toast.error("post class to fail");
    }
  };
}
export function actClassAPIPostAsync(data, token) {
  return async (dispatch) => {
    const response = await ClassServices.postClassAPI(data, token);
    try {
      if (response.status === 200 || response.status === 201) {
        toast.success("New Class has been added successfully ~");
        dispatch(actClassAPIGetAsync(token));
      } else {
        toast.error("post class to fail");
      }
    } catch (error) {
      if (response.status === 418) {
        toast.error("class Code is exist");
      }
    }
  };
}
export function actClassUpdateAsync(id, data) {
  return async (dispatch) => {
    const response = await ClassServices.updateClass(id, data);
    if (response.status === 200 || response.status === 201) {
      toast.success(`Class ${id} has been update successfully ~`);
    } else {
      toast.error("Update to fail!!!");
    }
    dispatch(actClassGetAsync());
  };
}
export function actClassUpdateAPIAsync(id, data, token) {
  return async (dispatch) => {
    const response = await ClassServices.updateClassAPI(id, data, token);
    if (response.status === 200 || response.status === 201) {
      toast.success(`Class ${data?.classCode} has been update successfully ~`);
    } else {
      toast.error("Update to fail!!!");
    }
    dispatch(actClassAPIGetAsync(token));
  };
}
export function actClassDeleteAsync(id) {
  return async (dispatch) => {
    const response = await ClassServices.deleteClass(id);
    if (response.status === 200 || response.status === 201) {
      toast.success(`Class ${id} has been delete successfully ~`);
    } else {
      toast.error("Deleted to fail!!!");
    }
    dispatch(actClassGetAsync());
  };
}
export function actClassDeleteAsyncAPI(id, token) {
  return async (dispatch) => {
    const response = await ClassServices.deleteClassAPI(id, token);
    if (response.status === 200 || response.status === 201) {
      toast.success(`Class ${id} has been delete successfully ~`);
    } else {
      toast.error("Deleted to fail!!!");
    }
    dispatch(actClassAPIGetAsync(token));
  };
}
