const initialState = {
  classes: [],
  syllabus: [],
};
const classReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_CLASS":
      return {
        ...state,
        classes: action.payload,
      };
    case "ADD_SYLLABUS":
      return { ...state, syllabus: action.payload };
    default:
      return state;
  }
};

export default classReducer;
