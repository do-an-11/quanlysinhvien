const initialState = {
  programs: [],
  programsAll: [],
  programsNew: [],
  syllabus: [],
};
const programReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_PROGRAM":
      return {
        ...state,
        programs: action.payload,
      };
      case "ADD_PROGRAM_ALL":
        return {
          ...state,
          programsAll: action.payload,
        };
    case "ADD_PROGRAM_NEW":
      return {
        ...state,
        programsNew: action.payload,
      };
    case "ADD_SYLLABUS":
      return { ...state, syllabus: action.payload };
    default:
      return state;
  }
};

export default programReducer;
