import { ProgramServices } from "../../services/programServices";
import { toast } from "react-toastify";

export const addProgram = (programName) => {
  return {
    type: "ADD_PROGRAM",
    payload: programName,
  };
};
export const addProgramAll = (programName) => {
  return {
    type: "ADD_PROGRAM_ALL",
    payload: programName,
  };
};
export const addProgramNew = (programName) => {
  return {
    type: "ADD_PROGRAM_NEW",
    payload: programName,
  };
};
export const addAllSyllabus = (list) => {
  return {
    type: "ADD_SYLLABUS",
    payload: list,
  };
};

export function actProgramAPIGetAsync(token) {
  return async (dispatch) => {
    try {
      const response = await ProgramServices.getListProgramAPI(token);
      if (response.status === 200 || response.status === 201) {
        dispatch(addProgram(response.data.payload));
      } else {
        toast.error("Failed to get all program");
      }
    } catch (error) {
      console.error("Error fetching programs API:", error);
      toast.error("An error occurred while fetching programs API");
    }
  };
}

export function actProgramAPIAllGetAsync(token) {
  return async (dispatch) => {
    try {
      const response = await ProgramServices.getListProgramAPIAll(token);
      if (response.status === 200 || response.status === 201) {
        dispatch(addProgramAll(response.data.payload));
      } else {
        toast.error("Failed to get all program");
      }
    } catch (error) {
      console.error("Error fetching programs API all:", error);
      toast.error("An error occurred while fetching programs API all");
    }
  };
}

function formatDob(dob) {
  const date = new Date(dob);
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, "0");
  const day = String(date.getDate()).padStart(2, "0");
  return `${year}-${month}-${day}`;
}

export function actProgramAPINewGetAsync(token) {
  return async (dispatch) => {
    try {
      const response = await ProgramServices.getListProgramAPI(token);
      let data = response?.data?.payload;
      if (response.status === 200 || response.status === 201) {
        let format = data?.map((e) => {
          return {
            title: e?.name,
            modifiedBy: e?.modifiedBy,
            modifiedDay: formatDob(e?.modifiedDate),
            id: e?.trainingProgramCode,
            list: e?.trainingProgramSyllabus?.map((e) => {
              return { nameCode: e?.id?.topicCode, title: e?.id?.topicCode };
            }),
          };
        });
        dispatch(addProgramNew(format));
      } else {
        toast.error("Failed to get all program");
      }
    } catch (error) {
      console.error("Error fetching new programs API:", error);
      toast.error("An error occurred while fetching new programs API");
    }
  };
}

export function actProgramPostAPIAsync(data, token) {
  return async (dispatch) => {
    try {
      const response = await ProgramServices.getPostProgramAPI(data, token);
      if (response.status === 200 || response.status === 201) {
        toast.success("New Program has been added successfully ~");
        dispatch(actProgramAPIGetAsync(token));
      } else {
        toast.error("Failed to post program");
      }
    } catch (error) {
      console.error("Error posting program API:", error);
      toast.error("An error occurred while posting program API");
    }
  };
}

export function actProgramUpdateAPIAsync(id, data, token) {
  return async (dispatch) => {
 
      const response = await ProgramServices.updateProgramAPI(id, data, token);
      if (response.status === 200 || response.status === 201) {
        toast.success(`Program ${id} has been updated successfully ~`);
      } else {
        toast.error("Failed to update program");
      }
      dispatch(actProgramAPIGetAsync(token));
 
  };
}

export function actProgramDeleteAPIAsync(id, token) {
  return async (dispatch) => {
    try {
      const response = await ProgramServices.inActiveProgramAPI(id, token);
      if (response.status === 200 || response.status === 201) {
        toast.success(`Program ${id} has been inactivated successfully ~`);
      } else {
        toast.error("Failed to inactivate program");
      }
      dispatch(actProgramAPIAllGetAsync(token));
    } catch (error) {
      console.error(`Error inactivating program ${id}:`, error);
      toast.error(`An error occurred while inactivating program ${id}`);
    }
  };
}

export function actProgramRecoverAPIAsync(id, token) {
  return async (dispatch) => {
    try {
      const response = await ProgramServices.activeProgramAPI(id, token);
      if (response.status === 200 || response.status === 201) {
        toast.success(`Program ${id} has been activated successfully ~`);
      } else {
        toast.error("Failed to activate program");
      }
      dispatch(actProgramAPIAllGetAsync(token));
    } catch (error) {
      console.error(`Error activating program ${id}:`, error);
      toast.error(`An error occurred while activating program ${id}`);
    }
  };
}
