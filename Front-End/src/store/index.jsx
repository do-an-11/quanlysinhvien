import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import classReducer from "./class/reducer";
import userReducer from "./user/reducer";
import permissionReducer from "./permission/reducer";
import programReducer from "./program/reducer";
import syllabusReducer from "./syllabus/reducer";
const rootReducer = combineReducers({
  class: classReducer,
  USER: userReducer,
  PERMISSIONS: permissionReducer,
  program: programReducer,
  SYLLABUS: syllabusReducer,
});
const store = createStore(rootReducer, applyMiddleware(thunk));
export default store;
