import { ACT_GET_PERMISSIONS } from "./action";

const initialState = {
  permissionsList: [],
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ACT_GET_PERMISSIONS:
      return { ...state, permissionsList: action.payload };

    default:
      return state;
  }
};

export default reducer;
