import { toast } from "react-toastify";
import { PermissionServices } from "../../services/permissionServices";

export const ACT_GET_PERMISSIONS = "ACT_GET_PERMISSIONS";
export function actGetPermissions(list) {
  return {
    type: ACT_GET_PERMISSIONS,
    payload: list,
  };
}
export function actGetPermissionsAsync(token) {
  return async (dispatch) => {
    try {
      const response = await PermissionServices.getListPermission(token);
      if (response.status === 200 || response.status === 201) {
        dispatch(actGetPermissions(response.data));
      } else {
        toast.error("get all user to fail");
      }
    } catch (error) {
      console.error("An error occurred while making the request:", error);
    }
  };
}
export function actUpdatePermissionsAsync(token, data) {
  return async (dispatch) => {
    const response = await PermissionServices.updatePermission(token, data);
    if (response.status === 200 || response.status === 201) {
      dispatch(actGetPermissionsAsync(token));
    } else {
      toast.error("get update permission to fail");
    }
  };
}
