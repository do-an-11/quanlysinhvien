import React, { useEffect, useState } from "react";

import { Button } from "react-bootstrap";
import { BiPlusCircle } from "react-icons/bi";
import { useNavigate, useParams } from "react-router-dom";
import UserFormModal from "../components/UserFormModal";
import UserFormSearch from "../components/UserFormSearch";
import { useTranslation } from "react-i18next";

function UserListPage(props) {
  const [showModal, setShowModal] = useState(false);
  const param = useParams();
  const id = param.id;

  const { t } = useTranslation();

  const handleOpenModal = () => {
    setShowModal(true);
  };

  const navigate = useNavigate();
  useEffect(() => {
    if (id) {
      setShowModal(true);
    }
  }, [id]);
  const handleCloseModal = () => {
    if (id) {
      navigate("/usermanage");
    }
    setShowModal(false);
  };

  const handleSubmit = (formData) => {
    handleCloseModal();
  };

  return (
    <div>
      <h1>{t("user_management")}</h1>
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Button variant="primary" onClick={handleOpenModal}>
          <BiPlusCircle /> {t("add_user")}
        </Button>
      </div>
      <UserFormModal
        show={showModal}
        handleClose={handleCloseModal}
        handleSubmit={handleSubmit}
      />
      <UserFormSearch />
    </div>
  );
}

export default UserListPage;
