import React, { useEffect, useState } from "react";

import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import CreateProgram from "../components/CreateTrainingProgram/CreateProgram";
import { CreateTrainingProgram } from "../components/CreateTrainingProgram/CreateTrainingProgram";
import {
  actProgramPostAPIAsync,
  actProgramUpdateAPIAsync,
} from "../store/program/action";
import {
  actSyllabusListAPIGetAsync,
  actSyllabusListActiveAPIGetAsync,
} from "../store/syllabus/action";

const LOCAL_STORAGE_TOKEN_KEY = "ACCESS_TOKEN";

export const CreateProgramPage = () => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [open, setOpen] = useState(false);

  const navigate = useNavigate();

  const param = useParams();
  let id = +param.id;

  const [form, setForm] = useState({
    trainingProgramName: "",
    duration: "",
    trainerGmail: "",
    status: "",
    topicCode: [],
  });

  const listProgram = useSelector(state => state.program.programsAll);
  const listSyllabus = useSelector(state => state.SYLLABUS.syllabus);

  useEffect(() => {
    dispatch(actSyllabusListAPIGetAsync(token));
  }, []);

  useEffect(() => {
    if (id && listProgram) {
      let item = listProgram?.find(e => e.trainingProgramCode === id);
      let data = {
        trainingProgramName: item?.name,
        topicCode: item?.trainingProgramSyllabus?.map((e) => e?.id?.topicCode),
        trainerGmail: item?.userID?.email,
        status: item?.status,
        duration: item?.duration,
      };
      setForm(data);
      setOpen(true);
    }
  }, [id, listProgram]);

  const token = localStorage.getItem(LOCAL_STORAGE_TOKEN_KEY);

  useEffect(() => {
    dispatch(actSyllabusListActiveAPIGetAsync(token));
  }, []);

  const handleSaveButton = () => {
    setIsLoading(true);

    let data = listSyllabus?.filter((e) =>
      form?.topicCode?.includes(e.syllabusCode)
    );

    const update = {
      ...form,
      duration: calculateDuration(data),
    };

    const apiAction = id
      ? actProgramUpdateAPIAsync(id, update, token)
      : actProgramPostAPIAsync(update, token);

    dispatch(apiAction).then(() => {
      setIsLoading(false);
      navigate("/program");
    });
  };

  const calculateDuration = (data) => {
    return data
      ?.map((e) => parseInt(e.duration, 10))
      .reduce((acc, duration) => acc + duration, 0);
  };

  const handleChangeAll = (e) => {
    setForm(e);
  };

  return (
    <div>
      <CreateProgram
        id={id}
        setOpen={setOpen}
        listItem={handleChangeAll}
        item={form}
      />
      {open && (
        <CreateTrainingProgram
          id={id}
          item={form}
          handleSaveButton={handleSaveButton}
          listItem={handleChangeAll}
        />
      )}
      {isLoading && (
        <FontAwesomeIcon
          style={{
            fontSize: 40,
            position: "fixed",
            right: "200px",
            color: "blue",
            marginTop: "10px",
          }}
          icon={faSpinner}
          spin
        />
      )}
    </div>
  );
};
