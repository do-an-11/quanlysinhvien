import React from "react";
import { ProgramFormModal } from "../components/ProgramFormModal";
import { Button } from "react-bootstrap";
import { BiPlusCircle } from "react-icons/bi";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

function TrainingProgramPage(props) {
  const { t } = useTranslation();

  return (
    <div>
      <h1>{t("training_program")}</h1>
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <div style={{ width: "1rem" }}></div>
        <Button variant="primary">
          <BiPlusCircle />{" "}
          <Link
            to="/createprogram"
            style={{ color: "white", textDecoration: "none" }}
          >
            {t("create_program")}
          </Link>
        </Button>
      </div>
      <ProgramFormModal />
    </div>
  );
}

export default TrainingProgramPage;
