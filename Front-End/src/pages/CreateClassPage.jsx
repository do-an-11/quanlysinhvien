import React, { useState } from "react";

import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import {
  actClassAPIGetAsync,
  actClassAPIPostAsync,
  actClassUpdateAPIAsync,
} from "../store/class/action";

import CreateClass from "../components/ClassFormModal/CreateClass";
import { CreatedClass } from "../components/ClassFormModal/CreatedClass";
import {
  actUserGetAdminAndSuperAdminAsyncAPI,
  actUserGetAdminAsyncAPI,
  actUserGetTrainerAsyncAPI,
} from "../store/user/action";
import { t } from "i18next";
import { actProgramAPINewGetAsync } from "../store/program/action";

export const CreateClassPage = () => {
  const [isOpen, setIsOpen] = useState(false);
  const dispatch = useDispatch();
  const param = useParams();
  let { id } = param;
  const [clearForm, setClearForm] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const [form, setForm] = useState({
    nameClass: "",
    status: "",
    classCode: "",
    totalTimeLearning: "",
    classTimeFrom: "",
    classTimeTo: "",
    location: "",
    trainer: "",
    admin: "",
    fsu: "",
    created: "",
    review: "",
    approve: "",
    attendee: "",
    attendeePlanned: "",
    attendeeAccepted: "",
    attendeeActual: "",
    startDate: "",
    endDate: "",
    listDay: [],
    trainingProgram: "",
    attendeeList: [],
  });

  let token = localStorage.getItem("ACCESS_TOKEN");

  useEffect(() => {
    dispatch(actClassAPIGetAsync(token));
  }, []);
  useEffect(() => {
    dispatch(actProgramAPINewGetAsync(token));
  }, []);

  const listProgram = useSelector((state) => state.program.programsNew);
  const listClass = useSelector((state) => state.class.classes);

  useEffect(() => {
    if (id && listClass) {
      let item = listClass?.find(
        (e) => e?.classCode + "_" + (e?.location).trim().toLowerCase() === id
      );
      setForm(item);
      setIsOpen(true);
    }
  }, [id, listClass]);
  function handleChange(e) {
    setIsOpen(e);
  }
  function handleChangeValue(item) {
    setForm({
      ...form,
      nameClass: item,
    });
  }
  function handleChangeAll(e) {
    setForm(e);
  }
  const user = useSelector((state) => state.USER.currentUser);

  const navigate = useNavigate();
  function handleSubmit() {
    if (id) {
      let training = listProgram?.filter(
        (e) => e?.title === form?.trainingProgram
      );
      if (training && training.length > 0) {
        const selectedTraining = training[0]; 
        const update = {
          ...form,
          classCode:
            form?.classCode + "_" + (form?.location).trim().toLowerCase(),
          trainingProgram: selectedTraining.id,
          moderEmail: user?.email,
        };
        dispatch(actClassUpdateAPIAsync(id, update, token)).then(() => {
          setIsLoading(false);
          navigate("/class");
        });
        setIsOpen(!isOpen);
        resetForm();
        setClearForm(true);
      } else {
      }
    } else {
      if (
        !form.nameClass ||
        !form.status ||
        !form.classCode ||
        !form.totalTimeLearning ||
        !form.classTimeFrom ||
        !form.classTimeTo ||
        !form.location ||
        !form.trainer ||
        !form.admin ||
        !form.fsu ||
        !form.created ||
        !form.review ||
        !form.approve ||
        !form.attendee ||
        !form.attendeePlanned ||
        !form.attendeeAccepted ||
        !form.attendeeActual ||
        !form.startDate ||
        !form.endDate ||
        form?.listDay?.length < 0 ||
        !form.trainingProgram ||
        form?.attendeeList?.length < 0
      ) {
        toast.error("Please complete all information");
      } else {
        setIsLoading(true);
        let training = listProgram?.filter(
          (e) => e?.title === form?.trainingProgram
        );
        if (training && training.length > 0) {
          const selectedTraining = training[0]; 
          const update = {
            ...form,
            classCode:
              form?.classCode + "_" + (form?.location).trim().toLowerCase(),
            trainingProgram: selectedTraining.id,
            moderEmail: user?.email,
          };
          dispatch(actClassAPIPostAsync(update, token)).then(() => {
            setIsLoading(false);
            navigate("/class");
          });
          setIsOpen(!isOpen);
          resetForm();
          setClearForm(true);
        } else {
        }
      }
    }
    function resetForm() {
      setForm({
        nameClass: "",
        status: "",
        classCode: "",
        totalTimeLearning: "",
        classTimeFrom: "",
        classTimeTo: "",
        location: "",
        trainer: "",
        admin: "",
        fsu: "",
        created: "",
        review: "",
        approve: "",
        attendee: "",
        attendeePlanned: "",
        attendeeAccepted: "",
        attendeeActual: "",
        startDate: "",
        endDate: "",
        listDay: [],
        trainingProgram: "",
        attendeeList: [],
      });
    }
  }

  useEffect(() => {
    dispatch(actUserGetAdminAsyncAPI(token));
    dispatch(actUserGetTrainerAsyncAPI(token));
    dispatch(actUserGetAdminAndSuperAdminAsyncAPI(token));
  }, []);

  return (
    <div>
      <CreateClass id={id} open={handleChange} value={handleChangeValue} />
      <CreatedClass
        id={id}
        listItem={handleChangeAll}
        item={form}
        isOpen={isOpen}
        clearForm={clearForm}
      />
      {isLoading ? (
        <>
          <FontAwesomeIcon
            style={{
              fontSize: 40,
              position: "fixed",
              right: "200px",
              color: "blue",
              marginTop: "10px",
            }}
            icon={faSpinner}
            spin
          />
        </>
      ) : (
        ""
      )}

      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <button
          style={{
            fontSize: "20px",
            fontWeight: "600",
            borderRadius: "10px",
            backgroundColor: "#002761",
            color: "white",
            padding: "2px 10px",
            marginBottom: "60px",
          }}
          onClick={handleSubmit}
        >
          {t("save")}
        </button>
      </div>
    </div>
  );
};
