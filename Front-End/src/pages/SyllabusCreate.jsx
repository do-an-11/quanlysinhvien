import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import SyllabusFormModal from "../components/SyllabusFormModal/index";
import {
  actPostSyllabusAPIAsync,
  actSyllabusUpdateAPIAsync,
} from "../store/syllabus/action";
import { actSyllabusListAPIGetAsync } from "../store/syllabus/action";

export const SyllabusCreate = () => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [clearForm, setClearForm] = useState(false);
  const [open, setOpen] = useState(false);
  const param = useParams();
  let id = param.id;

  const [form, setForm] = useState({
    topicName: "",
    topicCode: "",
    version: "",
    techrequire: "",
    trainingAudience: "",
    trainingPrinciple: "",
    courseobj: "",
    level: "",
  });

  const listSyllabus = useSelector((state) => state.SYLLABUS.syllabus);
  useEffect(() => {
    if (id && listSyllabus) {
      let item = listSyllabus?.find((e) => e.syllabusCode === id);
      let data = {
        topicName: item?.syllabusName,
        topicCode: item?.syllabusCode,
        version: item?.version,
        priority: item?.priority,
        courseObjective: item?.courseobj,
        technicalRequirement: item?.techrequire,
        trainingAudience: item?.trainingAudience,
        trainingPrinciple: item?.trainingPrinciple,
        status: item?.status,
      };
      setForm(data);
      setOpen(true);
    }
  }, [id, listSyllabus]);
  const token = localStorage.getItem("ACCESS_TOKEN");

  useEffect(() => {
    dispatch(actSyllabusListAPIGetAsync(token));
  }, []);
  const handleChangeValue = (item) => {
    setForm({
      ...form,
      form: item,
    });
  };

  const navigate = useNavigate();

  const handleSaveButton = () => {
    if (id) {
      setIsLoading(true);
      let data = listSyllabus?.filter((e) =>
        form?.topicCode?.includes(e.syllabusCode)
      );
      const update = {
        ...form,
        duration: data
          ?.map((e) => {
            return e.duration;
          })
          .reduce((acc, duration) => acc + duration, 0),
      };
      dispatch(actSyllabusUpdateAPIAsync(id, update, token)).then(() => {
        setIsLoading(false);
        navigate("/syllabus");
      });
      setClearForm(true);
    } else {
      if (!form.topicName || form.topicCode.length < 0) {
        toast.error("Please complete all information");
      } else {
        setIsLoading(true);
        let data = listSyllabus?.filter((e) =>
          form?.topicCode?.includes(e.syllabusCode)
        );
        const update = {
          ...form,
          status: "Active",
          duration: data
            ?.map((e) => {
              return e.duration;
            })
            .reduce((acc, duration) => acc + duration, 0),
        };
        dispatch(actPostSyllabusAPIAsync(update, token)).then(() => {
          setIsLoading(false);
          navigate("/syllabus");
        });
        setClearForm(true);
      }
    }
  };
  const handleChangeAll = (e) => {
    setForm(e);
  };
  return (
    <div>
      <SyllabusFormModal
        id={id}
        value={handleChangeValue}
        setOpen={setOpen}
        listItem={handleChangeAll}
        item={form}
      />
    </div>
  );
};
