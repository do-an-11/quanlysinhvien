import React, { useState } from "react";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";

import { CheckBox, CheckBoxOutlineBlank } from "@mui/icons-material";

import "../css/password.css";

library.add(fab);

function ChangePasswordPage(props) {
  const [password, setPassword] = useState("");
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [passwordStrength, setPasswordStrength] = useState(0);
  const [showPassword, setShowPassword] = useState(false);
  const [error, setError] = useState("");
  const [passwordConditions, setPasswordConditions] = useState({
    lowercase: false,
    uppercase: false,
    number: false,
    specialChar: false,
    minLength: false,
  });
  const handleConfirmPassword = () => {
    if (
      passwordConditions.lowercase &&
      passwordConditions.uppercase &&
      passwordConditions.number &&
      passwordConditions.specialChar &&
      passwordConditions.minLength
    ) {
      setError("");
      if (newPassword === password) {
      } else {
        setError("Passwords do not match.");
      }
    } else {
      setError("Password does not meet the requirements.");
    }
  };

  const calculatePasswordStrength = (inputPassword) => {
    let strength = 0;

    const specialSymbols = /[$&+,:;=?@#|'<>.^*()%!-]/;
    if (specialSymbols.test(inputPassword)) {
      strength += 10;
      setPasswordConditions((prevConditions) => ({
        ...prevConditions,
        specialChar: true,
      }));
    } else {
      setPasswordConditions((prevConditions) => ({
        ...prevConditions,
        specialChar: false,
      }));
    }

    const uppercaseLetters = /[A-Z]/;
    if (uppercaseLetters.test(inputPassword)) {
      strength += 5;
      setPasswordConditions((prevConditions) => ({
        ...prevConditions,
        uppercase: true,
      }));
    } else {
      setPasswordConditions((prevConditions) => ({
        ...prevConditions,
        uppercase: false,
      }));
    }

    const numbers = /\d/;
    if (numbers.test(inputPassword)) {
      strength += 5;
      setPasswordConditions((prevConditions) => ({
        ...prevConditions,
        number: true,
      }));
    } else {
      setPasswordConditions((prevConditions) => ({
        ...prevConditions,
        number: false,
      }));
    }

    const lengthStrength = (inputPassword.length / 35) * 100;
    strength += Math.min(lengthStrength, 100);

    if (inputPassword.length >= 8) {
      setPasswordConditions((prevConditions) => ({
        ...prevConditions,
        minLength: true,
      }));
    } else {
      setPasswordConditions((prevConditions) => ({
        ...prevConditions,
        minLength: false,
      }));
    }

    return Math.min(strength, 100);
  };

  const clearError = () => {
    setError("");
  };

  const handlePasswordChange = (e) => {
    const newPassword = e.target.value;
    setPassword(newPassword);
    const strength = calculatePasswordStrength(newPassword);
    setPasswordStrength(strength);

    const progressBar = document.getElementById("password-strength");
    if (progressBar) {
      progressBar.style.width = strength + "%";
    }
  };

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  return (
    <div>
      <div className="changepassword">
        <div
          className="custom-height"
          style={{ display: "flex", height: "90%", paddingTop: "2rem" }}
        >
          <div className="col-md-2"></div>
          <div className="col-md-8" style={{ display: "flex" }}>
            <div
              className="col-md-4 text-center text-red flex "
              style={{ backgroundColor: "#aaa", borderRadius: "5px" }}
            >
              <img
                src="https://inkythuatso.com/uploads/thumbnails/800/2023/03/9-anh-dai-dien-trang-inkythuatso-03-15-27-03.jpg"
                alt="Avatar"
                className="btn text-white btn-floating m-5 rounded-circle"
                style={{ width: "110px" }}
              />
              <p id="job">~~~~~~~~~</p>
              <i className="far fa-edit mb-5"></i>
              <h3>PRIVACY</h3>
            </div>
            <div
              className="col-md-8"
              style={{ backgroundColor: "#49a9a9", borderRadius: "5px" }}
            >
              <div className="form-group">
                <label className="label-password" htmlFor="oldPassword">
                  Old Password:
                </label>
                <input
                  className="input_password"
                  name="password"
                  type="password"
                  id="oldPassword"
                  placeholder="Enter your old password"
                  value={oldPassword}
                  onChange={(e) => setOldPassword(e.target.value)}
                  textAlign="left"
                />
              </div>
              <div className="form-group">
                <label className="label-password" htmlFor="user_password">
                  New Password:
                </label>
                <div className="gl-field-error-anchor input-icon-wrapper">
                  <input
                    className="input_password"
                    id="password"
                    name="password"
                    type={showPassword ? "text" : "password"}
                    placeholder="Enter your password"
                    onChange={handlePasswordChange}
                    onClick={clearError}
                  ></input>

                  <span
                    className="show-pass"
                    onClick={togglePasswordVisibility}
                    style={{ color: "black" }}
                  >
                    {showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                  </span>
                  <div id="popover-password">
                    <p>
                      <span id="result"></span>
                    </p>
                    <div className="progress">
                      <div
                        id="password-strength"
                        className="progress-bar"
                        role="progressbar"
                        aria-valuenow="40"
                        aria-valuemin="0"
                        aria-valuemax="100"
                        style={{ width: `${passwordStrength}%` }}
                      ></div>
                    </div>
                    <ul className="list-unstyled">
                      <li className="">
                        <span
                          className={`low-upper-case ${passwordConditions.uppercase ? "checked" : ""
                            }`}
                          style={{
                            color: "black",
                          }}
                        >
                          {passwordConditions.uppercase ? (
                            <CheckBox />
                          ) : (
                            <CheckBoxOutlineBlank />
                          )}
                          &nbsp;Uppercase
                        </span>
                      </li>
                      <li className="">
                        <span
                          className={`one-number ${passwordConditions.number ? "checked" : ""
                            }`}
                          style={{
                            color: "black",
                          }}
                        >
                          {passwordConditions.number ? (
                            <CheckBox />
                          ) : (
                            <CheckBoxOutlineBlank />
                          )}
                          &nbsp;Number (0-9)
                        </span>
                      </li>
                      <li className="">
                        <span
                          className={`one-special-char ${passwordConditions.specialChar ? "checked" : ""
                            }`}
                          style={{
                            color: "black",
                          }}
                        >
                          {passwordConditions.specialChar ? (
                            <CheckBox />
                          ) : (
                            <CheckBoxOutlineBlank />
                          )}
                          &nbsp;Special Character (!@#$%^&*)
                        </span>
                      </li>
                      <li className="">
                        <span
                          className={`eight-character ${passwordConditions.minLength ? "checked" : ""
                            }`}
                          style={{
                            color: "black",
                          }}
                        >
                          {passwordConditions.minLength ? (
                            <CheckBox />
                          ) : (
                            <CheckBoxOutlineBlank />
                          )}
                          &nbsp;At least 8 Characters
                        </span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <label className="label-password" htmlFor="confirmPassword">
                  Confirm Password:
                </label>
                <div className="gl-field-error-anchor input-icon-wrapper">
                  <input
                    className="input_password"
                    id="password"
                    name="password"
                    type="password"
                    placeholder="Enter your password"
                  />
                </div>
              </div>
              <div
                className="d-flex "
                style={{
                  width: "100%",
                  justifyContent: "flex-end",
                  paddingRight: "1rem",
                  paddingBottom: " 1rem",
                }}
              >
                <button
                  className="button-password"
                  type="button"
                  onClick={handleConfirmPassword}
                  style={{
                    right: "0",
                    bot: "0",
                    marginLeft: "7px",
                    backgroundColor: "grey",
                  }}
                >
                  Change Password
                </button>
              </div>
              {error && (
                <div
                  className="error-message"
                  onClick={clearError}
                  style={{ color: "black", padding: "7px", fontWeight: "bold" }}
                >
                  {error}
                </div>
              )}
              <span> </span>
            </div>
          </div>
          <div className="col-md-2"></div>
        </div>
      </div>
    </div>
  );
}

export default ChangePasswordPage;
