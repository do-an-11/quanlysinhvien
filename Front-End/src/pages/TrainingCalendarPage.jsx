import React from "react";
import { TrainingCalendar } from "../components/TrainingCalendar/TrainingCalendar";

function TrainingCalendarPage(props) {
  return (
    <div>
      <TrainingCalendar />
    </div>
  );
}

export default TrainingCalendarPage;
