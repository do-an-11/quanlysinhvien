import React from "react";

import { Button } from "react-bootstrap";
import { BiPlusCircle } from "react-icons/bi";
import { Link } from "react-router-dom";
import { ClassFormModal } from "../components/ClassFormModal";
import { useTranslation } from "react-i18next";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { actClassAPIGetAsync } from "../store/class/action";
import { actUserGetTrainerAsyncAPI } from "../store/user/action";

function ClassPage(props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actClassAPIGetAsync(localStorage.getItem("ACCESS_TOKEN")));
    dispatch(actUserGetTrainerAsyncAPI(localStorage.getItem("ACCESS_TOKEN")));
  }, []);
  return (
    <div>
      <h1>{t("training_class")}</h1>
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Button variant="primary">
          <BiPlusCircle />{" "}
          <Link
            to="/createclass"
            style={{ color: "white", textDecoration: "none" }}
          >
            {t("create_class")}
          </Link>
        </Button>
      </div>
      <ClassFormModal />
    </div>
  );
}

export default ClassPage;
