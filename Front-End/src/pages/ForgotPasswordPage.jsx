import React, { useState } from "react";
import styles from "../css/forgot.module.css";
import { Link } from "react-router-dom";

function ForgotPassword() {
  const [email, setEmail] = useState("");
  const [verificationCode, setVerificationCode] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [isEmailEntered, setIsEmailEntered] = useState(false);
  const [isCodeVerified, setIsCodeVerified] = useState(false);
  const [isPasswordReset, setIsPasswordReset] = useState(false);

  const handleEnterEmail = () => {
    if (email) {
      setIsEmailEntered(true);
    }
  };

  const handleSendVerificationCode = () => {
    setIsCodeVerified(false);
  };

  const handleVerifyCode = () => {
    if (verificationCode === "YOUR_VERIFICATION_CODE_FROM_EMAIL") {
      setIsCodeVerified(true);
    } else {
      alert("Mã xác thực không đúng. Vui lòng kiểm tra lại.");
    }
  };

  const handleResetPassword = () => {
    setIsPasswordReset(true);
  };

  return (
    <div className={styles.container}>
      <div className={styles.box}>
        {isPasswordReset ? (
          <div>
            <p>Mật khẩu của bạn đã được đặt lại thành công.</p>
          </div>
        ) : (
          <div>
            {isCodeVerified ? (
              <div>
                <h3>Change Password</h3>
                <label htmlFor="loginInput" style={{ margin: "20px 0 10px 0" }}>
                  New password
                </label>
                <input
                  type="password"
                  placeholder="New Password"
                  value={newPassword}
                  onChange={(e) => setNewPassword(e.target.value)}
                />
                <label htmlFor="loginInput" style={{ margin: "30px 0 10px 0" }}>
                  Confirm password
                </label>
                <input
                  type="password"
                  placeholder="Confirm Password"
                />
                <div className={styles.buttonContainer}>
                  <button onClick={handleResetPassword}>Save Changes</button>
                </div>
                <div className={styles.linkContainer}>
                  <Link to={"/login"}>Cancel</Link>
                </div>
              </div>
            ) : isEmailEntered ? (
              <div className={styles.inputContainer}>
                <h3>Password Recovery</h3>
                <p
                  style={{
                    textAlign: "center",
                    marginBottom: "30px",
                    fontSize: "20px",
                  }}
                >
                  An email with a verification code was just sent to your email
                </p>
                <form className={styles.formContainer}>
                  <input
                    type="text"
                    placeholder="Enter the code"
                    value={verificationCode}
                    onChange={(e) => setVerificationCode(e.target.value)}
                  />
                  <button
                    style={{
                      background: "none",
                      color: "#007bff",
                      border: "none",
                      fontWeight: "bold",
                      justifyContent: "left",
                    }}
                    onClick={handleSendVerificationCode}
                  >
                    Resend the code
                  </button>
                  <div
                    className={styles.buttonContainer}
                    style={{ justifyContent: "space-around" }}
                  >
                    <button onClick={handleVerifyCode}>Confirm</button>
                  </div>
                </form>
              </div>
            ) : (
              <div className={styles.inputContainer}>
                <h3>Password Recovery</h3>
                <p
                  style={{
                    textAlign: "center",
                    marginBottom: "30px",
                    fontSize: "20px",
                  }}
                >
                  Enter your email to reset your password
                </p>
                <form className={styles.formContainer}>
                  <div>
                    <label className={email && styles.filled}>Email</label>
                    <input
                      type="email"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                    />
                  </div>
                  <div
                    className={styles.buttonContainer}
                    style={{ textAlign: "center", marginTop: "10px" }}
                  >
                    <button onClick={handleEnterEmail}>Next</button>
                  </div>
                  <div className={styles.linkContainer}>
                    Have an account?<span style={{ marginRight: "5px" }}></span>
                    <Link to={"/login"}> Login</Link>
                  </div>
                </form>
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  );
}

export default ForgotPassword;
