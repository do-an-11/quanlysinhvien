import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import "../css/profile.css";
import { UserServices } from "../services/userServices";
import { useEffect } from "react";
import { actUserGetAsyncAPI, actUserLogin } from "../store/user/action";
import { useTranslation } from "react-i18next";
import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";
import { Grid } from "@mui/material";
import Avatar from "@mui/material/Avatar";
import Badge from "@mui/material/Badge";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import PhotoCameraIcon from "@mui/icons-material/PhotoCamera";

import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";

function ProfilePage(props) {
  const user = useSelector((state) => state.USER.currentUser);
  const token = localStorage.getItem("ACCESS_TOKEN");
  const navigate = useNavigate();
  const dispatch = useDispatch();
  useEffect(() => {
    UserServices.fetchMe(token)
      .then((res) => {
        if (res.data && res.data.payload) {
          const currentUser = res.data.payload;
          const role = res.data.message;
          dispatch(actUserLogin(currentUser, token, role));
          dispatch(actUserGetAsyncAPI(token));
        } else {
          alert("Please login");
        }
      })
      .catch((err) => {
        if (err.response) {
        } else {
          alert("An error occurred. Please login.");
        }
        navigate("/login");
      });
  }, []);

  const handleClick = () => {
    navigate("/");
  };

  const { t } = useTranslation();

  const styles = {
    details: {
      padding: "1rem",
      borderTop: "1px solid #e1e1e1",
    },
    value: {
      padding: "1rem 2rem",
      borderTop: "1px solid #e1e1e1",
      color: "#899499",
    },
  };

  return (
    <div className="belac">
      <CssBaseline>
        <Grid container direction="column" sx={{ overflowX: "hidden", overflowY: "hidden" }}>
          <Grid item xs={12} md={6}>
            <img
              alt="avatar"
              style={{
                width: "100vw",
                height: "35vh",
                objectFit: "cover",
                objectPosition: "50% 50%",
                position: "relative",
              }}
              src="https://iris2.gettimely.com/images/default-cover-image.jpg"
            />
          </Grid>
          <Grid
            container
            direction={{ xs: "column", md: "row" }}
            spacing={3}
            sx={{
              position: "absolute",
              top: "20vh",
              px: { xs: 0, md: 10 },
            }}
          >
            <Grid item md={3} style={{ minHeight: "auto", display: "flex", paddingLeft: "10px", padding: "10px" }}>
              <Card variant="outlined">
                <Grid
                  container
                  direction="column"
                  justifyContent="center"
                  alignItems="center"
                >
                  <Grid item sx={{ p: "1.5rem 0rem", textAlign: "center" }}>
                    <Badge
                      overlap="circular"
                      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
                      badgeContent={
                        <PhotoCameraIcon
                          sx={{
                            border: "5px solid white",
                            backgroundColor: "white",
                            borderRadius: "50%",
                            padding: ".2rem",
                            width: 35,
                            height: 35,
                          }}
                        ></PhotoCameraIcon>
                      }
                    >
                      <Avatar
                        sx={{ width: 100, height: 100, mb: 1.5 }}
                        src="https://inkythuatso.com/uploads/thumbnails/800/2023/03/9-anh-dai-dien-trang-inkythuatso-03-15-27-03.jpg"
                      ></Avatar>
                    </Badge>
                    <Typography variant="h6">{user?.name}</Typography>
                    <h3 id="fullname"></h3>
                    <p id="job">~~~~~~~~~~~~~~~~~~~~~~~~~~~~</p>
                    <i className="far fa-edit mb-5"></i>
                  </Grid>
                  <Grid container>
                    <Grid item xs={6}>
                      <Typography style={styles.details}>Detail 1</Typography>
                      <Typography style={styles.details}>Detail 2</Typography>
                      <Typography style={styles.details}>Detail 3</Typography>
                    </Grid>
                    <Grid item xs={6} sx={{ textAlign: "end" }}>
                      <Typography style={styles.value}>50</Typography>
                      <Typography style={styles.value}>50</Typography>
                      <Typography style={styles.value}>50</Typography>
                    </Grid>
                  </Grid>
                </Grid>
              </Card>
            </Grid>
            <Grid item md={9} style={{ paddingLeft: "10px", padding: "10px" }}>
              <div className="custom-height">
                <div className="">
                  <div className="card-body p-4">
                    <h3 style={{ color: "black" }}>{t("information")}</h3>
                    <hr className="mt-0 mb-4" />
                    <div className="row ">
                      <div className="col-6 " style={{ display: "flex" }}>
                        <h5 style={{ color: "black", marginRight: "10px" }}>{t("full_name")}:</h5>
                        <p id="fullname" className="text-mutedd">
                          {user?.name || "N/A"}
                        </p>
                      </div>
                      <div className="col-6 " style={{ display: "flex" }}>
                        <h5 style={{ color: "black", marginRight: "10px" }}>{t("gender")}:</h5>
                        <p id="gender" className="text-mutedd">
                          {user?.gender || "N/A"}
                        </p>
                      </div>
                      <div className="col-6 " style={{ display: "flex" }}>
                        <h5 style={{ color: "black", marginRight: "10px" }}>{t("Role")}:</h5>
                        <p id="role" className="text-mutedd">
                          Admin
                        </p>
                      </div>
                      <div className="col-6 " style={{ display: "flex" }}>
                        <h5 style={{ color: "black", marginRight: "10px" }}>{t("status")}:</h5>
                        <p id="status" className="text-mutedd">
                          Active
                        </p>
                      </div>
                    </div>
                    <h3 style={{ color: "black" }}>{t("Contact")}</h3>
                    <hr className="mt-0 mb-4" />
                    <div className="row pt-1">
                      <div className="col-6 mb-3" style={{ display: "flex" }}>
                        <h5 style={{ color: "black", marginRight: "10px" }}>{t("phone")}:</h5>
                        <p id="phone" className="text-mutedd">
                          {user?.phone || "N/A"}
                        </p>
                      </div>
                      <div className="col-6 mb-3" style={{ display: "flex" }}>
                        <h5 style={{ color: "black", marginRight: "10px" }}>{t("email")}:</h5>
                        <p id="email" className="text-mutedd">
                          {user?.email}
                        </p>
                      </div>
                      <div className="col-6 mb-3" style={{ display: "flex" }}>
                        <h5 style={{ color: "black", marginRight: "10px" }}>{t("address")}:</h5>
                        <p id="address" className="text-mutedd">
                          123 ho chi minh
                        </p>
                      </div>
                    </div>
                    <div className="d-flex">
                      <a
                        href="#!"
                        className="btn text-white btn-floating m-1 rounded-circle"
                        style={{ backgroundColor: "#3b5998" }}
                        role="button"
                      >
                        <FontAwesomeIcon icon={["fab", "facebook"]} />
                      </a>
                      <a
                        href="#!"
                        className="btn text-white btn-floating m-1 rounded-circle"
                        style={{ backgroundColor: "rgb(85, 172, 238)" }}
                        role="button"
                      >
                        <FontAwesomeIcon icon={["fab", "twitter"]} />
                      </a>
                      <a
                        href="#!"
                        className="btn text-white btn-floating m-1 rounded-circle"
                        style={{ backgroundColor: "#dd4b39" }}
                        role="button"
                      >
                        <FontAwesomeIcon icon={["fab", "google"]} />
                      </a>
                      <a
                        href="#!"
                        className="btn text-white btn-floating m-1 rounded-circle"
                        style={{ backgroundColor: "#ac2bac" }}
                        role="button"
                      >
                        <FontAwesomeIcon icon={["fab", "instagram"]} />
                      </a>
                      <a
                        href="#!"
                        className="btn text-white btn-floating m-1 rounded-circle"
                        style={{ backgroundColor: "#0082ca" }}
                        role="button"
                      >
                        <FontAwesomeIcon icon={["fab", "linkedin-in"]} />
                      </a>
                      <a
                        href="#!"
                        className="btn text-white btn-floating m-1 rounded-circle"
                        style={{ backgroundColor: "#333333" }}
                        role="button"
                      >
                        <FontAwesomeIcon icon={["fab", "github"]} />
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </Grid>
          </Grid>
        </Grid>
      </CssBaseline>
    </div>
  );
}

export default ProfilePage;
