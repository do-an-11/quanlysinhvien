import React, { useEffect, useState } from "react";

import {
  faBan,
  faEdit,
  faEye,
  faPlusCircle,
  faStar,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Form, Table } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import Select from "react-select";
import {
  actGetPermissionsAsync,
  actUpdatePermissionsAsync,
} from "../store/permission/action";

import "../css/permission.css";
import { useTranslation } from "react-i18next";

function PermissionPage() {
  const [status, setStatus] = useState(false);
  const role = useSelector((state) => state.USER.role);

  const permissionList = useSelector(
    (state) => state.PERMISSIONS.permissionsList
  );
  const dispatch = useDispatch();
  const token = localStorage.getItem("ACCESS_TOKEN");

  useEffect(() => {
    dispatch(actGetPermissionsAsync(token));
    setTableData(permissionList);
  }, []);
  const [tableData, setTableData] = useState(permissionList);

  const accessLevels = [
    { value: "ACCESS_DENIED", label: "Access Denied", icon: faBan },
    { value: "VIEW", label: "View", icon: faEye },
    { value: "MODIFY", label: "Modify", icon: faEdit },
    { value: "CREATE", label: "Create", icon: faPlusCircle },
    { value: "FULL_ACCESS", label: "Full Access", icon: faStar },
  ];

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      display: "flex",
      alignItems: "center",
      padding: "8px",
      transition: "background-color 0.3s ease-in-out",
      backgroundColor: state.isSelected
        ? "#13c044"
        : state.isFocused
        ? "lightgray"
        : "white",
    }),
  };

  const handleAccessLevelChange = (roleName, field, selectedOption) => {
    const updatedTableData = tableData.map((row) => {
      if (row.roleName === roleName) {
        return { ...row, [field]: selectedOption.value };
      }
      return row;
    });
    setTableData(updatedTableData);
  };

  function handleStatus() {
    setStatus(!status);

    if (!status) {
      if (role === "CLASS_ADMIN") {
        setTableData(
          permissionList.filter((e) => e.roleName !== "SUPER_ADMIN")
        );
      } else {
        setTableData(permissionList);
      }
    }
  }
  useEffect(() => {
    if (role === "CLASS_ADMIN") {
      setTableData(permissionList.filter((e) => e.roleName !== "SUPER_ADMIN"));
    }
  }, []);

  function handleCancel() {
    setStatus(false);
    setTableData(permissionList);
  }

  function handleSubmit() {
    if (role === "CLASS_ADMIN") {
      let item = permissionList.filter((e) => e.roleName === "SUPER_ADMIN");
      setTableData([...tableData, ...item]);
    }
    dispatch(actUpdatePermissionsAsync(token, tableData));

    setStatus(false);
  }
  const { t } = useTranslation();
  return (
    <>
      <h1>{t("user_permission")}</h1>
      {!status && (
        <Button onClick={handleStatus}>{t("update_permission")}</Button>
      )}
      <br />
      <br />
      <Form>
        <Table striped bordered hover className="permission-table">
          <thead>
            <tr>
              <th>{t("role_name")}</th>
              <th>{t("syllabus")}</th>
              <th>{t("training_program")}</th>
              <th>{t("class")}</th>
              <th>{t("learn_material")}</th>
              <th>{t("user")}</th>
            </tr>
          </thead>
          <tbody>
            {status
              ? tableData?.map((row, index) => (
                  <tr key={index}>
                    <td>{row.roleName}</td>
                    <td>
                      <Select
                        styles={customStyles}
                        value={{
                          value: row.syllabus,
                          label: (
                            <div>
                              <FontAwesomeIcon
                                icon={
                                  accessLevels.find((level) =>
                                    row.syllabus.includes(level.value)
                                  ).icon
                                }
                                style={{ marginRight: "8px" }}
                              />
                              {row.syllabus}
                            </div>
                          ),
                        }}
                        options={accessLevels.map((accessLevel) => ({
                          value: accessLevel.value,
                          label: (
                            <div>
                              <FontAwesomeIcon
                                icon={accessLevel.icon}
                                style={{ marginRight: "8px" }}
                              />
                              {accessLevel.label}
                            </div>
                          ),
                        }))}
                        onChange={(selectedOption) =>
                          handleAccessLevelChange(
                            row.roleName,
                            "syllabus",
                            selectedOption
                          )
                        }
                      />
                    </td>
                    <td>
                      <Select
                        styles={customStyles}
                        value={{
                          value: row.training,
                          label: (
                            <div>
                              <FontAwesomeIcon
                                icon={
                                  accessLevels.find((level) =>
                                    row.training.includes(level.value)
                                  ).icon
                                }
                                style={{ marginRight: "8px" }}
                              />
                              {row.training}
                            </div>
                          ),
                        }}
                        options={accessLevels.map((accessLevel) => ({
                          value: accessLevel.value,
                          label: (
                            <div>
                              <FontAwesomeIcon
                                icon={accessLevel.icon}
                                style={{ marginRight: "8px" }}
                              />
                              {accessLevel.label}
                            </div>
                          ),
                        }))}
                        onChange={(selectedOption) =>
                          handleAccessLevelChange(
                            row.roleName,
                            "training",
                            selectedOption
                          )
                        }
                      />
                    </td>
                    <td>
                      <Select
                        styles={customStyles}
                        value={{
                          value: row.userclass,
                          label: (
                            <div>
                              <FontAwesomeIcon
                                icon={
                                  accessLevels.find((level) =>
                                    row.userclass.includes(level.value)
                                  ).icon
                                }
                                style={{ marginRight: "8px" }}
                              />
                              {row.userclass}
                            </div>
                          ),
                        }}
                        options={accessLevels.map((accessLevel) => ({
                          value: accessLevel.value,
                          label: (
                            <div>
                              <FontAwesomeIcon
                                icon={accessLevel.icon}
                                style={{ marginRight: "8px" }}
                              />
                              {accessLevel.label}
                            </div>
                          ),
                        }))}
                        onChange={(selectedOption) =>
                          handleAccessLevelChange(
                            row.roleName,
                            "userclass",
                            selectedOption
                          )
                        }
                      />
                    </td>
                    <td>
                      <Select
                        styles={customStyles}
                        value={{
                          value: row.learningMaterial,
                          label: (
                            <div>
                              <FontAwesomeIcon
                                icon={
                                  accessLevels.find((level) =>
                                    row.learningMaterial.includes(level.value)
                                  ).icon
                                }
                                style={{ marginRight: "8px" }}
                              />
                              {row.learningMaterial}
                            </div>
                          ),
                        }}
                        options={accessLevels.map((accessLevel) => ({
                          value: accessLevel.value,
                          label: (
                            <div>
                              <FontAwesomeIcon
                                icon={accessLevel.icon}
                                style={{ marginRight: "8px" }}
                              />
                              {accessLevel.label}
                            </div>
                          ),
                        }))}
                        onChange={(selectedOption) =>
                          handleAccessLevelChange(
                            row.roleName,
                            "learningMaterial",
                            selectedOption
                          )
                        }
                      />
                    </td>
                    {role === "SUPER_ADMIN" ? (
                      <td>
                        <Select
                          styles={customStyles}
                          value={{
                            value: row.userManagement,
                            label: (
                              <div>
                                <FontAwesomeIcon
                                  icon={
                                    accessLevels.find((level) =>
                                      row.userManagement.includes(level.value)
                                    ).icon
                                  }
                                  style={{ marginRight: "8px" }}
                                />
                                {row.userManagement}
                              </div>
                            ),
                          }}
                          options={accessLevels.map((accessLevel) => ({
                            value: accessLevel.value,
                            label: (
                              <div>
                                <FontAwesomeIcon
                                  icon={accessLevel.icon}
                                  style={{ marginRight: "8px" }}
                                />
                                {accessLevel.label}
                              </div>
                            ),
                          }))}
                          onChange={(selectedOption) =>
                            handleAccessLevelChange(
                              row.roleName,
                              "userManagement",
                              selectedOption
                            )
                          }
                        />
                      </td>
                    ) : (
                      <td>
                        <FontAwesomeIcon
                          icon={
                            accessLevels.find((level) =>
                              row.userManagement.includes(level.value)
                            ).icon
                          }
                          style={{ marginRight: "8px" }}
                        />
                        {row.userManagement}
                      </td>
                    )}
                  </tr>
                ))
              : permissionList.map((row, index) => (
                  <tr key={index}>
                    <td>{row.roleName}</td>
                    <td>
                      {row.syllabus}
                      <FontAwesomeIcon
                        icon={
                          accessLevels.find((level) =>
                            row.syllabus.includes(level.value)
                          ).icon
                        }
                        style={{ marginLeft: "4px" }}
                      />
                    </td>
                    <td>
                      {row.training}
                      <FontAwesomeIcon
                        icon={
                          accessLevels.find((level) =>
                            row.training.includes(level?.value)
                          ).icon
                        }
                        style={{ marginLeft: "4px" }}
                      />
                    </td>
                    <td>
                      {row.userclass}
                      <FontAwesomeIcon
                        icon={
                          accessLevels.find((level) =>
                            row.userclass.includes(level.value)
                          ).icon
                        }
                        style={{ marginLeft: "4px" }}
                      />
                    </td>
                    <td>
                      {row.learningMaterial}
                      <FontAwesomeIcon
                        icon={
                          accessLevels.find((level) =>
                            row.learningMaterial.includes(level.value)
                          ).icon
                        }
                        style={{ marginLeft: "4px" }}
                      />
                    </td>
                    <td>
                      {row.userManagement}
                      <FontAwesomeIcon
                        icon={
                          accessLevels.find((level) =>
                            row.userManagement.includes(level.value)
                          ).icon
                        }
                        style={{ marginLeft: "4px" }}
                      />
                    </td>
                  </tr>
                ))}
          </tbody>
        </Table>
        {status && (
          <>
            <div style={{ display: "flex", justifyContent: "flex-end" }}>
              <Button
                onClick={(e) => handleSubmit(e, tableData)}
                className="btn-primary"
                style={{ marginRight: "10px" }}
              >
                Save
              </Button>
              <Button onClick={handleCancel} className="btn-danger">
                Cancel
              </Button>
            </div>
          </>
        )}
      </Form>
    </>
  );
}

export default PermissionPage;
