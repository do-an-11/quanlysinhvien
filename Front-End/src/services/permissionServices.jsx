import { API } from "./api";

export const PermissionServices = {
  getListPermission(token) {
    return API.get("/user-permission/get-all", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  updatePermission(token, tableData) {
    return API.put("/user-permission/update", tableData, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
};
