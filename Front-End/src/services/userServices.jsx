import { API, API_USER } from "./api";

export const UserServices = {
  postUser: (data) => {
    return API_USER.post("/user", data);
  },
  getAllUser: () => {
    return API_USER.get("/user");
  },
  updateUser: (id, data) => {
    return API_USER.put(`/user/${id}`, data);
  },
  deleteUser: (id) => {
    return API_USER.delete(`/user/${id}`);
  },
  loginUser(form) {
    return API.post("/authentication/login", form);
  },
  postUserAPI: (data, token) => {
    return API.post("/authentication/create-user", data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  updateUserAPI: (id, data, token) => {
    return API.put(`/user/update-user/${id}`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  deleteUserAPI: (id, token) => {
    return API.get(`/user/inactive/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  recoverUserAPI: (id, token) => {
    return API.get(`/user/active/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  getAllUserAPI: (token) => {
    return API.get("/user/get-all", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  getAllTrainerAPI: (token) => {
    return API.get("/user/get-all/trainer", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  getAllTraineeAPI: (token) => {
    return API.get("/user/get-all/trainee", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  getAllAdminAPI: (token) => {
    return API.get("/user/get-all/admin", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  getAllAdminAndSuperAdminAPI: (token) => {
    return API.get("/user/get-all/admin-and-superadmin", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  fetchMe: (token) => {
    return API.get("/authentication/login/get-user", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
};
