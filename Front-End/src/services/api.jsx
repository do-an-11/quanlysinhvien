import axios from "axios";

export const API_USER = axios.create({
  baseURL: "https://649e8167245f077f3e9c759b.mockapi.io",
});
export const API_PERMISSION = axios.create({
  baseURL: "https://64b347ea38e74e386d55f4cf.mockapi.io/permission",
});
export const API = axios.create({
  baseURL:
    // "https://hoanganhluong.id.vn/",
    "http://localhost:8080/",
});
export const API_CLASS = axios.create({
  baseURL: "https://649e8167245f077f3e9c759b.mockapi.io/",
});
export const API_CLASS_TEST = axios.create({
  baseURL: "https://65323ad84d4c2e3f333dcdd2.mockapi.io",
});
export const API_PROGRAM = axios.create({
  baseURL: "https://653f6a129e8bd3be29e082e1.mockapi.io",
});
export const API_PROGRAM_TEST = axios.create({
  baseURL: "https://65323ad84d4c2e3f333dcdd2.mockapi.io",
});
export const API_SYLLABUS = axios.create({
  baseURL: "https://654c6d1977200d6ba858d5a4.mockapi.io",
});
export const API_SYLLABUS_TEST = axios.create({
  baseURL:
    "https://hoanganhluong.id.vn/api/syllabus/create/outline",
});
