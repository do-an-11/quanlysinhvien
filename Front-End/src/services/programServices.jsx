import { API } from "./api";

export const ProgramServices = {
  getListProgramAPI: (token) => {
    return API.get("training-program/get-all", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  getListProgramAPIAll: (token) => {
    return API.get("training-program/get-all/All", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  getPostProgramAPI: (data, token) => {
    return API.post("/training-program/create", data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },

  updateProgramAPI: (id, data, token) => {
    return API.put(`/training-program/training-program/${id}`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  inActiveProgramAPI: (id, token) => {
    return API.get(`/training-program/deactivate/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  activeProgramAPI: (id, token) => {
    return API.get(`/training-program/activate/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
};
