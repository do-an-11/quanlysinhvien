import { API, API_CLASS, API_CLASS_TEST } from "./api";

export const ClassServices = {
  postClass: (data) => {
    return API_CLASS.post("/class", data);
  },
  postClassAPI: (data, token) => {
    return API.post("/api/class/create", data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  getListClass: () => {
    return API_CLASS.get("/class");
  },
  getListClassAPI: (token) => {
    return API.get("/api/class", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  updateClassAPI: (id, data, token) => {
    return API.put(`/api/class/update/${id}`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  deleteClassAPI: (id, token) => {
    return API.put(`/api/class/deactivate/${id}`, id, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  deleteClass: (id) => {
    return API_CLASS.delete(`/class/${id}`);
  },
  updateClass: (id, data) => {
    return API_CLASS.put(`/class/${id}`, data);
  },
  getAllSyllabus: () => {
    return API_CLASS_TEST.get("/courseName");
  },
};
