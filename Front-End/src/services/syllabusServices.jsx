import { API, API_SYLLABUS_TEST, API_SYLLABUS } from "./api";

export const SyllabusServices = {
  addSyllabus: (data) => {
    return API_SYLLABUS.post("/syllabus", data);
  },
  getAllSyllabus: () => {
    return API_SYLLABUS.get("/syllabus");
  },
  updateSyllabus: (id, data) => {
    return API_SYLLABUS.put(`/syllabus/${id}`, data);
  },
  deleteSyllabus: (id) => {
    return API_SYLLABUS.delete(`/syllabus/${id}`);
  },
  getAllSyllabusAPI: (token) => {
    return API.get("/api/syllabus", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  getAllActiveSyllabusAPI: (token) => {
    return API.get("/api/syllabus/active", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  postSyllabusAPI: (data, token) => {
    return API.post(`/api/syllabus/create`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  postSyllabusOutlineAPI: (data, token) => {
    return API.post("/api/syllabus/outline", data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  updateSyllabusAPI: (data, token) => {
    return API.put(`/api/syllabus/update`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  deleteSyllabusAPI: (topicCode, token) => {
    return API.put(`/api/syllabus/delete/${topicCode}`, topicCode, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  CreateOutline: (token, data) => {
    return API_SYLLABUS_TEST.post("/api/syllabus/create/outline", data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  activeSyllabusAPI: (topicCode, token) => {
    return API.put(`/api/syllabus/undelete/${topicCode}`, topicCode, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  duplicateSyllabusAPI: (topicCode, token) => {
    return API.get(`/api/syllabus/duplicate/${topicCode}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
};

